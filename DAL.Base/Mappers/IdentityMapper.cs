using Contracts.DAL.Base.Mappers;

namespace DAL.Base.Mappers
{
    public class IdentityMapper<TLeftObject, TRightObject> : IBaseMapper
        where TRightObject : class?, new() 
        where TLeftObject : class?, new()
    {
        /*
        public TRightObject Map(TLeftObject inObject)
        {
            return inObject as TRightObject ?? default!;
        }

        public TLeftObject Map(TRightObject inObject)
        {
            return inObject as TLeftObject ?? default!;
        }
        */
        public TOut Map<TIn, TOut>(TIn inObject) 
            where TIn : class?, new() 
            where TOut : class?, new()
        {
            return inObject as TOut ?? default!;
        }
    }
}
