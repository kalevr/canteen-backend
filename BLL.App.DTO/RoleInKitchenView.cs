using System;
using System.ComponentModel.DataAnnotations;
using BLL.App.DTO.Identity;
using Contracts.Domain;

namespace BLL.App.DTO
{
    public class RoleInKitchenView : IDomainEntityId
    {
        public Guid Id { get; set; }

        [Display(Name = nameof(StartingFrom), ResourceType = typeof(Resources.BLL.RoleInKitchenView))]
        public DateTime StartingFrom { get; set; }

        [Display(Name = nameof(ValidUntil), ResourceType = typeof(Resources.BLL.RoleInKitchenView))]
        public DateTime? ValidUntil { get; set; }


        public Guid AppRoleId { get; set; } = default!;
        [Display(Name = nameof(AppRole), ResourceType = typeof(Resources.BLL.RoleInKitchenView))]
        public AppRole? AppRole { get; set; }

        public Guid KitchenViewId { get; set; } = default!;
        [Display(Name = nameof(KitchenView), ResourceType = typeof(Resources.BLL.RoleInKitchenView))]
        public KitchenView? KitchenView { get; set; } 
    }
}