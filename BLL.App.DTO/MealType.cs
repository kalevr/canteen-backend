using System;
using System.ComponentModel.DataAnnotations;
using Contracts.Domain;

namespace BLL.App.DTO
{
    public class MealType : IDomainEntityId
    {
        public Guid Id { get; set; }

        [MinLength(1)]
        [MaxLength(256)]
        [Required]
        [Display(Name = nameof(MealTypeName), ResourceType = typeof(Resources.BLL.MealType))]
        public string MealTypeName { get; set; } = default!;
        
        [MinLength(1)]
        [MaxLength(256)]
        [Required]
        [Display(Name = nameof(MealTypeDisplayName), ResourceType = typeof(Resources.BLL.MealType))]
        public string MealTypeDisplayName { get; set; } = default!;
        
        [Display(Name = nameof(MealTime), ResourceType = typeof(Resources.BLL.MealType))]
        public DateTime MealTime { get; set; } = default!;

        [Display(Name = nameof(ParticipationCutoff), ResourceType = typeof(Resources.BLL.MealType))]
        public int ParticipationCutoff { get; set; } = default!;
        
        [Display(Name = nameof(RegistrationCutoff), ResourceType = typeof(Resources.BLL.MealType))]
        public int RegistrationCutoff { get; set; } = default!;

        [MaxLength(1024)]
        [Display(Name = nameof(Comment), ResourceType = typeof(Resources.BLL.MealType))]
        public string? Comment { get; set; }


        public Guid KitchenId { get; set; }
        [Display(Name = nameof(Kitchen), ResourceType = typeof(Resources.BLL.MealType))]
        public Kitchen? Kitchen { get; set; }

    }
}