using System;
using System.ComponentModel.DataAnnotations;
using BLL.App.DTO.Identity;
using Contracts.Domain;

namespace BLL.App.DTO
{
    public class Registration : IDomainEntityId
    {
        public Guid Id { get; set; }

        [Display(Name = nameof(MealDate), ResourceType = typeof(Resources.BLL.Registration))]
        public DateTime MealDate { get; set; }

        
        public Guid RoleMealTypeId { get; set; } = default!;

        [Display(Name = nameof(RoleMealType), ResourceType = typeof(Resources.BLL.Registration))]
        public RoleMealType? RoleMealType { get; set; }
        
        public Guid AppUserId { get; set; }
        public AppUser? AppUser { get; set; }
    }
}