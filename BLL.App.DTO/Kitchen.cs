using System;
using System.ComponentModel.DataAnnotations;
using Contracts.Domain;

namespace BLL.App.DTO
{
    public class Kitchen : IDomainEntityMetadata, IDomainEntityId
    {
        public Guid Id { get; set; }

        [Display(Name = nameof(KitchenName), ResourceType = typeof(Resources.BLL.Kitchen))]
        public string KitchenName { get; set; } = default!;

        public string? CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string? DeletedBy { get; set; }
        public DateTime? DeletedAt { get; set; }
    }
}