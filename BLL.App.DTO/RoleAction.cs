using System;
using System.ComponentModel.DataAnnotations;
using BLL.App.DTO.Identity;
using Contracts.Domain;

namespace BLL.App.DTO
{
    public class RoleAction : IDomainEntityId
    {
        public Guid Id { get; set; }


        [Display(Name = nameof(StartingFrom), ResourceType = typeof(Resources.BLL.RoleAction))]
        public DateTime StartingFrom { get; set; }

        [Display(Name = nameof(ValidUntil), ResourceType = typeof(Resources.BLL.RoleAction))]
        public DateTime ValidUntil { get; set; }


        public Guid MyActionId { get; set; } = default!;

        [Display(Name = nameof(Action), ResourceType = typeof(Resources.BLL.RoleAction))]
        public MyAction? Action { get; set; }

        public Guid AppRoleId { get; set; } = default!;

        [Display(Name = nameof(AppRole), ResourceType = typeof(Resources.BLL.RoleAction))]
        public AppRole? AppRole { get; set; }

        public Guid? TargetAppRoleId { get; set; } = default!;

        [Display(Name = nameof(TargetAppRole), ResourceType = typeof(Resources.BLL.RoleAction))]
        public AppRole? TargetAppRole { get; set; }
    }
}