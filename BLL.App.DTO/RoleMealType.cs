using System;
using System.ComponentModel.DataAnnotations;
using BLL.App.DTO.Identity;
using Contracts.Domain;

namespace BLL.App.DTO
{
    public class RoleMealType : IDomainEntityId
    {
        public Guid Id { get; set; }

        [Display(Name = nameof(StartingFrom), ResourceType = typeof(Resources.BLL.RoleMealType))]
        [DataType(DataType.Date)]
        public DateTime StartingFrom { get; set; }

        [Display(Name = nameof(ValidUntil), ResourceType = typeof(Resources.BLL.RoleMealType))]
        [DataType(DataType.Date)]
        public DateTime? ValidUntil { get; set; }


        public Guid MealTypeId { get; set; } = default!;
        [Display(Name = nameof(MealType), ResourceType = typeof(Resources.BLL.RoleMealType))]
        public MealType? MealType { get; set; }

        public string? MealTypeName { get; set; }

        public Guid AppRoleId { get; set; } = default!;
        [Display(Name = nameof(AppRole), ResourceType = typeof(Resources.BLL.RoleMealType))]
        public AppRole? AppRole { get; set; }
    }
}