using System;
using System.ComponentModel.DataAnnotations;
using BLL.App.DTO.Identity;
using Contracts.Domain;

namespace BLL.App.DTO
{
    public class LoginHistory : IDomainEntityId
    {
        public Guid Id { get; set; }

        [MaxLength(256)]
        [Display(Name = nameof(UserName), ResourceType = typeof(Resources.BLL.LoginHistory))]
        public string UserName { get; set; } = default!;
        
        [MaxLength(20)]
        [Display(Name = nameof(Ipv4), ResourceType = typeof(Resources.BLL.LoginHistory))]
        public string? Ipv4 { get; set; }
        
        [MaxLength(35)]
        [Display(Name = nameof(Ipv6), ResourceType = typeof(Resources.BLL.LoginHistory))]
        public string? Ipv6 { get; set; }
        
        [Display(Name = nameof(LoginTime), ResourceType = typeof(Resources.BLL.LoginHistory))]
        public DateTime LoginTime { get; set; }
        
        [Display(Name = nameof(Success), ResourceType = typeof(Resources.BLL.LoginHistory))]
        public bool Success { get; set; }
        
        [MaxLength(4096)]
        [Display(Name = nameof(UserAgent), ResourceType = typeof(Resources.BLL.LoginHistory))]
        public string? UserAgent { get; set; }
        
        public Guid? AppUserId { get; set; }
        
        [Display(Name = nameof(AppUser), ResourceType = typeof(Resources.BLL.LoginHistory))]
        public AppUser? AppUser { get; set; }

        public string? Search { get; set; } = "";
    }
}