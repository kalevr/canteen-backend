using System;
using System.Collections.Generic;

namespace BLL.App.DTO
{
    public class RoleInKitchenViewSidebarViewModel
    {
        public IEnumerable<KitchenView> KitchenViews { get; set; } = default!;
        public DateTime ChosenDate { get; set; }
        public List<DateTime> WorkingDays { get; set; } = default!;
        public Guid MealTypeId { get; set; }
        public Dictionary<string, Dictionary<DateTime, int>> MealSummary { get; set; } = 
            new Dictionary<string, Dictionary<DateTime, int>>();

    }
}