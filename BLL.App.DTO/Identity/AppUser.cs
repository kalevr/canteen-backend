using System;
using System.ComponentModel.DataAnnotations;
using Contracts.Domain;

namespace BLL.App.DTO.Identity
{
    public class AppUser: IDomainEntityId, IDomainEntityMetadata
    {
        public Guid Id { get; set; }

        [MaxLength(50)] [MinLength(1)]
        [Display(Name = nameof(FirstName), ResourceType = typeof(Resources.BLL.Identity.AppUser))]
        public string FirstName { get; set; } = default!;

        [Display(Name = nameof(LastName), ResourceType = typeof(Resources.BLL.Identity.AppUser))]
        [MaxLength(50)] [MinLength(1)] public string LastName { get; set; } = default!;

        [Display(Name = nameof(ManageSelf), ResourceType = typeof(Resources.BLL.Identity.AppUser))]
        public bool ManageSelf { get; set; } = default!;
        
        [MaxLength(1024)]
        [Display(Name = nameof(Comment), ResourceType = typeof(Resources.BLL.Identity.AppUser))]
        public string? Comment { get; set; }

        
        [DataType(DataType.Password)]
        [Display(Name = nameof(Password), ResourceType = typeof(Resources.BLL.Identity.AppUser))]
        public string Password { get; set; } = default!;
        
        [DataType(DataType.Password)]
        [Display(Name = nameof(ConfirmPassword), ResourceType = typeof(Resources.BLL.Identity.AppUser))]
        public string ConfirmPassword { get; set; } = default!;

        [Display(Name = nameof(UserName), ResourceType = typeof(Resources.BLL.Identity.AppUser))]
        public string UserName { get; set; } = default!;
        
        [DataType(DataType.EmailAddress)]
        [Display(Name = nameof(Email), ResourceType = typeof(Resources.BLL.Identity.AppUser))]
        public string Email { get; set; } = default!;

        
        public string? CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string? DeletedBy { get; set; }
        public DateTime? DeletedAt { get; set; }
        
        [Display(Name = nameof(FirstLastName), ResourceType = typeof(Resources.BLL.Identity.AppUser))]
        public string? FirstLastName { get; set; }
    }
}