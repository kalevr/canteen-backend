using System;
using Contracts.Domain;

namespace BLL.App.DTO.Identity
{
    public class AppUserRole : IDomainEntityId, IDomainEntityMetadata
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }
        public AppUser? AppUser { get; set; }
        
        public Guid RoleId { get; set; }
        public AppRole? AppRole { get; set; }

        public DateTime StartingFrom { get; set; }
        public DateTime ValidUntil { get; set; }
        public string? CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string? DeletedBy { get; set; }
        public DateTime? DeletedAt { get; set; }
    }
}