using System;
using System.ComponentModel.DataAnnotations;
using Contracts.Domain;

namespace BLL.App.DTO.Identity
{
    public class AppRole : IDomainEntityId, IDomainEntityMetadata
    {
        public Guid Id { get; set; }

        [Display(Name = nameof(GetsMeal), ResourceType = typeof(Resources.BLL.Identity.AppRole))]
        public bool GetsMeal { get; set; } = default!;

        [Display(Name = nameof(Comment), ResourceType = typeof(Resources.BLL.Identity.AppRole))]
        [MaxLength(1024)]
        public string? Comment { get; set; }


        [Display(Name = nameof(CreatedBy), ResourceType = typeof(Resources.BLL.Metadata))]
        public string? CreatedBy { get; set; }

        [Display(Name = nameof(CreatedAt), ResourceType = typeof(Resources.BLL.Metadata))]
        public DateTime CreatedAt { get; set; }

        [Display(Name = nameof(DeletedBy), ResourceType = typeof(Resources.BLL.Metadata))]
        public string? DeletedBy { get; set; }

        [Display(Name = nameof(DeletedAt), ResourceType = typeof(Resources.BLL.Metadata))]
        public DateTime? DeletedAt { get; set; }
        

        [MinLength(1)]
        [MaxLength(255)]
        [Required]
        [Display(Name = nameof(Name), ResourceType = typeof(Resources.BLL.Identity.AppRole))]
        
        public string Name { get; set; } = default!;
        
        [MinLength(1)]
        [MaxLength(255)]
        [Required]
        [Display(Name = nameof(DisplayName), ResourceType = typeof(Resources.BLL.Identity.AppRole))]
        public string DisplayName { get; set; } = default!;
    }

}