using System;
using System.ComponentModel.DataAnnotations;
using Contracts.Domain;

namespace BLL.App.DTO
{
    public class KitchenView : IDomainEntityId
    {
        public Guid Id { get; set; }

        [MinLength(1)]
        [MaxLength(256)]
        [Required]
        [Display(Name = nameof(KitchenViewName), ResourceType = typeof(Resources.BLL.KitchenView))]
        public string KitchenViewName { get; set; } = default!;

        [MinLength(1)]
        [MaxLength(256)]
        [Required]
        [Display(Name = nameof(KitchenViewDisplayName), ResourceType = typeof(Resources.BLL.KitchenView))]
        public string KitchenViewDisplayName { get; set; } = default!;

        [Display(Name = nameof(StartingFrom), ResourceType = typeof(Resources.BLL.KitchenView))]
        public DateTime StartingFrom { get; set; }

        [Display(Name = nameof(ValidUntil), ResourceType = typeof(Resources.BLL.KitchenView))]
        public DateTime? ValidUntil { get; set; }


        public Guid MealTypeId { get; set; } = default!;
        [Display(Name = nameof(MealType), ResourceType = typeof(Resources.BLL.KitchenView))]
        public MealType? MealType { get; set; }
    }
}