using System;
using System.ComponentModel.DataAnnotations;
using Contracts.Domain;

namespace BLL.App.DTO
{
    public class MyAction : IDomainEntityId, IDomainEntityMetadata
    {
        public Guid Id { get; set; }

        [MinLength(1)]
        [MaxLength(256)]
        [Display(Name = nameof(MyActionName), ResourceType = typeof(Resources.BLL.MyAction))]
        public string MyActionName { get; set; } = default!;
        
        [MinLength(1)]
        [MaxLength(256)]
        [Required]
        [Display(Name = nameof(MyActionDisplayName), ResourceType = typeof(Resources.BLL.MyAction))]
        public string MyActionDisplayName { get; set; } = default!;
        
        public string? CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string? DeletedBy { get; set; }
        public DateTime? DeletedAt { get; set; }
    }
}