using System;
using System.Collections.Generic;
using BLL.App.DTO.Identity;

namespace BLL.App.DTO
{
    public class ParticipationSidebarViewModel
    {
        public IEnumerable<RoleMealType> AllCurrentRoleMealTypes { get; set; } = default!;
        public IEnumerable<Participation> Participations { get; set; } = default!;
        public IEnumerable<AppUserRole> AppUsers { get; set; } = default!;
        public Guid CurrentMealTypeId { get; set; }
        public DateTime ChosenDate { get; set; }
        public bool HasContent { get; set; }
        public List<DateTime> WorkingDays { get; set; } = default!;
        public bool HasEditRight { get; set; }
        public bool HasExtendedEditRight { get; set; }
        public Guid RoleMealTypeId { get; set; }
        public Guid RoleId { get; set; }
        public string MealName { get; set; } = default!;
        public int RegCutoff { get; set; }
        public DateTime MealTime { get; set; }

    }
}