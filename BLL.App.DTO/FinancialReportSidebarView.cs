using System;
using System.Collections.Generic;
using BLL.App.DTO.Identity;

namespace BLL.App.DTO
{
    public class FinancialReportSidebarView
    {
        public IEnumerable<MealType> MealTypes { get; set; } = default!;

        public Dictionary<string, Dictionary<string, int>> RegStats { get; set; } = default!;
        public DateTime ChosenDate { get; set; }
        public Guid MealTypeId { get; set; }
        public List<AppUserRole> AppUserRoles { get; set; } = default!;
    }
}