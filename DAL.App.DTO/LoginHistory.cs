using System;
using Contracts.Domain;
using DAL.App.DTO.Identity;

namespace DAL.App.DTO
{
    public class LoginHistory : IDomainEntityId
    { 
        public Guid Id { get; set; }
        
        public string UserName { get; set; } = default!;
        public string? Ipv4 { get; set; }
        public string? Ipv6 { get; set; }
        public DateTime LoginTime { get; set; }
        public bool Success { get; set; }
        public string? UserAgent { get; set; }
        
        public Guid? AppUserId { get; set; }
        public AppUser? AppUser { get; set; }

    }
}