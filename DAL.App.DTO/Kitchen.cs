using System;
using System.Text.Json.Serialization;
using Contracts.DAL.Base;
using Contracts.Domain;
using DAL.App.DTO.Identity;

namespace DAL.App.DTO
{
    public class Kitchen : IDomainEntityId, IDomainEntityMetadata
    { 
        public Guid Id { get; set; }

        public string KitchenName { get; set; } = default!;
        public string? CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string? DeletedBy { get; set; }
        public DateTime? DeletedAt { get; set; }
    }
}