using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Contracts.DAL.Base;
using Contracts.Domain;
using DAL.App.DTO.Identity;

namespace DAL.App.DTO
{
    public class KitchenView : IDomainEntityId
    { 
        public Guid Id { get; set; }
        
        [MinLength(1)]
        [MaxLength(256)]
        [Required]
        public string KitchenViewName { get; set; } = default!;
        
        [MinLength(1)]
        [MaxLength(256)]
        [Required]
        public string KitchenViewDisplayName { get; set; } = default!;
        
        public DateTime StartingFrom { get; set; }
        public DateTime ValidUntil { get; set; }

        public Guid MealTypeId { get; set; } = default!;
        public MealType? MealType { get; set; }
    }
}