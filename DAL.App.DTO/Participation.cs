using System;
using System.Text.Json.Serialization;
using Contracts.DAL.Base;
using Contracts.Domain;
using DAL.App.DTO.Identity;

namespace DAL.App.DTO
{
    public class Participation : IDomainEntityId
    { 
        public Guid Id { get; set; }
        
        public DateTime MealDate { get; set; }
        
        public Guid RoleMealTypeId { get; set; } = default!;
        public RoleMealType? RoleMealType { get; set; }

        public Guid AppUserId { get; set; }
        public AppUser? AppUser { get; set; }

    }
}