using System;
using System.Text.Json.Serialization;
using Contracts.DAL.Base;
using Contracts.Domain;
using DAL.App.DTO.Identity;

namespace DAL.App.DTO
{
    public class RoleInKitchenView : IDomainEntityId
    { 
        public Guid Id { get; set; }
        
        public DateTime StartingFrom { get; set; }
        public DateTime? ValidUntil { get; set; }

        public Guid AppRoleId { get; set; } = default!;
        public AppRole? AppRole { get; set; }

        public Guid KitchenViewId { get; set; } = default!;
        public KitchenView? KitchenView { get; set; } 

    }
}