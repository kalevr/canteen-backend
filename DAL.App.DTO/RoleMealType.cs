using System;
using System.Text.Json.Serialization;
using Contracts.DAL.Base;
using Contracts.Domain;
using DAL.App.DTO.Identity;

namespace DAL.App.DTO
{
    public class RoleMealType : IDomainEntityId
    { 
        public Guid Id { get; set; }
        
        public DateTime StartingFrom { get; set; }
        public DateTime? ValidUntil { get; set; }

        public Guid MealTypeId { get; set; } = default!;
        public MealType? MealType { get; set; }
        public string? MealTypeName { get; set; }

        public Guid AppRoleId { get; set; } = default!;
        public AppRole? AppRole { get; set; }

    }
}