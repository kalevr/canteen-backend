using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Contracts.DAL.Base;
using Contracts.Domain;
using DAL.App.DTO.Identity;

namespace DAL.App.DTO
{
    public class MyAction : IDomainEntityId, IDomainEntityMetadata
    { 
        public Guid Id { get; set; }

        [MinLength(1)]
        [MaxLength(256)]
        [Required]
        public string MyActionName { get; set; } = default!;
        
        [MinLength(1)]
        [MaxLength(256)]
        [Required]
        public string MyActionDisplayName { get; set; } = default!;
        
        public string? CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string? DeletedBy { get; set; }
        public DateTime? DeletedAt { get; set; }
    }
}