using System;
using System.ComponentModel.DataAnnotations;
using Contracts.Domain;

namespace DAL.App.DTO.Identity
{
    public class AppUserRole : IDomainEntityId
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }
        public AppUser? AppUser { get; set; }

        public Guid RoleId { get; set; }
        public AppRole? AppRole { get; set; }

        public DateTime StartingFrom { get; set; }
        public DateTime ValidUntil { get; set; }


        }
}