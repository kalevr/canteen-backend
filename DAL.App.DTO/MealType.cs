using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Contracts.DAL.Base;
using Contracts.Domain;
using DAL.App.DTO.Identity;

namespace DAL.App.DTO
{
    public class MealType : IDomainEntityId
    { 
        public Guid Id { get; set; }
        
        [MinLength(1)]
        [MaxLength(256)]
        [Required]
        public string MealTypeName { get; set; } = default!;
        
        [MinLength(1)]
        [MaxLength(256)]
        [Required]
        public string MealTypeDisplayName { get; set; } = default!;
        
        public DateTime MealTime { get; set; } = default!;
        public int RegistrationCutoff { get; set; } = default!;
        public string? Comment { get; set; }

        public Guid KitchenId { get; set; }
        public Kitchen? Kitchen { get; set; }

    }
}