﻿using BLL.App.Services;
using BLL.App.Services.Identity;
using BLL.Base;
using Contracts.BLL.App;
using Contracts.BLL.App.Services;
using Contracts.BLL.App.Services.Identity;
using Contracts.DAL.App;

namespace BLL.App
{
    public class AppBLL : BaseBLL<IAppUnitOfWork>, IAppBLL
    {
        public AppBLL(IAppUnitOfWork uow) : base(uow)
        {
        }

        public IAppUserService AppUsers =>
            GetService<IAppUserService>(() => new AppUserService(UOW));

        public IAppRoleService AppRoles =>
            GetService<IAppRoleService>(() => new AppRoleService(UOW));

        public IAppUserRoleService AppUserRoles =>
            GetService<IAppUserRoleService>(() => new AppUserRoleService(UOW));
        
        public IKitchenService Kitchens =>
            GetService<IKitchenService>(() => new KitchenService(UOW));
        
        public IKitchenViewService KitchenViews =>
            GetService<IKitchenViewService>(() => new KitchenViewService(UOW));

        public ILoginHistoryService LoginHistories =>
            GetService<ILoginHistoryService>(() => new LoginHistoryService(UOW));
        
        public IMealTypeService MealTypes =>
            GetService<IMealTypeService>(() => new MealTypeService(UOW));
        
        public IMyActionService MyActions =>
            GetService<IMyActionService>(() => new MyActionService(UOW));
        
        public IRegistrationService Registrations =>
            GetService<IRegistrationService>(() => new RegistrationService(UOW));
        
        public IParticipationService Participations =>
            GetService<IParticipationService>(() => new ParticipationService(UOW));
        
        public IRoleActionService RoleActions =>
            GetService<IRoleActionService>(() => new RoleActionService(UOW));
        
        public IRoleInKitchenViewService RoleInKitchenViews =>
            GetService<IRoleInKitchenViewService>(() => new RoleInKitchenViewService(UOW));
        
        public IRoleMealTypeService RoleMealTypes =>
            GetService<IRoleMealTypeService>(() => new RoleMealTypeService(UOW));
    }
}