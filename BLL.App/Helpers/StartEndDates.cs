using System;

namespace BLL.App.Helpers
{
    public static class StartEndDates
    {
        public static (DateTime startDate, DateTime endDate) GetStartAndEndOfMonth(DateTime date)
        {
            var startDate = new DateTime(date.Year, date.Month, 1);
            var endDate = new DateTime(date.Year, date.Month, 
                DateTime.DaysInMonth(date.Year, date.Month), 23, 59, 59);
            return (startDate, endDate);
        }
    }
}