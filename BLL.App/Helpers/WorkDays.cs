using System;
using System.Collections.Generic;

namespace BLL.App.Helpers
{
    public static class WorkDays
    {
        public static List<DateTime> GetWorkDays(DateTime date, DateTime? mealTime = null)
        {
            var workingDays = new List<DateTime>();

            for (int i = 1; i <= DateTime.DaysInMonth(date.Year, date.Month); i++)
            {
                DateTime tmpDate;
                if (mealTime.HasValue)
                {
                    tmpDate = new DateTime(date.Year, date.Month, i,
                        mealTime.Value.Hour, mealTime.Value.Minute, mealTime.Value.Second);
                }
                else
                {
                    tmpDate = new DateTime(date.Year, date.Month, i);
                }
                 
                if (tmpDate.DayOfWeek != DayOfWeek.Saturday && tmpDate.DayOfWeek != DayOfWeek.Sunday)
                {
                    workingDays.Add(tmpDate);
                }
            }

            return workingDays;
        }
    }
}