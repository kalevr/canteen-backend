// ReSharper disable InconsistentNaming
namespace BLL.App.Helpers
{
    
    public static class C
    {
        // Configuration keys (appsettings.json)
        public static string CONF_KEY_JWT_SIGN = "JWT:SigninKey";
        public static string CONF_KEY_JWT_ISSUER = "JWT:Issuer";
        public static string CONF_KEY_JWT_EXPIRATION = "JWT:ExpirationInDays";
        public static string CONF_KEY_DEFAULT_CONNECTION = "DefaultConnection";
        
        // Session stuff
        public static string SESS_CURRENT_USER_ROLE_IDS = "CurrentUserRoleIds";
        public static string SESS_CURRENT_USER_ROLE_NAMES = "CurrentUserRoleNames";
        public static string SESS_USER_FULLNAME = "UserFullName";
        public static string SESS_CURRENT_MEAL_TYPES = "CurrentMealTypes";
        public static string SESS_ADMIN_RANK = "IsAdmin";


        public static string SESS_ADMIN_NAME = "Admin";
        public static string SESS_MANAGER_NAME = "Manager";
        public static string SESS_FINANCE_NAME = "Finance";
        public static string SESS_COOK_NAME = "Cook";
        public static string SESS_NOT_ADMIN = "NotAdmin";
        
        
        // Policy stuff
        public static string POLICY_NAME_ADMIN = "Admin";
        public static string POLICY_NAME_USER = "User";
        public static string POLICY_IS_MANAGER = "IsManager";
        public static string CLAIM_IS_MANAGER = "IsManager";
        
        // ===== GraphQL Field names (mainly in Webapp/GraphQL) - also graph type names =====
        public static string FIELD_ID = "id";
        public static string FIELD_COMMENT = "comment";

        public static string GRAPH_TYPE_LOGIN = "Login";
        public static string GRAPH_TYPE_REGISTER = "Register";
        public static string FIELD_FIRSTNAME = "firstName";
        public static string FIELD_LASTNAME = "lastName";
        public static string FIELD_EMAIL = "email";
        public static string FIELD_PASSWORD = "password";
        public static string FIELD_CONFIRM_PASSWORD = "confirmPassword";
        public static string FIELD_JWT_TOKEN = "jwtToken";

        public static string GRAPH_TYPE_KITCHEN = "Kitchen";
        public static string FIELD_KITCHEN_NAME = "kitchenName";

        public static string GRAPH_TYPE_MEALTYPE = "MealType";
        public static string FIELD_MEALTYPE_NAME = "mealTypeName";
        public static string FIELD_MEALTIME = "mealTime";
        public static string FIELD_REG_CUTOFF = "registrationCutoff";
        public static string FIELD_KITCHEN_ID = "kitchenId";
    }
}