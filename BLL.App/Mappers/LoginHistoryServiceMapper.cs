using AutoMapper;
using Contracts.BLL.App.Mappers;
using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;
using PublicApiV1=PublicApi.DTO.v1;
namespace BLL.App.Mappers
{
    public class LoginHistoryServiceMapper : BLLMapper<DALAppDTO.LoginHistory, BLLAppDTO.LoginHistory>, ILoginHistoryServiceMapper
    {
        public LoginHistoryServiceMapper(): base()
        {
            MapperConfigurationExpression.CreateMap<DALAppDTO.LoginHistory, BLLAppDTO.LoginHistory>();
            MapperConfigurationExpression.CreateMap<PublicApiV1.LoginHistory, BLLAppDTO.LoginHistory>();
            MapperConfigurationExpression.CreateMap<BLLAppDTO.LoginHistory, DALAppDTO.LoginHistory>();


            MapperConfigurationExpression.CreateMap<DALAppDTO.Identity.AppUser, BLLAppDTO.Identity.AppUser>();
            MapperConfigurationExpression.CreateMap<PublicApiV1.Identity.AppUser, BLLAppDTO.LoginHistory>();


            
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));

        }
    }
}