using AutoMapper;
using Contracts.BLL.App.Mappers;
using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;
using PublicApiV1=PublicApi.DTO.v1;
namespace BLL.App.Mappers
{
    public class RegistrationServiceMapper : BLLMapper<DALAppDTO.Registration, BLLAppDTO.Registration>, IRegistrationServiceMapper
    {
        public RegistrationServiceMapper(): base()
        {
            MapperConfigurationExpression.CreateMap<DALAppDTO.RoleMealType, BLLAppDTO.RoleMealType>();
            MapperConfigurationExpression.CreateMap<PublicApiV1.RoleMealType, BLLAppDTO.RoleMealType>();
            MapperConfigurationExpression.CreateMap<BLLAppDTO.RoleMealType, PublicApiV1.RoleMealType>();

            MapperConfigurationExpression.CreateMap<DALAppDTO.Registration, BLLAppDTO.Registration>();
            MapperConfigurationExpression.CreateMap<PublicApiV1.Registration, BLLAppDTO.Registration>();
            MapperConfigurationExpression.CreateMap<BLLAppDTO.Registration, PublicApiV1.Registration>();

            
            MapperConfigurationExpression.CreateMap<DALAppDTO.RoleAction, BLLAppDTO.RoleAction>();
            MapperConfigurationExpression.CreateMap<PublicApiV1.RoleAction, BLLAppDTO.RoleAction>();
            MapperConfigurationExpression.CreateMap<BLLAppDTO.RoleAction, PublicApiV1.RoleAction>();


            MapperConfigurationExpression.CreateMap<DALAppDTO.Identity.AppUserRole, BLLAppDTO.Identity.AppUserRole>();
            MapperConfigurationExpression.CreateMap<PublicApiV1.Identity.AppUserRole, BLLAppDTO.Identity.AppUserRole>();
            MapperConfigurationExpression.CreateMap<BLLAppDTO.Identity.AppUserRole, PublicApiV1.Identity.AppUserRole>();


            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));

        }
    }
}