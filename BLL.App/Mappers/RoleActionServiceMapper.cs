using AutoMapper;
using Contracts.BLL.App.Mappers;
using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;
using PublicApiV1=PublicApi.DTO.v1;
namespace BLL.App.Mappers
{
    public class RoleActionServiceMapper : BLLMapper<DALAppDTO.RoleAction, BLLAppDTO.RoleAction>, IRoleActionServiceMapper
    {
        public RoleActionServiceMapper()
        {
            MapperConfigurationExpression.CreateMap<DALAppDTO.MyAction, BLLAppDTO.MyAction>();
            MapperConfigurationExpression.CreateMap<PublicApiV1.MyAction, BLLAppDTO.MyAction>();
            
            MapperConfigurationExpression.CreateMap<DALAppDTO.RoleAction, BLLAppDTO.RoleAction>();
            MapperConfigurationExpression.CreateMap<PublicApiV1.RoleAction, BLLAppDTO.RoleAction>();

            MapperConfigurationExpression.CreateMap<DALAppDTO.Identity.AppRole, BLLAppDTO.Identity.AppRole>();
            MapperConfigurationExpression.CreateMap<PublicApiV1.Identity.AppRole, BLLAppDTO.Identity.AppRole>();

            MapperConfigurationExpression.CreateMap<DALAppDTO.RoleInKitchenView, BLLAppDTO.RoleInKitchenView>();
            MapperConfigurationExpression.CreateMap<PublicApiV1.RoleInKitchenView, BLLAppDTO.RoleInKitchenView>();

            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));

        }
    }
}