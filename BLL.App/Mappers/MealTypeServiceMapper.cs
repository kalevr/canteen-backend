using AutoMapper;
using Contracts.BLL.App.Mappers;
using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;
using PublicApiV1=PublicApi.DTO.v1;
namespace BLL.App.Mappers
{
    public class MealTypeServiceMapper : BLLMapper<DALAppDTO.MealType, BLLAppDTO.MealType>, IMealTypeServiceMapper
    {
        public MealTypeServiceMapper()
        {
            MapperConfigurationExpression.CreateMap<DALAppDTO.MealType, BLLAppDTO.MealType>();
            MapperConfigurationExpression.CreateMap<PublicApiV1.MealType, BLLAppDTO.MealType>();

            
            MapperConfigurationExpression.CreateMap<DALAppDTO.Kitchen, BLLAppDTO.Kitchen>();
            MapperConfigurationExpression.CreateMap<PublicApiV1.Kitchen, BLLAppDTO.Kitchen>();


            
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));

        }
    }
}