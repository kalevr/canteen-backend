using AutoMapper;
using DAL.Base.Mappers;

namespace BLL.App.Mappers
{
    public class BLLMapper<TLeftObject, TRightObject> : BaseDALMapper<TLeftObject, TRightObject>
        where TRightObject : class?, new()
        where TLeftObject : class?, new()
    {
        public BLLMapper() : base()
        { 
            // add more mappings
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Kitchen, BLL.App.DTO.Kitchen>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Kitchen, DAL.App.DTO.Kitchen>();

            MapperConfigurationExpression.CreateMap<DAL.App.DTO.LoginHistory, BLL.App.DTO.LoginHistory>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.LoginHistory, DAL.App.DTO.LoginHistory>();

            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Identity.AppUser, BLL.App.DTO.Identity.AppUser>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Identity.AppUser, DAL.App.DTO.Identity.AppUser>();
            
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Identity.AppRole, BLL.App.DTO.Identity.AppRole>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Identity.AppRole, DAL.App.DTO.Identity.AppRole>();

            MapperConfigurationExpression.CreateMap<DAL.App.DTO.MealType, BLL.App.DTO.MealType>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.MealType, DAL.App.DTO.MealType>();
   
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.RoleInKitchenView, BLL.App.DTO.RoleInKitchenView>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.RoleInKitchenView, DAL.App.DTO.RoleInKitchenView>();
            
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.KitchenView, BLL.App.DTO.KitchenView>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.KitchenView, DAL.App.DTO.KitchenView>();


            
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }
    }
}