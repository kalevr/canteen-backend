using AutoMapper;
using Contracts.BLL.App.Mappers;
using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;
using PublicApiV1=PublicApi.DTO.v1;

namespace BLL.App.Mappers
{
    public class KitchenViewServiceMapper : BLLMapper<DALAppDTO.KitchenView, BLLAppDTO.KitchenView>, IKitchenViewServiceMapper
    {
        public KitchenViewServiceMapper()
        {
            MapperConfigurationExpression.CreateMap<DALAppDTO.MealType, BLLAppDTO.MealType>();
            MapperConfigurationExpression.CreateMap<PublicApiV1.MealType, BLLAppDTO.MealType>();

            
            MapperConfigurationExpression.CreateMap<DALAppDTO.KitchenView, BLLAppDTO.KitchenView>();
            MapperConfigurationExpression.CreateMap<PublicApiV1.KitchenView, BLLAppDTO.KitchenView>();


            
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));

        }
    }
}