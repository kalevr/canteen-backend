using AutoMapper;
using Contracts.BLL.App.Mappers.Identity;
using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;
using PublicApiV1=PublicApi.DTO.v1;


namespace BLL.App.Mappers.Identity
{
    public class AppRoleServiceMapper : BLLMapper<DALAppDTO.Identity.AppRole, BLLAppDTO.Identity.AppRole>, IAppRoleServiceMapper
    {
        public AppRoleServiceMapper(): base()
        {
            MapperConfigurationExpression.CreateMap<DALAppDTO.Identity.AppRole, BLLAppDTO.Identity.AppRole>();
            MapperConfigurationExpression.CreateMap<PublicApiV1.Identity.AppRole, BLLAppDTO.Identity.AppRole>();

            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));

        }
    }
}