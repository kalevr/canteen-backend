using AutoMapper;
using Contracts.BLL.App.Mappers.Identity;
using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;
using PublicApiV1=PublicApi.DTO.v1;


namespace BLL.App.Mappers.Identity
{
    public class AppUserRoleServiceMapper : BLLMapper<DALAppDTO.Identity.AppUserRole, BLLAppDTO.Identity.AppUserRole>, IAppUserRoleServiceMapper
    {
        public AppUserRoleServiceMapper(): base()
        {
            
            MapperConfigurationExpression.CreateMap<DALAppDTO.Identity.AppUser, BLLAppDTO.Identity.AppUser>();
            MapperConfigurationExpression.CreateMap<PublicApiV1.Identity.AppUser, BLLAppDTO.Identity.AppUser>();

            MapperConfigurationExpression.CreateMap<DALAppDTO.Identity.AppRole, BLLAppDTO.Identity.AppRole>();
            MapperConfigurationExpression.CreateMap<PublicApiV1.Identity.AppRole, BLLAppDTO.Identity.AppRole>();

            MapperConfigurationExpression.CreateMap<DALAppDTO.Identity.AppUserRole, BLLAppDTO.Identity.AppUserRole>();
            MapperConfigurationExpression.CreateMap<PublicApiV1.Identity.AppUserRole, BLLAppDTO.Identity.AppUserRole>();


            
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));


        }
    }
}