using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Helpers;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using PublicApi.DTO.v1;
using Registration = BLL.App.DTO.Registration;
using V1DTO = PublicApi.DTO.v1;


namespace BLL.App.Services
{
    public class RegistrationService :
        BaseEntityService<IAppUnitOfWork, IRegistrationRepository, IRegistrationServiceMapper, DAL.App.DTO.Registration,
            BLL.App.DTO.Registration>, IRegistrationService
    {
        public RegistrationService(IAppUnitOfWork uow) : base(uow, uow.Registrations, new RegistrationServiceMapper())
        {
        }

        public virtual async Task<IEnumerable<Registration>> GetAllAsync(Guid groupId, Guid? userId = null,
            bool noTracking = true)
        {
            // to use companyId in next line, you must add custom repository interface in Contracts.DAL.APP Repositories
            return (await Repository.GetAllAsync(userId, noTracking)).Select(e => Mapper.Map<DAL.App.DTO.Registration,
                BLL.App.DTO.Registration>(e));
        }

        public virtual async Task<IEnumerable<Registration>> GetForRoleMealTypeForMonthAsync(Guid roleMealTypeId,
            DateTime startDate, DateTime endDate, bool noTracking = true)
        {
            return (await Repository.GetForRoleMealTypeForMonthAsync(roleMealTypeId, startDate, endDate, noTracking))
                .Select(e => Mapper.Map<DAL.App.DTO.Registration, BLL.App.DTO.Registration>(e));
        }

        public virtual async Task<Dictionary<string, int>> GetMealCountForGroupForMonthAsync(
            Guid mealTypeId, Guid roleId,
            DateTime startDate, DateTime endDate, bool noTracking = true)
        {
            return await Repository.GetMealCountForGroupForMonthAsync(mealTypeId, roleId, startDate, endDate,
                noTracking);
        }

        public Task<Dictionary<DateTime, int>> GetMealCountsPerDayForMonthAsync(
            Guid mealTypeId, List<Guid> roleIds, DateTime startDate, DateTime endDate, bool noTracking = true)
        {
            return Repository.GetMealCountsPerDayForMonthAsync(mealTypeId, roleIds, startDate, endDate, noTracking);
        }

        public virtual async Task<RegistrationSidebarViewModel> GetRegistrationsForGroupForMonthAsync(
            RegistrationSidebarViewModel regSidebar, List<Guid> roleIdList, Guid mealTypeId, Guid roleId,
            Guid roleMealTypeId, DateTime date)
        {
            regSidebar.CurrentMealTypeId = mealTypeId;
            regSidebar.RoleId = roleId;
            regSidebar.RoleMealTypeId = roleMealTypeId;
            regSidebar.ChosenDate = date;

            var roleMealTypes = await UOW.RoleMealTypes.GetAllForRoleAsync(
                regSidebar.CurrentMealTypeId, null!, false);
            var roleMealTypes2 =
                roleMealTypes.Select(e => Mapper.Map<DAL.App.DTO.RoleMealType, BLL.App.DTO.RoleMealType>(e));

            if (roleMealTypes != null)
            {
                regSidebar.MealName = roleMealTypes.FirstOrDefault(
                    m => m.MealTypeId == regSidebar.CurrentMealTypeId).MealType!.MealTypeDisplayName;
            }

            var myRoles = await UOW.AppRoles.GetMultipleByIdsAsync(roleIdList);
            var myRoleNames = myRoles.Select(r => r.Name).ToList();

            // check if user has extended edit right (that ignores time restrictions)
            if (UOW.AppUserRoles.HasSimilarRole("admin", myRoleNames)
                || UOW.AppUserRoles.HasSimilarRole("manager", myRoleNames)
                || UOW.AppUserRoles.HasSimilarRole("finance", myRoleNames))
            {
                regSidebar.HasEditRight = true;
                regSidebar.HasExtendedEditRight = true;
                regSidebar.AllCurrentRoleMealTypes = roleMealTypes2;
            }


            if (!regSidebar.HasExtendedEditRight)
            {
                var groups =
                    await UOW.RoleActions.GetAllForRolesAsync(roleIdList);

                // check if user has right to edit
                if (regSidebar.RoleId != Guid.Empty
                    && groups.Any(r => r.TargetAppRoleId == regSidebar.RoleId && r.Action!.MyActionName == "Write"))
                {
                    regSidebar.HasEditRight = true;
                }

                var roleIds = groups.Select(i => i.TargetAppRoleId).ToList() ?? new List<Guid?>();

                regSidebar.AllCurrentRoleMealTypes = roleMealTypes2.Where(e => roleIds.Contains(e.AppRoleId));
            }

            var tmpMealType = roleMealTypes2.FirstOrDefault(
                e => e.MealType!.Id == regSidebar.CurrentMealTypeId);
            regSidebar!.RegCutoff = tmpMealType!.MealType!.RegistrationCutoff;
            regSidebar.MealTime = tmpMealType.MealType.MealTime;

            if (regSidebar.ChosenDate == DateTime.MinValue)
            {
                regSidebar.ChosenDate = DateTime.Now;
            }

            if (regSidebar.RoleId != Guid.Empty)
            {
                regSidebar.HasContent = true;
                var (startDate, endDate) = StartEndDates.GetStartAndEndOfMonth(date);

                var allUsers = await UOW.AppUserRoles.GetAllForRoleAsync(regSidebar.RoleId);
                regSidebar.AppUsers = allUsers.Select(e =>
                    Mapper.Map<DAL.App.DTO.Identity.AppUserRole, BLL.App.DTO.Identity.AppUserRole>(e));
                var registrations = await GetForRoleMealTypeForMonthAsync(
                    regSidebar.RoleMealTypeId, startDate, endDate);

                regSidebar.Registrations = registrations;
                regSidebar.WorkingDays = WorkDays.GetWorkDays(regSidebar.ChosenDate, regSidebar.MealTime);
            }

            return regSidebar;
        }


        public virtual async Task<RegistrationsContainer> GetRegistrationsForGroupForMonthApiAsync(
            RegistrationsContainer regContainer, List<Guid> roleIdList, Guid mealTypeId, Guid roleId,
            Guid roleMealTypeId, DateTime date)
        {
            var regSidebar = new RegistrationSidebarViewModel();


            var registrations = await GetRegistrationsForGroupForMonthAsync(
                regSidebar, roleIdList, mealTypeId, roleId, roleMealTypeId, date);

            regContainer.ChosenDate = regSidebar.ChosenDate;
            regContainer.MealTime = regSidebar.ChosenDate;
            regContainer.RegCutoff = regSidebar.RegCutoff;
            regContainer.HasEditRight = regSidebar.HasEditRight;
            regContainer.HasExtendedEditRight = regSidebar.HasExtendedEditRight;
            regContainer.MealName = regSidebar.MealName;

            regContainer.RoleMealTypes = registrations.AllCurrentRoleMealTypes.Select(e =>
                Mapper.Map<BLL.App.DTO.RoleMealType, V1DTO.RoleMealType>(e));


            if (registrations.HasContent)
            {
                regContainer.RegistrationList = new Dictionary<string, Dictionary<string, int>>();
                foreach (var appUserRole in registrations.AppUsers.OrderBy(n => n.AppUser!.LastName))
                {
                    var tmp = new Dictionary<string, Dictionary<string, int>>();
                    var fedDays = new Dictionary<string, int>();

                    foreach (var day in regSidebar.WorkingDays)
                    {
                        fedDays.Add(day.Day.ToString(), 0);
                        if (registrations.Registrations.Any(e => e.MealDate.Day == day.Day
                                                                 && e.AppUser!.FirstLastName ==
                                                                 appUserRole.AppUser!.FirstLastName))
                        {
                            fedDays[day.Day.ToString()] = 1;
                        }
                        else
                        {
                            fedDays[day.Day.ToString()] = 0;
                        }
                    }
                    
                    regContainer.RegistrationList.Add(appUserRole.AppUser!.FirstLastName ?? "unknown", fedDays);
                }
            }


            return regContainer;
        }


        public virtual async Task<Registration> UpdateRegistrations(List<string> selection, string username,
            RegistrationSidebarViewModel regSidebar, Guid userId, Guid mealTypeId, Guid roleId,
            Guid roleMealTypeId, DateTime date)
        {
            regSidebar.CurrentMealTypeId = mealTypeId;
            regSidebar.RoleId = roleId;
            regSidebar.RoleMealTypeId = roleMealTypeId;
            regSidebar.ChosenDate = date;

            var userRoles = await UOW.AppUserRoles.GetAllAsync(userId);
            var roleIdList = userRoles.Select(i => i.RoleId).ToList();
            var roleNameList = userRoles.Select(n => n.AppRole!.Name).ToList();
            var rightToChange = UOW.RoleActions.UserHasAction(roleIdList, regSidebar.RoleId, "Write").Result;
            var isAdmin = UOW.AppUserRoles.HasSimilarRole(C.SESS_ADMIN_NAME, roleNameList)
                          || UOW.AppUserRoles.HasSimilarRole(C.SESS_MANAGER_NAME, roleNameList);

            if (!isAdmin && !rightToChange)
            {
                return new Registration();
            }

            var (startDate, endDate) = StartEndDates.GetStartAndEndOfMonth(date);

            var registrations = await GetForRoleMealTypeForMonthAsync(
                regSidebar.RoleMealTypeId, startDate, endDate);

            var dayUsers = SeparateDayAndGuid(selection);
            var allUsers = await UOW.AppUserRoles.GetAllForRoleAsync(regSidebar.RoleId);
            regSidebar.MealTime = UOW.MealTypes.FirstOrDefaultAsync(regSidebar.CurrentMealTypeId).Result.MealTime;

            foreach (var user in allUsers.Select(e => e.AppUser))
            {
                var workDays = WorkDays.GetWorkDays(regSidebar.ChosenDate, regSidebar.MealTime);
                foreach (var day in workDays)
                {
                    var key = int.Parse(day.Day.ToString());
                    if (dayUsers.ContainsKey(key) && dayUsers[key].Contains(user!.Id) &&
                        !registrations.Any(u => u.AppUserId == user!.Id && u.MealDate.Day == day.Day))
                    {
                        Repository.Add(new DAL.App.DTO.Registration()
                        {
                            AppUserId = user.Id,
                            MealDate = new DateTime(regSidebar.ChosenDate.Year, regSidebar.ChosenDate.Month, day.Day,
                                regSidebar.MealTime.Hour, regSidebar.MealTime.Minute, regSidebar.MealTime.Second),
                            RoleMealTypeId = regSidebar.RoleMealTypeId
                        });
                    }
                    else if (((dayUsers.ContainsKey(key) && !dayUsers[key].Contains(user!.Id)) ||
                              !dayUsers.ContainsKey(key)) &&
                             registrations.Any(u => u.AppUserId == user!.Id && u.MealDate.Day == day.Day))
                    {
                        var id = registrations.FirstOrDefault(u =>
                            u.AppUserId == user!.Id && u.MealDate.Day == day.Day);
                        await Repository.RemoveAsync(new DAL.App.DTO.Registration()
                        {
                            Id = id!.Id,
                            AppUserId = user!.Id,
                            MealDate = new DateTime(regSidebar.ChosenDate.Year, regSidebar.ChosenDate.Month, day.Day),
                            RoleMealTypeId = regSidebar.RoleMealTypeId
                        });
                    }
                }
            }

            await UOW.SaveChangesAsync();
            return new Registration();
        }


        public virtual async Task<FinancialReportSidebarView> GetFinancialReport(
            FinancialReportSidebarView frSidebar, Guid mealTypeId, DateTime date)
        {
            if (date == DateTime.MinValue)
            {
                date = DateTime.Now;
            }

            frSidebar.ChosenDate = date;
            frSidebar.MealTypeId = mealTypeId;
            var companyRoleMealTypes = await UOW.RoleMealTypes.GetAllAsync(null);
            var mealTypes = companyRoleMealTypes.Select(mt =>
                Mapper.Map<DAL.App.DTO.MealType, BLL.App.DTO.MealType>(mt.MealType!));
            frSidebar.MealTypes = mealTypes.GroupBy(mt => mt.MealTypeDisplayName)
                .Select(mt => mt.First()).OrderBy(mt => mt.MealTypeName);

            if (mealTypeId != Guid.Empty)
            {
                var (startDate, endDate) = StartEndDates.GetStartAndEndOfMonth(date);

                //var allRegs = await Repository.GetMealCountForGroupForMonthAsync(mealTypeId, startDate, endDate);
                var allRegs = new Dictionary<string, Dictionary<string, int>>();

                var roleMealTypes = (await UOW.RoleMealTypes.GetAllForRoleAsync(mealTypeId))
                    .Select(e => Mapper.Map<DAL.App.DTO.RoleMealType, BLL.App.DTO.RoleMealType>(e));

                foreach (var roleMealType in roleMealTypes)
                {
                    var roleRegs = await Repository.GetMealCountForGroupForMonthAsync(
                        mealTypeId, roleMealType.AppRoleId, startDate, endDate);
                    allRegs.Add(roleMealType.AppRole!.DisplayName, roleRegs);
                }

                var roleIds = roleMealTypes.Select(r => r.AppRole!.Id).ToList();
                frSidebar.AppUserRoles = (await UOW.AppUserRoles.GetAllWithRolesAsync(roleIds)).Select(e =>
                    Mapper.Map<DAL.App.DTO.Identity.AppUserRole, BLL.App.DTO.Identity.AppUserRole>(e)).ToList();

                frSidebar.RegStats = allRegs;
            }

            return frSidebar;
        }


        private Dictionary<int, List<Guid>> SeparateDayAndGuid(List<string> selection)
        {
            var dict = new Dictionary<int, List<Guid>>();

            foreach (var item in selection)
            {
                var items = item.Split(" ");
                var userId = Guid.Parse(items[0]);
                var day = int.Parse(items[1]);

                if (!dict.ContainsKey(day))
                {
                    dict[day] = new List<Guid>() {userId,};
                }
                else
                {
                    dict[day].Add(userId);
                }
            }

            return dict;
        }
    }
}