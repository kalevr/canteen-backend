using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO.Identity;

namespace BLL.App.Services
{
    public class LoginHistoryService :
        BaseEntityService<IAppUnitOfWork, ILoginHistoryRepository, ILoginHistoryServiceMapper, DAL.App.DTO.LoginHistory,
            BLL.App.DTO.LoginHistory>, ILoginHistoryService
    {
        public LoginHistoryService(IAppUnitOfWork uow) : base(uow, uow.LoginHistories, new LoginHistoryServiceMapper())
        {
        }

        public virtual async Task<IEnumerable<LoginHistory>> GetAllAsync(Guid? userId = null,
            bool noTracking = true)
        {
            return (await Repository.GetAllAsync(userId, noTracking)).Select(e => Mapper.Map<DAL.App.DTO.LoginHistory,
                BLL.App.DTO.LoginHistory>(e));
        }

        public virtual async Task<LoginHistory> FirstOrDefaultAsync(Guid id, Guid? userId = null,
            bool noTracking = true)
        {
            var loginHistory = (await Repository.FirstOrDefaultAsync(id, userId, noTracking));
            return Mapper.Map<DAL.App.DTO.LoginHistory,
                BLL.App.DTO.LoginHistory>(loginHistory);
        }

        public virtual async Task<DAL.App.DTO.LoginHistory> WtfAmIDoing(Guid id, Guid? userId = null,
            bool noTracking = true)
        {
            var loginHistory = (await Repository.FirstOrDefaultAsync(id, userId, noTracking));

            var mapped = new DAL.App.DTO.LoginHistory()
            {
                Id = loginHistory.Id, Ipv4 = loginHistory.Ipv4, Ipv6 = loginHistory.Ipv6,
                Success = loginHistory.Success,
                AppUserId = loginHistory.AppUserId, LoginTime = loginHistory.LoginTime,
                UserAgent = loginHistory.UserAgent,
                UserName = loginHistory.UserName
            };

            if (loginHistory.AppUser != null)
            {
                mapped.AppUser = new AppUser()
                {
                    Id = loginHistory.AppUser.Id,
                    Email = loginHistory.AppUser.Email,
                    FirstName = loginHistory.AppUser.FirstName,
                    LastName = loginHistory.AppUser.LastName,
                    UserName = loginHistory.AppUser.UserName
                };
            }

            return mapped;
        }
    }
}