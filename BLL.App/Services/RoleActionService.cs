using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class RoleActionService :
        BaseEntityService<IAppUnitOfWork, IRoleActionRepository, IRoleActionServiceMapper, DAL.App.DTO.RoleAction,
            BLL.App.DTO.RoleAction>, IRoleActionService
    {
        public RoleActionService(IAppUnitOfWork uow) : base(uow, uow.RoleActions, new RoleActionServiceMapper())
        {
        }

        public virtual async Task<IEnumerable<RoleAction>> GetAllAsync(Guid? userId = null,
            bool noTracking = true)
        {
            return (await Repository.GetAllAsync(userId, noTracking)).Select(e => Mapper.Map<DAL.App.DTO.RoleAction,
                BLL.App.DTO.RoleAction>(e));
        }

        public virtual async Task<IEnumerable<RoleAction>> GetAllForRolesAsync(List<Guid> roleIds, bool noTracking = true)
        {
            return (await Repository.GetAllForRolesAsync(roleIds, noTracking)).Select(e => Mapper.Map<DAL.App.DTO.RoleAction,
                BLL.App.DTO.RoleAction>(e));
        }

        public virtual async Task<RoleAction> FirstOrDefaultAsync(Guid id, Guid? userId = null, bool noTracking = true)
        {
            var roleAction = (await Repository.FirstOrDefaultAsync(id, userId, noTracking));
            return Mapper.Map<DAL.App.DTO.RoleAction,
                BLL.App.DTO.RoleAction>(roleAction);
        }

        public virtual async Task<bool> UserHasAction(List<Guid> userRoleIds, Guid targetRoleId, string actionName,
            bool noTracking = true)
        {
            return await Repository.UserHasAction(userRoleIds, targetRoleId, actionName, noTracking);
        }
    }
}