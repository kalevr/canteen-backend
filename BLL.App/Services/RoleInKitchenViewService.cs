using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Helpers;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class RoleInKitchenViewService :
        BaseEntityService<IAppUnitOfWork, IRoleInKitchenViewRepository, IRoleInKitchenViewServiceMapper, DAL.App.DTO.RoleInKitchenView,
            BLL.App.DTO.RoleInKitchenView>, IRoleInKitchenViewService
    {
        public RoleInKitchenViewService(IAppUnitOfWork uow) : base(uow, uow.RoleInKitchenViews, new RoleInKitchenViewServiceMapper())
        {
        }

        public virtual async Task<IEnumerable<RoleInKitchenView>> GetAllAsync(
            Guid? userId = null, bool noTracking = true)
        {
            return (await Repository.GetAllAsync(userId, noTracking)).Select(e => 
                Mapper.Map<DAL.App.DTO.RoleInKitchenView, BLL.App.DTO.RoleInKitchenView>(e));
        }
        
        public virtual async Task<RoleInKitchenView> FirstOrDefaultAsync(
            Guid id, Guid? userId = null, bool noTracking = true)
        {
            var roleInKitchenView = (await Repository.FirstOrDefaultAsync(id, userId, noTracking));
            return Mapper.Map<DAL.App.DTO.RoleInKitchenView,
                BLL.App.DTO.RoleInKitchenView>(roleInKitchenView);
        }

        public virtual async Task<IEnumerable<RoleInKitchenView>> GetAllForKitchenViewAsync(
            Guid kitchenViewId, bool noTracking = true)
        {
            return (await Repository.GetAllAsync(kitchenViewId, noTracking)).Select(e => 
                Mapper.Map<DAL.App.DTO.RoleInKitchenView, BLL.App.DTO.RoleInKitchenView>(e));
        }

        public virtual async Task<RoleInKitchenViewSidebarViewModel> GetMealStatsForMonth(
            RoleInKitchenViewSidebarViewModel rkvSidebar, DateTime date,Guid mealTypeId)
        {
            rkvSidebar.ChosenDate = date;
            rkvSidebar.MealTypeId = mealTypeId;
            
            var kitchens = await UOW.Kitchens.GetAllAsync(null);
            var kitchenIds = kitchens.Select(k => k.Id).ToList();
            var allKitchenViews = await UOW.KitchenViews.GetAllAsync(kitchenIds);

            rkvSidebar.KitchenViews = Mapper.Map<List<DAL.App.DTO.KitchenView>, List<BLL.App.DTO.KitchenView>>(
                allKitchenViews.GroupBy(n => n.MealType!.MealTypeDisplayName)
                    .Select(kv => kv.First()).ToList());

            if (rkvSidebar.ChosenDate == DateTime.MinValue)
            {
                rkvSidebar.ChosenDate = DateTime.Now;
            }

            if (rkvSidebar.MealTypeId != Guid.Empty && allKitchenViews != null)
            {
                rkvSidebar.WorkingDays = WorkDays.GetWorkDays(rkvSidebar.ChosenDate);
                var (startDate, endDate) = StartEndDates.GetStartAndEndOfMonth(date);

                var currentKitchenViews = allKitchenViews.Where(kv => kv.MealTypeId == rkvSidebar.MealTypeId);
                foreach (var kitchenView in currentKitchenViews)
                {
                    var rolesInKv = await Repository.GetAllForKitchenViewAsync(kitchenView.Id);
                    if (rolesInKv != null)
                    {
                        rkvSidebar.MealSummary.Add(kitchenView.KitchenViewDisplayName, new Dictionary<DateTime, int>());
                        var roleIds = rolesInKv.Select(id => id.AppRole!.Id).ToList();
                        var stats = await UOW.Registrations.GetMealCountsPerDayForMonthAsync(
                            rkvSidebar.MealTypeId, roleIds, startDate, endDate);
                        if (stats != null)
                        {
                            var wtfName = kitchenView.KitchenViewDisplayName;
                            var filthyStats = stats;
                            rkvSidebar.MealSummary[wtfName] = filthyStats;
                        }
                    }
                    
                }
            }
            
            return rkvSidebar;
        }
    }
}