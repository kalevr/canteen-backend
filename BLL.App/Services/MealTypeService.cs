using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class MealTypeService :
        BaseEntityService<IAppUnitOfWork, IMealTypeRepository, IMealTypeServiceMapper, DAL.App.DTO.MealType,
            BLL.App.DTO.MealType>, IMealTypeService
    {
        public MealTypeService(IAppUnitOfWork uow) : base(uow, uow.MealTypes, new MealTypeServiceMapper())
        {
        }

        public virtual async Task<IEnumerable<MealType>> GetAllAsync(Guid? userId = null,
            bool noTracking = true)
        {
            return (await Repository.GetAllAsync(userId, noTracking)).Select(e => Mapper.Map<DAL.App.DTO.MealType,
                BLL.App.DTO.MealType>(e));
        }
        
        public virtual async Task<MealType> FirstOrDefaultAsync(Guid id, Guid? userId = null, bool noTracking = true)
        {
            var mealType = (await Repository.FirstOrDefaultAsync(id, userId, noTracking));
            return Mapper.Map<DAL.App.DTO.MealType,
                BLL.App.DTO.MealType>(mealType);
        }

        public virtual async Task<IEnumerable<MealType>> GetAllForKitchenAsync(List<Guid> mealTypeIds, bool noTracking = true)
        {
            return (await Repository.GetAllForKitchenAsync(mealTypeIds, noTracking)).Select(e => Mapper.Map<DAL.App.DTO.MealType,
                BLL.App.DTO.MealType>(e));
        }

        public virtual async Task<IEnumerable<MealType>> GetAllByIdsAsync(IReadOnlyList<Guid> mealTypeIds, Guid? userId = null, bool noTracking = true)
        {
            return (await Repository.GetAllByIdsAsync(mealTypeIds, userId, noTracking)).Select(e =>
                Mapper.Map<DAL.App.DTO.MealType, BLL.App.DTO.MealType>(e));
        }
    }
}