using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class KitchenViewService :
        BaseEntityService<IAppUnitOfWork, IKitchenViewRepository, IKitchenViewServiceMapper, DAL.App.DTO.KitchenView,
            BLL.App.DTO.KitchenView>, IKitchenViewService
    {
        public KitchenViewService(IAppUnitOfWork uow) : base(uow, uow.KitchenViews, new KitchenViewServiceMapper())
        {
        }

        public virtual async Task<IEnumerable<KitchenView>> GetAllAsync(Guid? userId = null,
            bool noTracking = true)
        {
            return (await Repository.GetAllAsync(userId, noTracking)).Select(
                e => Mapper.Map<DAL.App.DTO.KitchenView, BLL.App.DTO.KitchenView>(e));
        }

        public virtual async Task<KitchenView> FirstOrDefaultAsync(Guid id, Guid? userId = null, bool noTracking = true)
        {
            var kitchenView = (await Repository.FirstOrDefaultAsync(id, userId, noTracking));
            return Mapper.Map<DAL.App.DTO.KitchenView,
                BLL.App.DTO.KitchenView>(kitchenView);
        }
        
        public virtual async Task<IEnumerable<KitchenView>> GetAllByIdsAsync(IReadOnlyList<Guid> kitchenViewIds, 
            Guid? userId = null, bool noTracking = true)
        {
            return (await Repository.GetAllByIdsAsync(kitchenViewIds, userId, noTracking)).Select(e =>
                Mapper.Map<DAL.App.DTO.KitchenView, BLL.App.DTO.KitchenView>(e));
        }
    }
}