using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class MyActionService :
        BaseEntityService<IAppUnitOfWork, IMyActionRepository, IMyActionServiceMapper, DAL.App.DTO.MyAction,
            BLL.App.DTO.MyAction>, IMyActionService
    {
        public MyActionService(IAppUnitOfWork uow) : base(uow, uow.MyActions, new MyActionServiceMapper())
        {
        }

        public virtual async Task<IEnumerable<MyAction>> GetAllAsync(Guid? userId = null,
            bool noTracking = true)
        {
            return (await Repository.GetAllAsync(userId, noTracking)).Select(e => Mapper.Map<DAL.App.DTO.MyAction,
                BLL.App.DTO.MyAction>(e));
        }
    }
}