using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Helpers;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using PublicApi.DTO.v1;
using Participation = BLL.App.DTO.Participation;
using V1DTO = PublicApi.DTO.v1;


namespace BLL.App.Services
{
    public class ParticipationService :
        BaseEntityService<IAppUnitOfWork, IParticipationRepository, IParticipationServiceMapper, DAL.App.DTO.Participation,
            BLL.App.DTO.Participation>, IParticipationService
    {
        public ParticipationService(IAppUnitOfWork uow) : base(uow, uow.Participations, new ParticipationServiceMapper())
        {
        }

        public virtual async Task<IEnumerable<Participation>> GetAllAsync(Guid groupId, Guid? userId = null,
            bool noTracking = true)
        {
            return (await Repository.GetAllAsync(userId, noTracking)).Select(e => Mapper.Map<DAL.App.DTO.Participation,
                BLL.App.DTO.Participation>(e));
        }

        public virtual async Task<IEnumerable<Participation>> GetForRoleMealTypeForMonthAsync(Guid roleMealTypeId,
            DateTime startDate, DateTime endDate, bool noTracking = true)
        {
            return (await Repository.GetForRoleMealTypeForMonthAsync(roleMealTypeId, startDate, endDate, noTracking))
                .Select(e => Mapper.Map<DAL.App.DTO.Participation, BLL.App.DTO.Participation>(e));
        }

        public virtual async Task<Dictionary<string, int>> GetMealCountForGroupForMonthAsync(
            Guid mealTypeId, Guid roleId,
            DateTime startDate, DateTime endDate, bool noTracking = true)
        {
            return await Repository.GetMealCountForGroupForMonthAsync(mealTypeId, roleId, startDate, endDate,
                noTracking);
        }

        public Task<Dictionary<DateTime, int>> GetMealCountsPerDayForMonthAsync(
            Guid mealTypeId, List<Guid> roleIds, DateTime startDate, DateTime endDate, bool noTracking = true)
        {
            return Repository.GetMealCountsPerDayForMonthAsync(mealTypeId, roleIds, startDate, endDate, noTracking);
        }

        public virtual async Task<ParticipationSidebarViewModel> GetParticipationsForGroupForMonthAsync(
            ParticipationSidebarViewModel prtSidebar, List<Guid> roleIdList, Guid mealTypeId, Guid roleId,
            Guid roleMealTypeId, DateTime date)
        {
            prtSidebar.CurrentMealTypeId = mealTypeId;
            prtSidebar.RoleId = roleId;
            prtSidebar.RoleMealTypeId = roleMealTypeId;
            prtSidebar.ChosenDate = date;

            var roleMealTypes = await UOW.RoleMealTypes.GetAllForRoleAsync(
                prtSidebar.CurrentMealTypeId, null!, false);
            var roleMealTypes2 =
                roleMealTypes.Select(e => Mapper.Map<DAL.App.DTO.RoleMealType, BLL.App.DTO.RoleMealType>(e));

            if (roleMealTypes != null)
            {
                prtSidebar.MealName = roleMealTypes.FirstOrDefault(
                    m => m.MealTypeId == prtSidebar.CurrentMealTypeId).MealType!.MealTypeDisplayName;
            }

            var myRoles = await UOW.AppRoles.GetMultipleByIdsAsync(roleIdList);
            var myRoleNames = myRoles.Select(r => r.Name).ToList();

            // check if user has extended edit right (that ignores time restrictions)
            if (UOW.AppUserRoles.HasSimilarRole("admin", myRoleNames)
                || UOW.AppUserRoles.HasSimilarRole("manager", myRoleNames)
                || UOW.AppUserRoles.HasSimilarRole("finance", myRoleNames))
            {
                prtSidebar.HasEditRight = true;
                prtSidebar.HasExtendedEditRight = true;
                prtSidebar.AllCurrentRoleMealTypes = roleMealTypes2;
            }


            if (!prtSidebar.HasExtendedEditRight)
            {
                var groups =
                    await UOW.RoleActions.GetAllForRolesAsync(roleIdList);

                // check if user has right to edit
                if (prtSidebar.RoleId != Guid.Empty
                    && groups.Any(r => r.TargetAppRoleId == prtSidebar.RoleId && r.Action!.MyActionName == "Write"))
                {
                    prtSidebar.HasEditRight = true;
                }

                var roleIds = groups.Select(i => i.TargetAppRoleId).ToList() ?? new List<Guid?>();

                prtSidebar.AllCurrentRoleMealTypes = roleMealTypes2.Where(e => roleIds.Contains(e.AppRoleId));
            }

            var tmpMealType = roleMealTypes2.FirstOrDefault(
                e => e.MealType!.Id == prtSidebar.CurrentMealTypeId);
            prtSidebar!.RegCutoff = tmpMealType!.MealType!.ParticipationCutoff;
            prtSidebar.MealTime = tmpMealType.MealType.MealTime;

            if (prtSidebar.ChosenDate == DateTime.MinValue)
            {
                prtSidebar.ChosenDate = DateTime.Now;
            }

            if (prtSidebar.RoleId != Guid.Empty)
            {
                prtSidebar.HasContent = true;
                var (startDate, endDate) = StartEndDates.GetStartAndEndOfMonth(date);

                var allUsers = await UOW.AppUserRoles.GetAllForRoleAsync(prtSidebar.RoleId);
                prtSidebar.AppUsers = allUsers.Select(e =>
                    Mapper.Map<DAL.App.DTO.Identity.AppUserRole, BLL.App.DTO.Identity.AppUserRole>(e));
                var participations = await GetForRoleMealTypeForMonthAsync(
                    prtSidebar.RoleMealTypeId, startDate, endDate);

                prtSidebar.Participations = participations;
                prtSidebar.WorkingDays = WorkDays.GetWorkDays(prtSidebar.ChosenDate, prtSidebar.MealTime);
            }

            return prtSidebar;
        }


        public virtual async Task<ParticipationsContainer> GetParticipationsForGroupForMonthApiAsync(
            ParticipationsContainer prtContainer, List<Guid> roleIdList, Guid mealTypeId, Guid roleId,
            Guid roleMealTypeId, DateTime date)
        {
            var prtSidebar = new ParticipationSidebarViewModel();


            var participations = await GetParticipationsForGroupForMonthAsync(
                prtSidebar, roleIdList, mealTypeId, roleId, roleMealTypeId, date);

            prtContainer.ChosenDate = prtSidebar.ChosenDate;
            prtContainer.MealTime = prtSidebar.ChosenDate;
            prtContainer.RegCutoff = prtSidebar.RegCutoff;
            prtContainer.HasEditRight = prtSidebar.HasEditRight;
            prtContainer.HasExtendedEditRight = prtSidebar.HasExtendedEditRight;
            prtContainer.MealName = prtSidebar.MealName;

            prtContainer.RoleMealTypes = participations.AllCurrentRoleMealTypes.Select(e =>
                Mapper.Map<BLL.App.DTO.RoleMealType, V1DTO.RoleMealType>(e));


            if (participations.HasContent)
            {
                prtContainer.ParticipationList = new Dictionary<string, Dictionary<string, int>>();
                foreach (var appUserRole in participations.AppUsers.OrderBy(n => n.AppUser!.LastName))
                {
                    var tmp = new Dictionary<string, Dictionary<string, int>>();
                    var fedDays = new Dictionary<string, int>();

                    foreach (var day in prtSidebar.WorkingDays)
                    {
                        fedDays.Add(day.Day.ToString(), 0);
                        if (participations.Participations.Any(e => e.MealDate.Day == day.Day
                                                                  && e.AppUser!.FirstLastName ==
                                                                  appUserRole.AppUser!.FirstLastName))
                        {
                            fedDays[day.Day.ToString()] = 1;
                        }
                        else
                        {
                            fedDays[day.Day.ToString()] = 0;
                        }
                    }
                    
                    prtContainer.ParticipationList.Add(appUserRole.AppUser!.FirstLastName ?? "unknown", fedDays);
                }
            }


            return prtContainer;
        }


        public virtual async Task<Participation> UpdateParticipations(List<string> selection, string username,
            ParticipationSidebarViewModel prtSidebar, Guid userId, Guid mealTypeId, Guid roleId,
            Guid roleMealTypeId, DateTime date)
        {
            prtSidebar.CurrentMealTypeId = mealTypeId;
            prtSidebar.RoleId = roleId;
            prtSidebar.RoleMealTypeId = roleMealTypeId;
            prtSidebar.ChosenDate = date;

            var userRoles = await UOW.AppUserRoles.GetAllAsync(userId);
            var roleIdList = userRoles.Select(i => i.RoleId).ToList();
            var roleNameList = userRoles.Select(n => n.AppRole!.Name).ToList();
            var rightToChange = UOW.RoleActions.UserHasAction(roleIdList, prtSidebar.RoleId, "Write").Result;
            var isAdmin = UOW.AppUserRoles.HasSimilarRole(C.SESS_ADMIN_NAME, roleNameList)
                          || UOW.AppUserRoles.HasSimilarRole(C.SESS_MANAGER_NAME, roleNameList);

            if (!isAdmin && !rightToChange)
            {
                return new Participation();
            }

            var (startDate, endDate) = StartEndDates.GetStartAndEndOfMonth(date);

            var participations = await GetForRoleMealTypeForMonthAsync(
                prtSidebar.RoleMealTypeId, startDate, endDate);

            var dayUsers = SeparateDayAndGuid(selection);
            var allUsers = await UOW.AppUserRoles.GetAllForRoleAsync(prtSidebar.RoleId);
            prtSidebar.MealTime = UOW.MealTypes.FirstOrDefaultAsync(prtSidebar.CurrentMealTypeId).Result.MealTime;

            foreach (var user in allUsers.Select(e => e.AppUser))
            {
                var workDays = WorkDays.GetWorkDays(prtSidebar.ChosenDate, prtSidebar.MealTime);
                foreach (var day in workDays)
                {
                    var key = int.Parse(day.Day.ToString());
                    if (dayUsers.ContainsKey(key) && dayUsers[key].Contains(user!.Id) &&
                        !participations.Any(u => u.AppUserId == user!.Id && u.MealDate.Day == day.Day))
                    {
                        Repository.Add(new DAL.App.DTO.Participation()
                        {
                            AppUserId = user.Id,
                            MealDate = new DateTime(prtSidebar.ChosenDate.Year, prtSidebar.ChosenDate.Month, day.Day,
                                prtSidebar.MealTime.Hour, prtSidebar.MealTime.Minute, prtSidebar.MealTime.Second),
                            RoleMealTypeId = prtSidebar.RoleMealTypeId
                        });
                    }
                    else if (((dayUsers.ContainsKey(key) && !dayUsers[key].Contains(user!.Id)) ||
                              !dayUsers.ContainsKey(key)) &&
                             participations.Any(u => u.AppUserId == user!.Id && u.MealDate.Day == day.Day))
                    {
                        var id = participations.FirstOrDefault(u =>
                            u.AppUserId == user!.Id && u.MealDate.Day == day.Day);
                        await Repository.RemoveAsync(new DAL.App.DTO.Participation()
                        {
                            Id = id!.Id,
                            AppUserId = user!.Id,
                            MealDate = new DateTime(prtSidebar.ChosenDate.Year, prtSidebar.ChosenDate.Month, day.Day),
                            RoleMealTypeId = prtSidebar.RoleMealTypeId
                        });
                    }
                }
            }

            await UOW.SaveChangesAsync();
            return new Participation();
        }


        public virtual async Task<FinancialReportSidebarView> GetFinancialReport(
            FinancialReportSidebarView frSidebar, Guid mealTypeId, DateTime date)
        {
            if (date == DateTime.MinValue)
            {
                date = DateTime.Now;
            }

            frSidebar.ChosenDate = date;
            frSidebar.MealTypeId = mealTypeId;
            var companyRoleMealTypes = await UOW.RoleMealTypes.GetAllAsync(null);
            var mealTypes = companyRoleMealTypes.Select(mt => Mapper.Map<DAL.App.DTO.MealType, BLL.App.DTO.MealType>(mt.MealType!));
            frSidebar.MealTypes = mealTypes.GroupBy(mt => mt.MealTypeDisplayName)
                .Select(mt => mt.First()).OrderBy(mt => mt.MealTypeName);

            if (mealTypeId != Guid.Empty)
            {
                var (startDate, endDate) = StartEndDates.GetStartAndEndOfMonth(date);

                //var allRegs = await Repository.GetMealCountForGroupForMonthAsync(mealTypeId, startDate, endDate);
                var allRegs = new Dictionary<string, Dictionary<string, int>>();

                var roleMealTypes = (await UOW.RoleMealTypes.GetAllForRoleAsync(mealTypeId))
                    .Select(e => Mapper.Map<DAL.App.DTO.RoleMealType, BLL.App.DTO.RoleMealType>(e));

                foreach (var roleMealType in roleMealTypes)
                {
                    var roleRegs = await Repository.GetMealCountForGroupForMonthAsync(
                        mealTypeId, roleMealType.AppRoleId, startDate, endDate);
                    allRegs.Add(roleMealType.AppRole!.DisplayName, roleRegs);
                }

                var roleIds = roleMealTypes.Select(r => r.AppRole!.Id).ToList();
                frSidebar.AppUserRoles = (await UOW.AppUserRoles.GetAllWithRolesAsync(roleIds)).Select(e =>
                    Mapper.Map<DAL.App.DTO.Identity.AppUserRole, BLL.App.DTO.Identity.AppUserRole>(e)).ToList();

                frSidebar.RegStats = allRegs;
            }

            return frSidebar;
        }


        private Dictionary<int, List<Guid>> SeparateDayAndGuid(List<string> selection)
        {
            var dict = new Dictionary<int, List<Guid>>();

            foreach (var item in selection)
            {
                var items = item.Split(" ");
                var userId = Guid.Parse(items[0]);
                var day = int.Parse(items[1]);

                if (!dict.ContainsKey(day))
                {
                    dict[day] = new List<Guid>() {userId,};
                }
                else
                {
                    dict[day].Add(userId);
                }
            }

            return dict;
        }
    }
}