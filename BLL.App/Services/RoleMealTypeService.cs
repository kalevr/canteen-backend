using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class RoleMealTypeService :
        BaseEntityService<IAppUnitOfWork, IRoleMealTypeRepository, IRoleMealTypeServiceMapper, DAL.App.DTO.RoleMealType,
            BLL.App.DTO.RoleMealType>, IRoleMealTypeService
    {
        public RoleMealTypeService(IAppUnitOfWork uow) : base(uow, uow.RoleMealTypes, new RoleMealTypeServiceMapper())
        {
        }

        public virtual async Task<IEnumerable<RoleMealType>> GetAllAsync(Guid? userId = null,
            bool noTracking = true)
        {
            return (await Repository.GetAllAsync(userId, noTracking)).Select(e => Mapper.Map<DAL.App.DTO.RoleMealType,
                BLL.App.DTO.RoleMealType>(e));
        }

        public virtual async Task<IEnumerable<RoleMealType>> GetAllForRoleAsync(Guid roleId, List<Guid?>? appRoles, bool noTracking = true)
        {
            return (await Repository.GetAllForRoleAsync(roleId, appRoles, noTracking)).Select(e => 
                Mapper.Map<DAL.App.DTO.RoleMealType, BLL.App.DTO.RoleMealType>(e));
        }

        public virtual async Task<RoleMealType> FirstOrDefaultAsync(Guid id, Guid? userId = null,
            bool noTracking = true)
        {
            var roleMealType = (await Repository.FirstOrDefaultAsync(id, userId, noTracking));
            return Mapper.Map<DAL.App.DTO.RoleMealType,
                BLL.App.DTO.RoleMealType>(roleMealType);
        }

       
        public virtual async Task<IEnumerable<RoleMealType>> GetAllWithNamesAsync(Guid? userId = null,
            bool noTracking = true)
        {
            var data = (await Repository.GetAllAsync(userId, noTracking)).Select(e => Mapper
                .Map<DAL.App.DTO.RoleMealType,
                    BLL.App.DTO.RoleMealType>(e)).Select(e =>
            {
                e.MealTypeName = e.MealType?.MealTypeName;
                return e;
            }).ToList(); // .ToList() to evaluate .Select immediately (or so i read).

            return data;
        }
    }
}