using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class KitchenService :
        BaseEntityService<IAppUnitOfWork, IKitchenRepository, IKitchenServiceMapper, DAL.App.DTO.Kitchen,
            BLL.App.DTO.Kitchen>, IKitchenService
    {
        public KitchenService(IAppUnitOfWork uow) : base(uow, uow.Kitchens, new KitchenServiceMapper())
        {
        }

        public virtual async Task<IEnumerable<Kitchen>> GetAllAsync(Guid? userId = null,
            bool noTracking = true)
        {
            return (await Repository.GetAllAsync(userId, noTracking)).Select(e => 
                Mapper.Map<DAL.App.DTO.Kitchen, BLL.App.DTO.Kitchen>(e));
        }

        public virtual async Task<IEnumerable<Kitchen>> GetAllByIdsAsync(IReadOnlyList<Guid> kitchenIds, Guid? userId = null,
            bool noTracking = true)
        {
            return (await Repository.GetAllByIdsAsync(kitchenIds, userId, noTracking)).Select(e =>
                Mapper.Map<DAL.App.DTO.Kitchen, BLL.App.DTO.Kitchen>(e));
        }
    }
}