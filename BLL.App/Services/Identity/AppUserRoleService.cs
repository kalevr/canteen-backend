using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.Mappers.Identity;
using BLL.Base.Services;
using Contracts.BLL.App.Mappers.Identity;
using Contracts.BLL.App.Services.Identity;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories.Identity;
using AppUserRole = BLL.App.DTO.Identity.AppUserRole;

namespace BLL.App.Services.Identity
{
    public class AppUserRoleService :
        BaseEntityService<IAppUnitOfWork, IAppUserRoleRepository, IAppUserRoleServiceMapper, DAL.App.DTO.Identity.AppUserRole,
            BLL.App.DTO.Identity.AppUserRole>, IAppUserRoleService
    {
        public AppUserRoleService(IAppUnitOfWork uow) : base(uow, uow.AppUserRoles, new AppUserRoleServiceMapper())
        {
        }

        public virtual async Task<IEnumerable<BLL.App.DTO.Identity.AppUserRole>> GetAllAsync(Guid? userId = null,
            bool noTracking = true)
        {
            return (await Repository.GetAllAsync(userId, noTracking)).Select(e => Mapper.Map<DAL.App.DTO.Identity.AppUserRole,
                BLL.App.DTO.Identity.AppUserRole>(e));
        }

        
        public virtual async Task<IEnumerable<BLL.App.DTO.Identity.AppUserRole>> GetAllWithRoleGroupedByUserIdAsync(Guid? roleId,
            bool noTracking = true)
        {
            return (await Repository.GetAllWithRoleGroupedByUserIdAsync(roleId, noTracking)).Select(e => 
                Mapper.Map<DAL.App.DTO.Identity.AppUserRole, BLL.App.DTO.Identity.AppUserRole>(e));
        }
        
        public virtual async Task<IEnumerable<BLL.App.DTO.Identity.AppUserRole>> GetAllForRoleAsync(Guid roleId,
            bool noTracking = true)
        {
            return (await Repository.GetAllAsync(roleId, noTracking)).Select(e => 
                Mapper.Map<DAL.App.DTO.Identity.AppUserRole, BLL.App.DTO.Identity.AppUserRole>(e));
        }

        public virtual async Task<IEnumerable<AppUserRole>> GetAllWithRolesAsync(List<Guid> roleIds, bool noTracking = true)
        {
            return (await Repository.GetAllWithRolesAsync(roleIds, noTracking)).Select(e =>
                Mapper.Map<DAL.App.DTO.Identity.AppUserRole, BLL.App.DTO.Identity.AppUserRole>(e));
        }


        public virtual async Task<AppUserRole> FindByUserAndRoleAsync(Guid userId, Guid roleId)
        {
            var appUserRole = await Repository.FindByUserAndRoleAsync(userId, roleId);
            return Mapper.Map<DAL.App.DTO.Identity.AppUserRole,
                BLL.App.DTO.Identity.AppUserRole>(appUserRole);
        }

        public virtual async Task<bool> CheckIfAppUserRoleExists(Guid userId, Guid roleId)
        {
            if (userId == null || roleId == null)
            {
                return false;
            }

            var appUserRole = (await Repository.FindByUserAndRoleAsync(userId, roleId));

            return appUserRole != null;
        }

        public virtual async Task<BLL.App.DTO.Identity.AppUserRole> FirstOrDefaultAsync(Guid id, Guid? userId = null, bool noTracking = true)
        {
            var appUserRole = (await Repository.FirstOrDefaultAsync(id, userId, noTracking));
            return Mapper.Map<DAL.App.DTO.Identity.AppUserRole,
                BLL.App.DTO.Identity.AppUserRole>(appUserRole);
        }

        public bool HasSimilarRole(string targetRole, List<string> allRoles)
        {
            return Repository.HasSimilarRole(targetRole, allRoles);
        }
    }
}