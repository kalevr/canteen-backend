using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.Mappers.Identity;
using BLL.Base.Services;
using Contracts.BLL.App.Mappers.Identity;
using Contracts.BLL.App.Services.Identity;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories.Identity;

namespace BLL.App.Services.Identity
{
    public class AppUserService :
        BaseEntityService<IAppUnitOfWork, IAppUserRepository, IAppUserServiceMapper, DAL.App.DTO.Identity.AppUser,
            BLL.App.DTO.Identity.AppUser>, IAppUserService
    {
        public AppUserService(IAppUnitOfWork uow) : base(uow, uow.AppUsers, new AppUserServiceMapper())
        {
        }

        public virtual async Task<IEnumerable<BLL.App.DTO.Identity.AppUser>> GetAllAsync(Guid? userId = null,
            bool noTracking = true)
        {
            return (await Repository.GetAllAsync(userId, noTracking))
                .Select(e => Mapper.Map<DAL.App.DTO.Identity.AppUser,
                BLL.App.DTO.Identity.AppUser>(e));
        }
    }
}