using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.Mappers.Identity;
using BLL.Base.Services;
using Contracts.BLL.App.Mappers.Identity;
using Contracts.BLL.App.Services.Identity;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories.Identity;

namespace BLL.App.Services.Identity
{
    public class AppRoleService :
        BaseEntityService<IAppUnitOfWork, IAppRoleRepository, IAppRoleServiceMapper, DAL.App.DTO.Identity.AppRole,
            BLL.App.DTO.Identity.AppRole>, IAppRoleService
    {
        public AppRoleService(IAppUnitOfWork uow) : base(uow, uow.AppRoles, new AppRoleServiceMapper())
        {
        }

        public virtual async Task<IEnumerable<BLL.App.DTO.Identity.AppRole>> GetAllAsync(Guid? userId = null,
            bool noTracking = true)
        {
            return (await Repository.GetAllAsync(userId, noTracking)).Select(e =>
                Mapper.Map<DAL.App.DTO.Identity.AppRole, BLL.App.DTO.Identity.AppRole>(e));
        }

        public virtual async Task<IEnumerable<BLL.App.DTO.Identity.AppRole>> GetMultipleByIdsAsync(List<Guid> roleIds, bool noTracking = true)
        {
            return (await Repository.GetMultipleByIdsAsync(roleIds, noTracking)).Select(e =>
                Mapper.Map<DAL.App.DTO.Identity.AppRole, BLL.App.DTO.Identity.AppRole>(e));
        }

        public virtual async Task<BLL.App.DTO.Identity.AppRole> FirstOrDefaultAsync(Guid id, Guid? userId = null,
            bool noTracking = true)
        {
            var appRole = (await Repository.FirstOrDefaultAsync(id, userId, noTracking));
            return Mapper.Map<DAL.App.DTO.Identity.AppRole, BLL.App.DTO.Identity.AppRole>(appRole);
        }
    }
}