﻿using System;

namespace WebApp.GraphQL.Kitchens
{
    public record AddKitchenInput(
        string KitchenName);
}