﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Contracts.BLL.App;
using GreenDonut;
using HotChocolate.DataLoader;
using PublicApi.DTO.v1;
using PublicApi.DTO.v1.Mappers;

namespace WebApp.GraphQL.Kitchens
{
    public class KitchenBatchDataLoader : BatchDataLoader<Guid, Kitchen>
    {
        private readonly KitchenMapper _mapper = new KitchenMapper();
        private readonly IAppBLL _bll;
        
        public KitchenBatchDataLoader(
            IAppBLL bll,
            IBatchScheduler batchScheduler, 
            DataLoaderOptions<Guid>? options = null) 
            : base(batchScheduler, options)
        {
            _bll = bll;
        }

        protected override async Task<IReadOnlyDictionary<Guid, Kitchen>> LoadBatchAsync(
            IReadOnlyList<Guid> keys, 
            CancellationToken cancellationToken)
        {
            var bllKitchens = await _bll.Kitchens.GetAllByIdsAsync(keys);
            var kitchens = bllKitchens.Select(e => _mapper.Map<BLL.App.DTO.Kitchen, Kitchen>(e));
            return kitchens.ToDictionary(x => x.Id);
        }
    }
}