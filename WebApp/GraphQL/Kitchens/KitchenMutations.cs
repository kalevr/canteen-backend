﻿using System;
using System.Threading.Tasks;
using Contracts.BLL.App;
using HotChocolate;
using HotChocolate.AspNetCore.Authorization;
using HotChocolate.Types;
using HotChocolate.Types.Relay;
using PublicApi.DTO.v1.Mappers;
using WebApp.Helpers;
using BLLDTO = BLL.App.DTO;
using V1DTO = PublicApi.DTO.v1;
// ReSharper disable ConditionIsAlwaysTrueOrFalse

namespace WebApp.GraphQL.Kitchens
{
    [ExtendObjectType(OperationTypeNames.Mutation)]
    [Authorize(Roles = new []{"Admin"})]
    public class KitchenMutations
    {
        private readonly KitchenMapper _mapper = new KitchenMapper();

        /// <summary>
        /// Adds a Kitchen asynchronously.
        /// </summary>
        /// <param name="bll">Is <see cref="IAppBLL"/> interface, that allows access to BLL layer</param>
        /// <param name="input">The <see cref="AddKitchenInput"/> instance that represents the Kitchen to add</param>
        /// <returns>A task that represents asynchronous operation. The task contains <see cref="KitchenPayload"/>s
        /// (that contains <see cref="V1DTO.Kitchen"/> object added to the database if successful and list of
        /// <see cref="UserError"/>s if not.</returns>
        public async Task<KitchenPayload> AddKitchenAsync([Service] IAppBLL bll, AddKitchenInput input)
        {
            if (string.IsNullOrEmpty(input.KitchenName))
            {
                return new KitchenPayload(
                    new[] {new UserError("KitchenName cannot be empty!", "NAME_EMPTY")});
            }

            var kitchen = new BLLDTO.Kitchen()
            {
                KitchenName = input.KitchenName
            };

            bll.Kitchens.Add(kitchen);
            await bll.SaveChangesAsync();

            var v1Kitchen = _mapper.Map<BLLDTO.Kitchen, V1DTO.Kitchen>(kitchen);
            return new KitchenPayload(v1Kitchen);
        }

        /// <summary>
        /// Updates a Kitchen asynchronously.
        /// </summary>
        /// <param name="bll">Is <see cref="IAppBLL"/> interface, that allows access to BLL layer</param>
        /// <param name="input">The <see cref="UpdateKitchenInput"/> instance that represents the Kitchen to update</param>
        /// <returns>A task that represents asynchronous operation. The task contains <see cref="KitchenPayload"/> (that
        /// contains <see cref="V1DTO.Kitchen"/> object updated in the database if successful and list of
        /// <see cref="UserError"/>s if not.</returns>
        public async Task<KitchenPayload> UpdateKitchenAsync([Service] IAppBLL bll, UpdateKitchenInput input)
        {
            if (string.IsNullOrEmpty(input.KitchenName))
            {
                return new KitchenPayload(
                    new[] {new UserError("KitchenName cannot be empty!", "NAME_EMPTY")});
            }
            
            BLLDTO.Kitchen kitchen = await bll.Kitchens.FirstOrDefaultAsync(input.Id);
            
            if (kitchen == null)
            {
                var payload = new KitchenPayload(
                    new[] {new UserError("Record with given id could not be found!", "INVALID_ID")});
                return payload;
            }
            
            kitchen.KitchenName = input.KitchenName;

            await bll.Kitchens.UpdateAsync(kitchen);
            await bll.SaveChangesAsync();

            var v1Kitchen = _mapper.Map<BLLDTO.Kitchen, V1DTO.Kitchen>(kitchen);
            return new KitchenPayload(v1Kitchen);
        }

        /// <summary>
        /// Deletes a Kitchen asynchronously.
        /// </summary>
        /// <param name="bll">Is <see cref="IAppBLL"/> interface, that allows access to BLL layer</param>
        /// <param name="id">Id of a Kitchen object (implements Node)</param>
        /// <returns>A task that represents asynchronous operation. The task contains <see cref="KitchenPayload"/>
        /// (that contains <see cref="V1DTO.Kitchen"/> object from the the database if successful and list of
        /// <see cref="UserError"/>s if not.</returns>
        public async Task<KitchenPayload> DeleteKitchenAsync([Service] IAppBLL bll, [ID(nameof(V1DTO.Kitchen))] Guid id)
        {
            if (id == null || id == Guid.Empty)
            {
                return new KitchenPayload(
                    new[] {new UserError("Invalid id!", "EMPTY_ID")});
            }

            BLLDTO.Kitchen kitchen = await bll.Kitchens.FirstOrDefaultAsync(id);
            if (kitchen == null)
            {
                var payload = new KitchenPayload(
                    new[] {new UserError("Record with given id could not be found!", "INVALID_ID")});
                return payload;
            }

            await bll.Kitchens.RemoveAsync(kitchen);
            await bll.SaveChangesAsync();
            var v1Kitchen = _mapper.Map<BLLDTO.Kitchen, V1DTO.Kitchen>(kitchen);
            return new KitchenPayload(v1Kitchen);
        }
    }
}