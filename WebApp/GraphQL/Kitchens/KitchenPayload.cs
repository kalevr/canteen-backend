﻿using System.Collections.Generic;
using PublicApi.DTO.v1;
using WebApp.Helpers;

namespace WebApp.GraphQL.Kitchens
{
    /// <summary>
    /// Payload that contains either <see cref="Kitchen"/> object or list of <see cref="UserError"/>s.
    /// </summary>
    public class KitchenPayload : Payload
    {
        public Kitchen? Kitchen { get; }
        public KitchenPayload(Kitchen kitchen)
        {
            Kitchen = kitchen;
        }

        public KitchenPayload(IReadOnlyList<UserError> errors) : base(errors)
        {
        }
    }
}