﻿using System;
using HotChocolate.Types.Relay;
using PublicApi.DTO.v1;

namespace WebApp.GraphQL.Kitchens
{
    public record UpdateKitchenInput(
        [ID(nameof(Kitchen))]
        Guid Id,
        string KitchenName);
}