﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Contracts.BLL.App;
using HotChocolate;
using HotChocolate.AspNetCore.Authorization;
using HotChocolate.Types;
using HotChocolate.Types.Relay;
using PublicApi.DTO.v1.Mappers;
using V1DTO=PublicApi.DTO.v1;


namespace WebApp.GraphQL.Kitchens
{
    [ExtendObjectType(OperationTypeNames.Query)]
    [Authorize(Roles = new []{"Admin"})]
    public class KitchenQueries
    {
        private readonly KitchenMapper _mapper = new KitchenMapper();
        
        /// <summary>
        /// Get all Kitchens asynchronously.
        /// </summary>
        /// <param name="bll">Is <see cref="IAppBLL"/> interface, that allows access to BLL layer</param>
        /// <returns>A task that represents asynchronous operation. The task contains <see cref="V1DTO.Kitchen"/> objects
        /// fetched from the database.</returns>
        public async Task<IEnumerable<V1DTO.Kitchen>> GetKitchensAsync([Service] IAppBLL bll)
        {
            var result = await bll.Kitchens.GetAllAsync(null);
            return result.Select(e => _mapper.Map<BLL.App.DTO.Kitchen, V1DTO.Kitchen>(e));
        }

        
        /// <summary>
        /// Get a Kitchen by Id asynchronously.
        /// </summary>
        /// <param name="id">Id (<see cref="Guid"/>) of a Kitchen object (implements Node)</param>
        /// <param name="dataLoader"><see cref="KitchenBatchDataLoader"/></param>
        /// <param name="ct"><see cref="CancellationToken"/></param>
        /// <returns>A task that represents asynchronous operation. The task contains <see cref="V1DTO.Kitchen"/> object
        /// fetched from the database.</returns>
        public async Task<V1DTO.Kitchen> GetKitchenByIdAsync([ID(nameof(V1DTO.Kitchen))] Guid id, 
            KitchenBatchDataLoader dataLoader, CancellationToken ct)
        {
            Console.WriteLine("running getKById query!");
            return await dataLoader.LoadAsync(id, ct);
        }

        /// <summary>
        /// Get multiple Kitchens by multiple Id's asynchronously.
        /// </summary>
        /// <param name="ids"> List of Id's (<see cref="Guid"/>) of a Kitchen objects (implements Node)</param>
        /// <param name="dataLoader"><see cref="KitchenBatchDataLoader"/></param>
        /// <param name="ct"><see cref="CancellationToken"/></param>
        /// <returns>A task that represents asynchronous operation. The task contains <see cref="V1DTO.Kitchen"/>
        /// objects fetched from the database.</returns>
        public async Task<IEnumerable<V1DTO.Kitchen>> GetKitchensByIdAsync(
            [ID(nameof(V1DTO.Kitchen))] Guid[] ids, KitchenBatchDataLoader dataLoader, CancellationToken ct)
        {
            return await dataLoader.LoadAsync(ids, ct);
        }
    }
}