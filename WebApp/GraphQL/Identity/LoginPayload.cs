﻿using System.Collections.Generic;
using PublicApi.DTO.v1;
using WebApp.Helpers;

namespace WebApp.GraphQL.Identity
{
    public class LoginPayload : Payload
    {
        public LoginToken? LoginToken{ get; }

        public LoginPayload(LoginToken loginToken)
        {
            LoginToken = loginToken;
        }

        public LoginPayload(IReadOnlyList<UserError> errors) : base(errors)
        {
        }
    }
}