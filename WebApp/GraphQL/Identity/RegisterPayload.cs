﻿using System.Collections.Generic;
using PublicApi.DTO.v1;
using WebApp.Helpers;

namespace WebApp.GraphQL.Identity
{
    public class RegisterPayload : Payload
    {
        public Register? Register { get; }

        public RegisterPayload(Register register)
        {
            Register = register;
        }
        
        public RegisterPayload(IReadOnlyList<UserError> errors) : base(errors)
        {
        }
    }
}