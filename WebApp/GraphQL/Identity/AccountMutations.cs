﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Extensions;
using HotChocolate.Types;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using PublicApi.DTO.v1;
using WebApp.Helpers;

namespace WebApp.GraphQL.Identity
{
    [ExtendObjectType("Mutation")]
    public class AccountMutations
    {
        private readonly IConfiguration _configuration;
        private readonly UserManager<Domain.App.Identity.AppUser> _userManager;
        private readonly ILogger<AccountMutations> _logger;
        private readonly SignInManager<Domain.App.Identity.AppUser> _signInManager;
        private readonly IAppBLL _bll;
        private readonly IHttpContextAccessor _accessor;


        public AccountMutations(IConfiguration configuration, UserManager<Domain.App.Identity.AppUser> userManager,
            ILogger<AccountMutations> logger, SignInManager<Domain.App.Identity.AppUser> signInManager, IAppBLL bll,
            IHttpContextAccessor accessor)
        {
            _configuration = configuration;
            _userManager = userManager;
            _logger = logger;
            _signInManager = signInManager;
            _bll = bll;
            _accessor = accessor;
        }


        /// <summary>
        /// Register a new user.
        /// </summary>
        /// <param name="input">Registration form contents</param>
        /// <returns>Ok or Problem</returns>
        public async Task<string> CreateAccountAsync(RegisterInput input, ClaimsPrincipal claimsPrincipal)
        {
            var errors = RegistrationInputValidation(input);
            if (!string.IsNullOrEmpty(errors))
            {
                return errors;
            }

            var user = new Domain.App.Identity.AppUser()
            {
                UserName = input.Email,
                Email = input.Email,
                FirstName = input.FirstName,
                LastName = input.LastName
            };

            var result = await _userManager.CreateAsync(user, input.Password);

            if (result.Succeeded)
            {
                return "OK";
            }

            var passValidator = new PasswordValidator<Domain.App.Identity.AppUser>();
            var passResult = await passValidator.ValidateAsync(_userManager, null!, input.Password);
            var messages = "";

            foreach (var error in passResult.Errors)
            {
                messages += error.Description + "\n";
            }

            foreach (var error in result.Errors)
            {
                messages += error.Description + "\n";
            }

            return messages;
        }


        public async Task<LoginPayload> LoginAsync(LoginInput input)
        {
            if (string.IsNullOrEmpty(input.Email) || string.IsNullOrEmpty(input.Password))
            {
                return new LoginPayload(
                    new[] {new UserError("Wrong username or password!", "INVALID_CREDENTIALS")});
            }

            var appUser = await _userManager.FindByEmailAsync(input.Email);
            var saveLoginHistory = new SaveLoginHistory(_bll);
            var loginHistory = new BLL.App.DTO.LoginHistory()
            {
                Id = Guid.NewGuid(),
                UserName = input.Email,
                LoginTime = DateTime.Now,
                Success = false,
                Ipv4 = _accessor.HttpContext!.Connection.RemoteIpAddress!.ToString(),
                UserAgent = _accessor.HttpContext.Request.Headers[HeaderNames.UserAgent]
            };

            if (appUser == null)
            {
                await saveLoginHistory.AddLoginHistory(loginHistory);
                _logger.LogInformation($"Web-Api login. User {input.Email} not found!");

                return new LoginPayload(
                    new[] {new UserError("Wrong username or password!", "INVALID_CREDENTIALS")});
            }

            loginHistory.AppUserId = appUser.Id;
            var result = await _signInManager.CheckPasswordSignInAsync(appUser, input.Password, false);

            if (result.Succeeded)
            {
                var claimsPrincipal = await _signInManager.CreateUserPrincipalAsync(appUser); //get the User analog
                var jwt = IdentityExtensions.GenerateJWT(claimsPrincipal.Claims,
                    _configuration["JWT:SigningKey"],
                    _configuration["JWT:Issuer"],
                    _configuration.GetValue<int>("JWT:ExpirationInDays")
                );
                _logger.LogInformation($"Token generated for user {input.Email}");
                loginHistory.Success = true;
                await saveLoginHistory.AddLoginHistory(loginHistory);

                return new LoginPayload(new LoginToken()
                {
                    Email = input.Email,
                    JwtToken = jwt
                });
            }

            await saveLoginHistory.AddLoginHistory(loginHistory);
            _logger.LogInformation($"Web-Api login. User {input.Email} attempted to log-in with bad password!");

            return new LoginPayload(
                new[] {new UserError("Wrong username or password!", "INVALID_CREDENTIALS")});
        }


        private string RegistrationInputValidation(RegisterInput input)
        {
            string message = "";

            if (string.IsNullOrEmpty(input.Email))
            {
                message += "Email can't be empty!\n";
            }

            if (string.IsNullOrEmpty(input.Password) || string.IsNullOrEmpty(input.ConfirmPassword))
            {
                message += "Password or ConfirmPassword can't be empty!\n";
            }

            if (input.Password != input.ConfirmPassword)
            {
                message += "Password and password confirmation does not match!";
            }

            return message;
        }
    }
}