﻿namespace WebApp.GraphQL.Identity
{
    public record RegisterInput(
        string FirstName,
        string LastName,
        string Email,
        string Password,
        string ConfirmPassword
        
        );
}