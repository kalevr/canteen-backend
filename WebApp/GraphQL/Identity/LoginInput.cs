﻿namespace WebApp.GraphQL.Identity
{
    public record LoginInput(
        string Email,
        string Password
    );
}