﻿using System.Collections.Generic;
using PublicApi.DTO.v1;
using WebApp.Helpers;

namespace WebApp.GraphQL.KitchenViews
{
    /// <summary>
    /// Payload that contains either <see cref="KitchenView"/> object or list of <see cref="UserError"/>s.
    /// </summary>
    public class KitchenViewPayload : Payload
    {
        public KitchenView? KitchenView { get; }
        public KitchenViewPayload(KitchenView kitchenView)
        {
            KitchenView = kitchenView;
        }

        public KitchenViewPayload(IReadOnlyList<UserError> errors) : base(errors)
        {
        }
    }
}