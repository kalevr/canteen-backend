﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Contracts.BLL.App;
using GreenDonut;
using HotChocolate.DataLoader;
using PublicApi.DTO.v1;
using PublicApi.DTO.v1.Mappers;

namespace WebApp.GraphQL.KitchenViews
{
    public class KitchenViewBatchDataLoader : BatchDataLoader<Guid, KitchenView>
    {
        private readonly KitchenViewMapper _mapper = new KitchenViewMapper();
        private readonly IAppBLL _bll;
        
        public KitchenViewBatchDataLoader(
            IAppBLL bll,
            IBatchScheduler batchScheduler, 
            DataLoaderOptions<Guid>? options = null) 
            : base(batchScheduler, options)
        {
            _bll = bll;
        }

        protected override async Task<IReadOnlyDictionary<Guid, KitchenView>> LoadBatchAsync(
            IReadOnlyList<Guid> keys, 
            CancellationToken cancellationToken)
        {
            var bllKitchenViews = await _bll.KitchenViews.GetAllByIdsAsync(keys);
            var kitchenViews = bllKitchenViews.Select(e => _mapper.BllToV1WithMealType(e));
            return kitchenViews.ToDictionary(x => x.Id);
        }
    }
}