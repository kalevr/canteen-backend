﻿using System;
using System.Threading.Tasks;
using Contracts.BLL.App;
using HotChocolate;
using HotChocolate.AspNetCore.Authorization;
using HotChocolate.Types;
using HotChocolate.Types.Relay;
using PublicApi.DTO.v1.Mappers;
using WebApp.GraphQL.Kitchens;
using WebApp.Helpers;
using BLLDTO = BLL.App.DTO;
using V1DTO = PublicApi.DTO.v1;
// ReSharper disable ConditionIsAlwaysTrueOrFalse

namespace WebApp.GraphQL.KitchenViews
{
    [ExtendObjectType(OperationTypeNames.Mutation)]
    [Authorize(Roles = new []{"Admin"})]
    public class KitchenViewMutations
    {
        private readonly KitchenViewMapper _mapper = new KitchenViewMapper();

        /// <summary>
        /// Adds a KitchenView asynchronously.
        /// </summary>
        /// <param name="bll">Is <see cref="IAppBLL"/> interface, that allows access to BLL layer</param>
        /// <param name="input">The <see cref="AddKitchenViewInput"/> instance that represents the KitchenView to add</param>
        /// <returns>A task that represents asynchronous operation. The task contains <see cref="KitchenViewPayload"/>s
        /// (that contains <see cref="V1DTO.KitchenView"/> object added to the database if successful and list of
        /// <see cref="UserError"/>s if not.</returns>
        public async Task<KitchenViewPayload> AddKitchenViewAsync([Service] IAppBLL bll, AddKitchenViewInput input)
        {
            if (string.IsNullOrEmpty(input.KitchenViewName))
            {
                return new KitchenViewPayload(
                    new[] {new UserError("KitchenViewName cannot be empty!", "NAME_EMPTY")});
            }

            var kitchenView = new BLLDTO.KitchenView()
            {
                KitchenViewName = input.KitchenViewName,
                KitchenViewDisplayName = input.KitchenViewDisplayName,
                StartingFrom = input.StartingFrom,
                MealTypeId = input.MealTypeId
            };

            if (input.ValidUntil.HasValue && input.ValidUntil != null)
            {
                kitchenView.ValidUntil = (DateTime)input.ValidUntil;
            }
            else
            {
                kitchenView.ValidUntil = DateTime.MaxValue;
            }

            bll.KitchenViews.Add(kitchenView);
            await bll.SaveChangesAsync();

            var v1Kitchen = _mapper.Map<BLLDTO.KitchenView, V1DTO.KitchenView>(kitchenView);
            return new KitchenViewPayload(v1Kitchen);
        }

        /// <summary>
        /// Updates a KitchenView asynchronously.
        /// </summary>
        /// <param name="bll">Is <see cref="IAppBLL"/> interface, that allows access to BLL layer</param>
        /// <param name="input">The <see cref="UpdateKitchenViewInput"/> instance that represents the KitchenView to
        /// update</param>
        /// <returns>A task that represents asynchronous operation. The task contains <see cref="KitchenViewPayload"/>
        /// (that contains <see cref="V1DTO.KitchenView"/> object updated in the database if successful and list of
        /// <see cref="UserError"/>s if not.</returns>
        public async Task<KitchenViewPayload> UpdateKitchenViewAsync([Service] IAppBLL bll, UpdateKitchenViewInput input)
        {
            Console.WriteLine("Running UpdateKitchenViewAsync");
            if (string.IsNullOrEmpty(input.KitchenViewName))
            {
                return new KitchenViewPayload(
                    new[] {new UserError("KitchenViewName cannot be empty!", "NAME_EMPTY")});
            }
            
            BLLDTO.KitchenView kitchenView = await bll.KitchenViews.FirstOrDefaultAsync(input.Id);
            
            if (kitchenView == null)
            {
                var payload = new KitchenViewPayload(
                    new[] {new UserError("Record with given id could not be found!", "INVALID_ID")});
                return payload;
            }

            Console.WriteLine("mealTypeId: " + input.MealTypeId);
            kitchenView.KitchenViewName = input.KitchenViewName;
            kitchenView.KitchenViewDisplayName = input.KitchenViewDisplayName;
            kitchenView.StartingFrom = input.StartingFrom;
            //kitchenView.ValidUntil = input.ValidUntil;
            kitchenView.MealTypeId = input.MealTypeId;
            
            if (input.ValidUntil != null)
            {
                Console.WriteLine("validUntil: not null - mealTypeId: ");
                kitchenView.ValidUntil = (DateTime)input.ValidUntil;
            }
            else
            {
                Console.WriteLine("validUntil: null - value: " + input.ValidUntil);
                kitchenView.ValidUntil = DateTime.MaxValue;
                Console.WriteLine("value after: " + kitchenView.ValidUntil);
            }

            await bll.KitchenViews.UpdateAsync(kitchenView);
            await bll.SaveChangesAsync();

            var v1KitchenView = _mapper.BllToV1WithMealType(kitchenView);
            return new KitchenViewPayload(v1KitchenView);
        }

        /// <summary>
        /// Deletes a KitchenView asynchronously.
        /// </summary>
        /// <param name="bll">Is <see cref="IAppBLL"/> interface, that allows access to BLL layer</param>
        /// <param name="id">Id of a KitchenView object (implements Node)</param>
        /// <returns>A task that represents asynchronous operation. The task contains <see cref="KitchenViewPayload"/>
        /// (that contains <see cref="V1DTO.KitchenView"/> object from the the database if successful and list of
        /// <see cref="UserError"/>s if not.</returns>
        public async Task<KitchenViewPayload> DeleteKitchenViewAsync([Service] IAppBLL bll, [ID(nameof(V1DTO.KitchenView))] Guid id)
        {
            if (id == null || id == Guid.Empty)
            {
                return new KitchenViewPayload(
                    new[] {new UserError("Invalid id!", "EMPTY_ID")});
            }

            BLLDTO.KitchenView kitchenView = await bll.KitchenViews.FirstOrDefaultAsync(id);
            if (kitchenView == null)
            {
                var payload = new KitchenViewPayload(
                    new[] {new UserError("Record with given id could not be found!", "INVALID_ID")});
                return payload;
            }

            await bll.KitchenViews.RemoveAsync(kitchenView);
            await bll.SaveChangesAsync();
            var v1Kitchen = _mapper.BllToV1WithMealType(kitchenView);
            return new KitchenViewPayload(v1Kitchen);
        }
    }
}