﻿using System;
using HotChocolate.Types.Relay;
using PublicApi.DTO.v1;

namespace WebApp.GraphQL.KitchenViews
{
    public record UpdateKitchenViewInput(
        [ID(nameof(KitchenView))]
        Guid Id,
        string KitchenViewName,
        string KitchenViewDisplayName,
        DateTime StartingFrom,
        DateTime? ValidUntil,
        [ID(nameof(MealType))] Guid MealTypeId
        );
}