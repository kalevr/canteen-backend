﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Contracts.BLL.App;
using HotChocolate;
using HotChocolate.AspNetCore.Authorization;
using HotChocolate.Types;
using HotChocolate.Types.Relay;
using PublicApi.DTO.v1.Mappers;
using V1DTO=PublicApi.DTO.v1;


namespace WebApp.GraphQL.KitchenViews
{
    [ExtendObjectType(OperationTypeNames.Query)]
    [Authorize(Roles = new []{"Admin"})]
    public class KitchenViewQueries
    {
        private readonly KitchenViewMapper _mapper = new KitchenViewMapper();
        
        /// <summary>
        /// Get all KitchenViews asynchronously.
        /// </summary>
        /// <param name="bll">Is <see cref="IAppBLL"/> interface, that allows access to BLL layer</param>
        /// <returns>A task that represents asynchronous operation. The task contains <see cref="V1DTO.KitchenView"/>
        /// objects fetched from the database.</returns>
        public async Task<IEnumerable<V1DTO.KitchenView>> GetKitchenViewsAsync([Service] IAppBLL bll)
        {
            var result = await bll.KitchenViews.GetAllAsync(null);
            return result.Select(e => _mapper.BllToV1WithMealType(e));
        }

        
        /// <summary>
        /// Get a KitchenView by Id asynchronously.
        /// </summary>
        /// <param name="id">Id (<see cref="Guid"/>) of a KitchenView object (implements Node)</param>
        /// <param name="dataLoader"><see cref="KitchenViewBatchDataLoader"/></param>
        /// <param name="ct"><see cref="CancellationToken"/></param>
        /// <returns>A task that represents asynchronous operation. The task contains <see cref="V1DTO.KitchenView"/>
        /// object fetched from the database.</returns>
        public async Task<V1DTO.KitchenView> GetKitchenViewByIdAsync([ID(nameof(V1DTO.KitchenView))] Guid id, 
            KitchenViewBatchDataLoader dataLoader, CancellationToken ct)
        {
            return await dataLoader.LoadAsync(id, ct);
        }

        /// <summary>
        /// Get multiple KitchenViews by multiple Id's asynchronously.
        /// </summary>
        /// <param name="ids"> List of Id's (<see cref="Guid"/>) of a KitchenView objects (implements Node)</param>
        /// <param name="dataLoader"><see cref="KitchenViewBatchDataLoader"/></param>
        /// <param name="ct"><see cref="CancellationToken"/></param>
        /// <returns>A task that represents asynchronous operation. The task contains <see cref="V1DTO.KitchenView"/>
        /// objects fetched from the database.</returns>
        public async Task<IEnumerable<V1DTO.KitchenView>> GetKitchenViewsByIdAsync(
            [ID(nameof(V1DTO.KitchenView))] Guid[] ids, KitchenViewBatchDataLoader dataLoader, CancellationToken ct)
        {
            return await dataLoader.LoadAsync(ids, ct);
        }
    }
}