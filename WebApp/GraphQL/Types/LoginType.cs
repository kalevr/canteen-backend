﻿using BLL.App.Helpers;
using HotChocolate.Types;
using PublicApi.DTO.v1;

namespace WebApp.GraphQL.Types
{
    public class LoginType : ObjectType<Login>
    {
        protected override void Configure(IObjectTypeDescriptor<Login> descriptor)
        {
            descriptor
                .Name(C.GRAPH_TYPE_LOGIN);

            descriptor
                .Field(x => x.Email)
                .Name(C.FIELD_EMAIL);

            /*
            descriptor
                .Field(x => x.Password)
                .Name(C.FIELD_PASSWORD);
                */
/*
            descriptor
                .Field(x => x.)
                .Name(C.FIELD_JWT_TOKEN);
                */
        }
    }
}