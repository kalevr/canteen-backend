﻿using BLL.App.Helpers;
using HotChocolate.Types;
using PublicApi.DTO.v1;

namespace WebApp.GraphQL.Types
{
    public class RegisterType : ObjectType<Register>
    {
        protected override void Configure(IObjectTypeDescriptor<Register> descriptor)
        {
            descriptor
                .Name(C.GRAPH_TYPE_REGISTER)
                .Authorize();

            descriptor
                .Field(x => x.FirstName)
                .Name(C.FIELD_FIRSTNAME);

            descriptor
                .Field(x => x.LastName)
                .Name(C.FIELD_LASTNAME);

            descriptor
                .Field(x => x.Email)
                .Name(C.FIELD_EMAIL);

            descriptor
                .Field(x => x.Password)
                .Name(C.FIELD_PASSWORD);

            descriptor
                .Field(x => x.ConfirmPassword)
                .Name(C.FIELD_CONFIRM_PASSWORD);
        }
    }
}