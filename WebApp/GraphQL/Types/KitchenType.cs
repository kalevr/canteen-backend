﻿using HotChocolate.Resolvers;
using HotChocolate.Types;
using PublicApi.DTO.v1;
using WebApp.GraphQL.Kitchens;

namespace WebApp.GraphQL.Types
{
    public class KitchenType : ObjectType<Kitchen>
    {
        protected override void Configure(IObjectTypeDescriptor<Kitchen> descriptor)
        {
            descriptor
                .ImplementsNode()
                .IdField(x => x.Id)
                .ResolveNode(async (ctx, id) =>
                    await ctx.DataLoader<KitchenBatchDataLoader>().LoadAsync(id, ctx.RequestAborted)
                );
        }
    }
}