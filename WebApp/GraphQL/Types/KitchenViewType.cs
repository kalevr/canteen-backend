﻿using HotChocolate.Resolvers;
using HotChocolate.Types;
using PublicApi.DTO.v1;
using WebApp.GraphQL.KitchenViews;

namespace WebApp.GraphQL.Types
{
    public class KitchenViewType : ObjectType<KitchenView>
    {
        protected override void Configure(IObjectTypeDescriptor<KitchenView> descriptor)
        {
            descriptor
                .ImplementsNode()
                .IdField(x => x.Id)
                .ResolveNode(async (ctx, id) =>
                    await ctx.DataLoader<KitchenViewBatchDataLoader>().LoadAsync(id, ctx.RequestAborted)
                );
        }
    }
}