﻿using BLL.App.Helpers;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using PublicApi.DTO.v1;
using WebApp.GraphQL.Kitchens;
using WebApp.GraphQL.MealTypes;

namespace WebApp.GraphQL.Types
{
    public class MealTypeType : ObjectType<MealType>
    {
        protected override void Configure(IObjectTypeDescriptor<MealType> descriptor)
        {
            descriptor
                .ImplementsNode()
                .IdField(x => x.Id)
                .ResolveNode(async (ctx, id) =>
                    await ctx.DataLoader<MealTypeBatchDataLoader>().LoadAsync(id, ctx.RequestAborted)
                );
        }
    }
}