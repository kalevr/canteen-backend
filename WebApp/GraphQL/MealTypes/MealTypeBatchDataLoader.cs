﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Contracts.BLL.App;
using GreenDonut;
using HotChocolate.DataLoader;
using PublicApi.DTO.v1;
using PublicApi.DTO.v1.Mappers;

namespace WebApp.GraphQL.MealTypes
{
    public class MealTypeBatchDataLoader : BatchDataLoader<Guid, MealType>
    {
        private readonly MealTypeMapper _mapper = new MealTypeMapper();
        private readonly IAppBLL _bll;
        
        public MealTypeBatchDataLoader(
            IAppBLL bll,
            IBatchScheduler batchScheduler, 
            DataLoaderOptions<Guid>? options = null) 
            : base(batchScheduler, options)
        {
            _bll = bll;
        }

        protected override async Task<IReadOnlyDictionary<Guid, MealType>> LoadBatchAsync(
            IReadOnlyList<Guid> keys, 
            CancellationToken cancellationToken)
        {
            var bllMealTypes = await _bll.MealTypes.GetAllByIdsAsync(keys);
            var kitchens = bllMealTypes.Select(e => _mapper.BllToV1WithKitchen(e));
            return kitchens.ToDictionary(x => x.Id);
        }
    }
}