﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Contracts.BLL.App;
using HotChocolate;
using HotChocolate.Types;
using HotChocolate.Types.Relay;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;


namespace WebApp.GraphQL.MealTypes
{
    [ExtendObjectType(OperationTypeNames.Query)]
    public class MealTypeQueries
    {
        private readonly MealTypeMapper _mapper = new MealTypeMapper();

        /// <summary>
        /// Get all MealTypes asynchronously.
        /// </summary>
        /// <param name="bll">Is <see cref="IAppBLL"/> interface, that allows access to BLL layer</param>
        /// <returns>A task that represents asynchronous operation. The task contains <see cref="V1DTO.MealType"/> objects
        /// fetched from the database.</returns>
        public async Task<IEnumerable<V1DTO.MealType>> GetMealTypesAsync([Service] IAppBLL bll)
        {
            var result = await bll.MealTypes.GetAllAsync(null);
            return result.Select(e => _mapper.BllToV1WithKitchen(e));
        }

        /// <summary>
        /// Get a Mealtype by Id asynchronously.
        /// </summary>
        /// <param name="id">Id (<see cref="Guid"/>) of a MealType object (implements Node)</param>
        /// <param name="dataLoader"><see cref="MealTypeBatchDataLoader"/></param>
        /// <param name="ct"><see cref="CancellationToken"/></param>
        /// <returns>A task that represents asynchronous operation. The task contains <see cref="V1DTO.MealType"/>
        /// object fetched from the database.</returns>
        public async Task<V1DTO.MealType> GetMealTypeByIdAsync([ID(nameof(V1DTO.MealType))] Guid id,
            MealTypeBatchDataLoader dataLoader, CancellationToken ct)
        {
            return await dataLoader.LoadAsync(id, ct);
        }

        /// <summary>
        /// Get multiple MealTypesT by multiple Id's asynchronously.
        /// </summary>
        /// <param name="ids"> List of Id's (<see cref="Guid"/>) of a MealType objects (implements Node)</param>
        /// <param name="dataLoader"><see cref="MealTypeBatchDataLoader"/></param>
        /// <param name="ct"><see cref="CancellationToken"/></param>
        /// <returns>A task that represents asynchronous operation. The task contains <see cref="V1DTO.MealType"/>
        /// objects fetched from the database.</returns>
        public async Task<IEnumerable<V1DTO.MealType>> GetMealTypesById([ID(nameof(V1DTO.MealType))] Guid[] ids,
            MealTypeBatchDataLoader dataLoader, CancellationToken ct)
        {
            return await dataLoader.LoadAsync(ids, ct);
        }
    }
}