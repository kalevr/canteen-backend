﻿using System;
using HotChocolate.Types.Relay;
using PublicApi.DTO.v1;

namespace WebApp.GraphQL.MealTypes
{
    public record UpdateMealTypeInput(
        [ID(nameof(MealType))]
        Guid Id,
        string MealTypeName,
        string MealTypeDisplayName,
        DateTime MealTime,
        Guid KitchenId,
        Kitchen? Kitchen,
        int RegistrationCutoff,
        string Comment);
}