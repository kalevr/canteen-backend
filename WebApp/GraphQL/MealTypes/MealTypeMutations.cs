﻿using System;
using System.Threading.Tasks;
using Contracts.BLL.App;
using HotChocolate;
using HotChocolate.Types;
using HotChocolate.Types.Relay;
using PublicApi.DTO.v1.Mappers;
using WebApp.Helpers;
using BLLDTO = BLL.App.DTO;
using V1DTO = PublicApi.DTO.v1;
// ReSharper disable ConditionIsAlwaysTrueOrFalse

namespace WebApp.GraphQL.MealTypes
{
    [ExtendObjectType(OperationTypeNames.Mutation)]
    public class MealTypeMutations
    {
        private readonly MealTypeMapper _mapper = new MealTypeMapper();

        /// <summary>
        /// Adds a MealType asynchronously.
        /// </summary>
        /// <param name="bll">Is <see cref="IAppBLL"/> interface, that allows access to BLL layer</param>
        /// <param name="input">The <see cref="AddMealTypeInput"/> instance that represents the MealType to add</param>
        /// <returns>A task that represents asynchronous operation. The task contains <see cref="MealTypePayload"/>s
        /// (that contains <see cref="V1DTO.MealType"/> object added to the database if successful and list of
        /// <see cref="UserError"/>s if not.</returns>
        public async Task<MealTypePayload> AddMealTypeAsync([Service] IAppBLL bll, AddMealTypeInput input)
        {
            if (string.IsNullOrEmpty(input.MealTypeName))
            {
                return new MealTypePayload(
                    new[] {new UserError("MealTypeName cannot be empty!", "NAME_EMPTY")});
            }

            var mealType = new BLLDTO.MealType
            {
                MealTypeName = input.MealTypeName,
                MealTime = input.MealTime,
                RegistrationCutoff = input.RegistrationCutoff,
                Comment = input.Comment,
                KitchenId = input.KitchenId,
                MealTypeDisplayName = input.MealTypeDisplayName
            };

            bll.MealTypes.Add(mealType);
            await bll.SaveChangesAsync();

            var v1MealType = _mapper.Map<BLLDTO.MealType, V1DTO.MealType>(mealType);
            return new MealTypePayload(v1MealType);
        }

        /// <summary>
        /// Updates a MealType asynchronously.
        /// </summary>
        /// <param name="bll">Is <see cref="IAppBLL"/> interface, that allows access to BLL layer</param>
        /// <param name="input">The <see cref="UpdateMealTypeInput"/> instance that represents the MealType to update</param>
        /// <returns>A task that represents asynchronous operation. The task contains <see cref="MealTypePayload"/> (that
        /// contains <see cref="V1DTO.MealType"/> object updated in the database if successful and list of
        /// <see cref="UserError"/>s if not.</returns>
        public async Task<MealTypePayload> UpdateMealTypeAsync([Service] IAppBLL bll, UpdateMealTypeInput input)
        {
            if (string.IsNullOrEmpty(input.MealTypeName))
            {
                return new MealTypePayload(
                    new[] {new UserError("MealTypeName cannot be empty!", "NAME_EMPTY")});
            }
            
            BLLDTO.MealType mealType = await bll.MealTypes.FirstOrDefaultAsync(input.Id);
            
            if (mealType == null)
            {
                var payload = new MealTypePayload(
                    new[] {new UserError("Record with given id could not be found!", "INVALID_ID")});
                return payload;
            }
            
            mealType.MealTypeName = input.MealTypeName;
            mealType.MealTypeDisplayName = input.MealTypeDisplayName;
            mealType.MealTime = input.MealTime;
            mealType.KitchenId = input.KitchenId;
            mealType.RegistrationCutoff = input.RegistrationCutoff;
            mealType.Comment = input.Comment;

            await bll.MealTypes.UpdateAsync(mealType);
            await bll.SaveChangesAsync();

            var v1MealType = _mapper.BllToV1WithKitchen(mealType);
            return new MealTypePayload(v1MealType);
        }

        /// <summary>
        /// Deletes a MealType asynchronously.
        /// </summary>
        /// <param name="bll">Is <see cref="IAppBLL"/> interface, that allows access to BLL layer</param>
        /// <param name="id">Id of a MealType object (implements Node)</param>
        /// <returns>A task that represents asynchronous operation. The task contains <see cref="MealTypePayload"/>
        /// (that contains <see cref="V1DTO.MealType"/> object from the the database if successful and list of
        /// <see cref="UserError"/>s if not.</returns>
        public async Task<MealTypePayload> DeleteMealTypeAsync([Service] IAppBLL bll, [ID(nameof(V1DTO.MealType))] Guid id)
        {
            if (id == null || id == Guid.Empty)
            {
                return new MealTypePayload(
                    new[] {new UserError("Invalid id!", "EMPTY_ID")});
            }

            BLLDTO.MealType mealType = await bll.MealTypes.FirstOrDefaultAsync(id);
            if (mealType == null)
            {
                var payload = new MealTypePayload(
                    new[] {new UserError("Record with given id could not be found!", "INVALID_ID")});
                return payload;
            }

            await bll.MealTypes.RemoveAsync(mealType);
            await bll.SaveChangesAsync();

            var v1MealType = _mapper.BllToV1WithKitchen(mealType);
            return new MealTypePayload(v1MealType);
        }
    }
}