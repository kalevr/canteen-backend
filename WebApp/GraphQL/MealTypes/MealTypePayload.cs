﻿using System.Collections.Generic;
using PublicApi.DTO.v1;
using WebApp.Helpers;

namespace WebApp.GraphQL.MealTypes
{
    /// <summary>
    /// Payload that contains either <see cref="MealType"/> object or list of <see cref="UserError"/>s.
    /// </summary>
    public class MealTypePayload : Payload
    {
        public MealType? MealType { get; }
        public MealTypePayload(MealType mealType)
        {
            MealType = mealType;
        }

        public MealTypePayload(IReadOnlyList<UserError> errors) : base(errors)
        {
        }
    }
}