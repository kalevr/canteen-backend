﻿using System;
using PublicApi.DTO.v1;

namespace WebApp.GraphQL.MealTypes
{
    public record AddMealTypeInput(
        string MealTypeName,
        string MealTypeDisplayName,
        DateTime MealTime,
        Guid KitchenId,
        Kitchen? Kitchen,
        int RegistrationCutoff,
        string Comment
        );
}