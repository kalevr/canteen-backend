﻿using System.Collections.Generic;

namespace WebApp.Helpers
{
    public class Payload
    {
        public IReadOnlyList<UserError>? Errors { get; }

        public Payload(IReadOnlyList<UserError>? errors = null)
        {
            Errors = errors;
        }
        
        
    }
}