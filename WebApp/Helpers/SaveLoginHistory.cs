using System.Threading.Tasks;
using Contracts.BLL.App;
using V1DTO=PublicApi.DTO.v1;

namespace WebApp.Helpers
{
    /// <summary>
    /// Save login history
    /// </summary>
    public class SaveLoginHistory
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="bll"></param>
        public SaveLoginHistory(IAppBLL bll)
        {
            _bll = bll;
        }
        
        /// <summary>
        /// Save login history
        /// </summary>
        /// <param name="loginHistory">LoginHistory object</param>
        /// <returns></returns>
        public async Task AddLoginHistory(BLL.App.DTO.LoginHistory loginHistory)
        {
            //loginHistory.AppUser = null;
            
            _bll.LoginHistories.Add(loginHistory);
            
            await _bll.SaveChangesAsync();
        }
    }
}