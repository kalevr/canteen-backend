#pragma warning disable 1591
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using BLL.App;
using BLL.App.Helpers;
using Contracts.BLL.App;
using Contracts.DAL.App;
using Contracts.DAL.Base;
using DAL.App.EF;
using Domain.App.Identity;
using HotChocolate.AspNetCore;
using HotChocolate.AspNetCore.Playground;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using WebApp.GraphQL.Identity;
using WebApp.GraphQL.Kitchens;
using WebApp.GraphQL.KitchenViews;
using WebApp.GraphQL.MealTypes;
using WebApp.GraphQL.Types;
using WebApp.Helpers;

namespace WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Environment = env;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; set; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("DefaultConnection");
            var serverVersion = new MySqlServerVersion(new Version(10, 6));

            services.AddDbContext<AppDbContext>(options =>
                options.UseMySql(connectionString, serverVersion)
            );
            
            // =============== JWT support ===============
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // => remove default claims
            services
                .AddAuthentication(cfg =>
                {
                    cfg.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    cfg.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false; // should be disabled only in DEV env (enable/delete it in prod)
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = Configuration[C.CONF_KEY_JWT_ISSUER],
                        ValidAudience = Configuration[C.CONF_KEY_JWT_ISSUER],
                        IssuerSigningKey =
                            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration[C.CONF_KEY_JWT_SIGN])),
                        ClockSkew = TimeSpan.Zero // remove delay of token when expire
                    };
                });

            // add as a scoped dependency
            services.AddScoped<IUserNameProvider, UserNameProvider>();
            services.AddScoped<IAppUnitOfWork, AppUnitOfWork>();
            services.AddScoped<IAppBLL, AppBLL>();
            
            
            services.AddIdentity<Domain.App.Identity.AppUser, Domain.App.Identity.AppRole>()
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 1;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
            });

            // makes httpcontext injectable - needed to resolve username in dal layer
            services.AddHttpContextAccessor();

            services.AddCors(options =>
            {
                options.AddPolicy("CorsAllowAll",
                    builder =>
                    {
                        builder.AllowAnyHeader();
                        builder.AllowAnyMethod();
                        builder.AllowAnyOrigin();
                    });
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy(C.POLICY_NAME_ADMIN, policy => { policy.RequireClaim(ClaimTypes.Role, "Admin"); });
                options.AddPolicy(C.POLICY_NAME_USER, policy => { policy.RequireClaim(ClaimTypes.Role, "User"); });
            });

            services.AddGraphQLServer()
                .AddAuthorization()
                //.AddHttpRequestInterceptor()
                .AddHttpRequestInterceptor((context, executor, builder, cancellationToken) =>
                {
                    // This is a dirty hack... remove it when HotChocolate gets their authentication fixed.
                    try
                    {
                        // Decode token
                        var authHeader = context.Request.Headers.Single(p => p.Key == "Authorization");
                        var token = (new JwtSecurityTokenHandler()).ReadJwtToken((authHeader.Value).ToString().Replace("Bearer ", string.Empty));
                        // Get username and roles
                        var username = token.Claims.Single(p => p.Type.Equals("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name")).Value;
                        var roles = token.Claims.Where(p => p.Type.Equals("http://schemas.microsoft.com/ws/2008/06/identity/claims/role")).Select(p => p.Value).ToArray();
                        // Set User identity
                        context.User = new GenericPrincipal(new GenericIdentity(username), roles);
                    }
                    catch
                    {
                        // ignored
                    }

                    return ValueTask.CompletedTask;
                })
                
                // Queries
                .AddQueryType()
                    .AddTypeExtension<KitchenQueries>()
                    .AddTypeExtension<KitchenViewQueries>()
                    .AddTypeExtension<MealTypeQueries>()
                
                // Mutations
                .AddMutationType()
                    .AddTypeExtension<KitchenMutations>()
                    .AddTypeExtension<KitchenViewMutations>()
                    .AddTypeExtension<MealTypeMutations>()
                
                .AddType<KitchenType>()
                .AddType<KitchenViewType>()
                .AddType<MealTypeType>()
                .AddType<LoginType>()
                .AddType<RegisterType>()
                .AddSorting()
                .AddFiltering()
                //.AddDataLoader<KitchenBatchDataLoader>()
                //.ModifyOptions(o => o.DefaultResolverStrategy = ExecutionStrategy.Serial) // usable in HotChocolate 12
                .ModifyRequestOptions(options => options.IncludeExceptionDetails = Environment.IsDevelopment());
            
            //services.AddScoped<KitchenQueries>();
            services.AddScoped<MealTypeQueries>();

            //services.AddScoped<KitchenMutations>();
            services.AddScoped<AccountMutations>();


            services.AddControllers();
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            UpdateDatabase(app, env, Configuration);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UsePlayground(new PlaygroundOptions
                {
                    QueryPath = "/graphql",
                    Path = "/playground"
                });
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            //app.UseStaticFiles();

            app.UseRouting();
            app.UseCors("CorsAllowAll");

            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints => { endpoints.MapGraphQL(); });
        }


        private static void UpdateDatabase(IApplicationBuilder app, IWebHostEnvironment env,
            IConfiguration configuration)
        {
            // give me the scoped services (everyhting created by it will be closed at the end of service scope life).
            using var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope();

            using var ctx = serviceScope.ServiceProvider.GetService<AppDbContext>();
            using var userManager = serviceScope.ServiceProvider.GetService<UserManager<AppUser>>();
            using var roleManager = serviceScope.ServiceProvider.GetService<RoleManager<AppRole>>();

            if (configuration["AppDataInitialization:DropDatabase"] == "True")
            {
                if (configuration["AppDataInitialization:MigrateDatabase"] == "False" &&
                    (configuration["AppDataInitialization:SeedIdentity"] == "True" ||
                     configuration.GetValue<bool>("AppDataInitialization:SeedData")))
                {
                    Console.WriteLine("Skiping DB drop, since MigrateDatabase = false and SeedIdentity or " +
                                      "SeedData = true. These settings are invalid (you can't drop db and " +
                                      "add stuff into it without doing the migration first!");
                }
                else
                {
                    Console.WriteLine("DropDatabase");
                    DAL.App.EF.Helpers.DataInitializers.DeleteDatabase(ctx!);
                }
            }

            if (configuration["AppDataInitialization:MigrateDatabase"] == "True")
            {
                Console.WriteLine("MigrateDatabase");
                DAL.App.EF.Helpers.DataInitializers.MigrateDatabase(ctx!);
            }

            if (configuration["AppDataInitialization:SeedIdentity"] == "True")
            {
                Console.WriteLine("SeedIdentity");
                DAL.App.EF.Helpers.DataInitializers.SeedIdentity(userManager!, roleManager!, ctx!);
            }

            if (configuration.GetValue<bool>("AppDataInitialization:SeedData"))
            {
                Console.WriteLine("SeedData");
                DAL.App.EF.Helpers.DataInitializers.SeedData(ctx!);
            }
        }
    }
}