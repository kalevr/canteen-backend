﻿using System;
using System.Threading.Tasks;

namespace Contracts.DAL.Base.Repositories
{
    public interface ISessionVariables
    {
        public Task SetUserSessionVariables(Guid userId);
    }
}