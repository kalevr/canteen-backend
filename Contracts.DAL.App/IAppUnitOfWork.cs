﻿using Contracts.DAL.App.Repositories;
using Contracts.DAL.App.Repositories.Identity;
using Contracts.DAL.Base;

namespace Contracts.DAL.App
{
    public interface IAppUnitOfWork : IBaseUnitOfWork, IBaseEntityTracker
    {
        IKitchenRepository Kitchens { get; }
        
        IKitchenViewRepository KitchenViews { get; }
        
        ILoginHistoryRepository LoginHistories { get; }
        
        IMealTypeRepository MealTypes { get; }
        
        IMyActionRepository MyActions { get; }
        
        IRegistrationRepository Registrations { get; }
        
        IParticipationRepository Participations { get; }
        
        IRoleActionRepository RoleActions { get; }
        
        IRoleInKitchenViewRepository RoleInKitchenViews { get; }
        
        IRoleMealTypeRepository RoleMealTypes { get; }
        
        
        IAppUserRepository AppUsers { get; }
        
        IAppRoleRepository AppRoles { get; }
        
        IAppUserRoleRepository AppUserRoles { get; }
        
    }
}