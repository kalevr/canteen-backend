using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IRoleActionRepositoryCustom: IRoleActionRepositoryCustom<RoleAction>
    {
    }

    public interface IRoleActionRepositoryCustom<TRoleAction>
    {
        Task<IEnumerable<TRoleAction>> GetAllAsync(Guid? userId = null, bool noTracking = true);
        Task<IEnumerable<TRoleAction>> GetAllForRolesAsync(List<Guid> roleIds, bool noTracking = true);
        Task<TRoleAction> FirstOrDefaultAsync(Guid id, Guid? userId = null, bool noTracking = true);
        Task<bool> UserHasAction(List<Guid> userRoleIds, Guid targetRoleId, string actionName, bool noTracking = true);

    }
    
}