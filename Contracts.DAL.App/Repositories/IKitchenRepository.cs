using Contracts.DAL.Base.Repositories;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IKitchenRepository: IBaseRepository<Kitchen>, IKitchenRepositoryCustom
    {
        // your stuff goes here
    }
}