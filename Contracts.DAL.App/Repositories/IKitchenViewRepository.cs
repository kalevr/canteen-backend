using Contracts.DAL.Base.Repositories;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IKitchenViewRepository: IBaseRepository<KitchenView>, IKitchenViewRepositoryCustom
    {
        // your stuff goes here
        }
}