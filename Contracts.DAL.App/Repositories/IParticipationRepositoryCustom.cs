using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IParticipationRepositoryCustom: IParticipationRepositoryCustom<Participation>
    {
    }

    public interface IParticipationRepositoryCustom<TParticipation>
    {
        Task<IEnumerable<TParticipation>> GetAllAsync(Guid groupId, Guid? userId = null, bool noTracking = true);
        Task<IEnumerable<TParticipation>> GetForRoleMealTypeForMonthAsync(Guid roleMealTypeId, DateTime startDate, 
            DateTime endDate, bool noTracking = true);
        
        Task<Dictionary<string, int>> GetMealCountForGroupForMonthAsync(Guid mealTypeId, Guid roleId, DateTime startDate,
            DateTime endDate, bool noTracking = true);

        Task<Dictionary<DateTime, int>> GetMealCountsPerDayForMonthAsync(
            Guid mealTypeId, List<Guid> roleIds, DateTime startDate, DateTime endDate, bool noTracking = true);
    }
    
}