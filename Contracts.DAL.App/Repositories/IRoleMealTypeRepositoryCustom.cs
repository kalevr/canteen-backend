using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IRoleMealTypeRepositoryCustom: IRoleMealTypeRepositoryCustom<RoleMealType>
    {
    }

    public interface IRoleMealTypeRepositoryCustom<TRoleMealType>
    {
        Task<IEnumerable<TRoleMealType>> GetAllAsync(Guid? userId = null, bool noTracking = true);
        Task<IEnumerable<TRoleMealType>> GetAllForRoleAsync(Guid mealTypeId, List<Guid?>? roleIds = null, bool noTracking = true);
        Task<TRoleMealType> FirstOrDefaultAsync(Guid id, Guid? userId = null, bool noTracking = true);
    }
    
}