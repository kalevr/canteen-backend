using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IRegistrationRepositoryCustom: IRegistrationRepositoryCustom<Registration>
    {
    }

    public interface IRegistrationRepositoryCustom<TRegistration>
    {
        Task<IEnumerable<TRegistration>> GetAllAsync(Guid groupId, Guid? userId = null, bool noTracking = true);
        Task<IEnumerable<TRegistration>> GetForRoleMealTypeForMonthAsync(Guid roleMealTypeId, DateTime startDate, 
            DateTime endDate, bool noTracking = true);
        
        Task<Dictionary<string, int>> GetMealCountForGroupForMonthAsync(Guid mealTypeId, Guid roleId, DateTime startDate,
            DateTime endDate, bool noTracking = true);

        Task<Dictionary<DateTime, int>> GetMealCountsPerDayForMonthAsync(
            Guid mealTypeId, List<Guid> roleIds, DateTime startDate, DateTime endDate, bool noTracking = true);
    }
    
}