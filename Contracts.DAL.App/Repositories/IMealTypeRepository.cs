using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IMealTypeRepository: IBaseRepository<MealType>, IMealTypeRepositoryCustom
    {
        // your stuff goes here
        }
}