using Contracts.DAL.Base.Repositories;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface ILoginHistoryRepository : IBaseRepository<LoginHistory>, ILoginHistoryRepositoryCustom
    {
        // your stuff goes here
       }
}