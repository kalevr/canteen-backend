using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IRoleActionRepository: IBaseRepository<RoleAction>, IRoleActionRepositoryCustom
    {
        // your stuff goes here
        }
}