using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IKitchenRepositoryCustom: IKitchenRepositoryCustom<Kitchen>
    {
    }

    public interface IKitchenRepositoryCustom<TKitchen>
    {
        Task<IEnumerable<TKitchen>> GetAllAsync(Guid? userId = null, bool noTracking = true);
        Task<IEnumerable<TKitchen>> GetAllByIdsAsync(IReadOnlyList<Guid> kitchenIds, Guid? userId = null, bool noTracking = true);
    }
    
}