using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IKitchenViewRepositoryCustom: IKitchenViewRepositoryCustom<KitchenView>
    {
    }

    public interface IKitchenViewRepositoryCustom<TKitchenView>
    {
        Task<IEnumerable<TKitchenView>> GetAllAsync(Guid? userId = null, bool noTracking = true);
        Task<TKitchenView> FirstOrDefaultAsync(Guid id, Guid? userId = null, bool noTracking = true);
        Task<IEnumerable<TKitchenView>> GetAllByIdsAsync(IReadOnlyList<Guid> kitchenViewIds, Guid? userId = null, 
            bool noTracking = true);

    }
    
}