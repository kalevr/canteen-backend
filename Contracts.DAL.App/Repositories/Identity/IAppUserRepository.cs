using Contracts.DAL.Base.Repositories;
using DAL.App.DTO.Identity;

namespace Contracts.DAL.App.Repositories.Identity
{
    public interface IAppUserRepository: IBaseRepository<AppUser>, IAppUserRepositoryCustom
    {
        // your stuff here
        
    }
}