using Contracts.DAL.Base.Repositories;
using DAL.App.DTO.Identity;

namespace Contracts.DAL.App.Repositories.Identity
{
    public interface IAppRoleRepository: IBaseRepository<AppRole>, IAppRoleRepositoryCustom
    {
        // your stuff here
    }
}