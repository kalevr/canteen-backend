using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.App.DTO.Identity;

namespace Contracts.DAL.App.Repositories.Identity
{
    public interface IAppUserRepositoryCustom: IAppUserRepositoryCustom<AppUser>
    {
    }

    public interface IAppUserRepositoryCustom<TAppUser>
    {
        Task<IEnumerable<TAppUser>> GetAllAsync(Guid? userId = null, bool noTracking = true);
    }
    
}