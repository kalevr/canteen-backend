using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.App.DTO.Identity;

namespace Contracts.DAL.App.Repositories.Identity
{
    public interface IAppRoleRepositoryCustom: IAppRoleRepositoryCustom<AppRole>
    {
    }

    public interface IAppRoleRepositoryCustom<TAppRole>
    {
        Task<IEnumerable<TAppRole>> GetAllAsync(Guid? userId = null, bool noTracking = true);
        Task<IEnumerable<TAppRole>> GetMultipleByIdsAsync(List<Guid> roleIds, bool noTracking = true);
        Task<TAppRole> FirstOrDefaultAsync(Guid id, Guid? userId = null, bool noTracking = true);


    }
    
}