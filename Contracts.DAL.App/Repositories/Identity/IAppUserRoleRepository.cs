using Contracts.DAL.Base.Repositories;
using DAL.App.DTO.Identity;

namespace Contracts.DAL.App.Repositories.Identity
{
    public interface IAppUserRoleRepository: IBaseRepository<AppUserRole>, IAppUserRoleRepositoryCustom
    {
        // your stuff here
        
    }
}