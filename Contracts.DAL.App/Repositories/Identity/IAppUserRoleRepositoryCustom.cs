using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.App.DTO.Identity;

namespace Contracts.DAL.App.Repositories.Identity
{
    public interface IAppUserRoleRepositoryCustom: IAppUserRoleRepositoryCustom<AppUserRole>
    {
    }

    public interface IAppUserRoleRepositoryCustom<TAppUserRole>
    {
        Task<IEnumerable<TAppUserRole>> GetAllAsync(Guid? userId = null, bool noTracking = true);
        
        Task<IEnumerable<TAppUserRole>> GetAllWithRoleGroupedByUserIdAsync(Guid? roleId = null, bool noTracking = true);
        
        Task<IEnumerable<TAppUserRole>> GetAllForRoleAsync(Guid roleId, bool noTracking = true);
        Task<IEnumerable<TAppUserRole>> GetAllWithRolesAsync(List<Guid> roleIds, bool noTracking = true);

        
        Task<TAppUserRole> FindByUserAndRoleAsync(Guid userId, Guid roleId);

        
        bool HasSimilarRole(string targetRole, List<string> allRoles);
    }
    
}