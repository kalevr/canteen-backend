using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IMealTypeRepositoryCustom: IMealTypeRepositoryCustom<MealType>
    {
    }

    public interface IMealTypeRepositoryCustom<TMealType>
    {
        Task<IEnumerable<TMealType>> GetAllAsync(Guid? userId = null, bool noTracking = true);
        Task<TMealType> FirstOrDefaultAsync(Guid id, Guid? userId = null, bool noTracking = true);
        Task<IEnumerable<TMealType>> GetAllForKitchenAsync(List<Guid> mealTypeIds, bool noTracking = true);
        Task<IEnumerable<TMealType>> GetAllByIdsAsync(IReadOnlyList<Guid> mealTypeIds, Guid? userId = null, bool noTracking = true);



    }
    
}