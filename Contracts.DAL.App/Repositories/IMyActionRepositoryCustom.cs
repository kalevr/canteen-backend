using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IMyActionRepositoryCustom: IMyActionRepositoryCustom<MyAction>
    {
    }

    public interface IMyActionRepositoryCustom<TMyAction>
    {
        Task<IEnumerable<TMyAction>> GetAllAsync(Guid? userId = null, bool noTracking = true);
    }
    
}