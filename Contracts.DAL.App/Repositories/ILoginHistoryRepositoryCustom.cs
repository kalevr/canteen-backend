using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface ILoginHistoryRepositoryCustom: ILoginHistoryRepositoryCustom<LoginHistory>
    {
    }

    public interface ILoginHistoryRepositoryCustom<TLoginHistory>
    {
        Task<IEnumerable<TLoginHistory>> GetAllAsync(Guid? userId = null, bool noTracking = true);
        
        Task<TLoginHistory> FirstOrDefaultAsync(Guid id, Guid? userId = null, bool noTracking = true);

    }
    
}