using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IRoleInKitchenViewRepositoryCustom: IRoleInKitchenViewRepositoryCustom<RoleInKitchenView>
    {
    }

    public interface IRoleInKitchenViewRepositoryCustom<TRoleInKitchenView>
    {
        Task<IEnumerable<TRoleInKitchenView>> GetAllAsync(Guid? userId = null, bool noTracking = true);
        Task<TRoleInKitchenView> FirstOrDefaultAsync(Guid id, Guid? userId = null, bool noTracking = true);
        Task<IEnumerable<TRoleInKitchenView>> GetAllForKitchenViewAsync(Guid kitchenViewId, bool noTracking = true);

    }
    
}