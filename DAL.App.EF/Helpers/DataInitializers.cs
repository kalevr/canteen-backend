using System;
using System.Collections.Generic;
using System.Linq;
using Domain.App;
using Domain.App.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Helpers
{
    public class DataInitializers
    {
        public static void MigrateDatabase(AppDbContext context)
        {
            context.Database.Migrate();
        }

        public static bool DeleteDatabase(AppDbContext context)
        {
            return context.Database.EnsureDeleted();
        }

        public static void SeedIdentity(UserManager<AppUser> userManager, RoleManager<AppRole> roleManager, AppDbContext context)
        {
            var roleNames = new[] {"Admin", "Observer, Cook, Homeroom-teacher, Eater"};
            // Admin, observer, cook, homeroom-teacher, eater
            foreach (var roleName in roleNames)
            {
                var role = roleManager.FindByNameAsync(roleName).Result;
                if (role == null)
                {
                    role = new AppRole();
                    role.Name = roleName;
                    role.Comment = "Automatically generated";
                    role.DisplayName = roleName;
                    var result = roleManager.CreateAsync(role).Result;
                    if (!result.Succeeded)
                    {
                        throw new ApplicationException("Role creation failed!");
                    }
                }
            }


            var userOne = new Dictionary<string, string>()
            {
                {"userName", "admin@localhost"},
                {"passWord", "Maasikas123!"},
                {"firstName", "Admin"},
                {"lastName", "Kasutaja"},
                {"myRole", "Admin"},
                { "comment", "This is admin user"}
            };

            var userTwo = new Dictionary<string, string>()
            {
                {"userName", "user@localhost"},
                {"passWord", "Maasikas123!"},
                {"firstName", "Tava"},
                {"lastName", "Kasutaja"},
                {"myRole", "Eater"},
                { "comment", "This is normal user"}
            };

            var users = new[] {userOne, userTwo};

            foreach (var userData in users)
            {
                var user = userManager.FindByNameAsync(userData["userName"]).Result;
                if (user == null)
                {
                    user = new AppUser();
                    user.Email = userData["userName"];
                    user.UserName = userData["userName"];
                    user.FirstName = userData["firstName"];
                    user.LastName = userData["lastName"];
                    user.Comment = userData["comment"];
                    user.CreatedBy = "SYSTEM";

                    var result = userManager.CreateAsync(user, userData["passWord"]).Result;
                    if (!result.Succeeded)
                    {
                        throw new ApplicationException("User creation failed!");
                    }

                    var role = roleManager.FindByNameAsync(userData["myRole"]).Result;
                    if (role != null)
                    {
                        var myRole = new AppUserRole()
                        {
                            UserId = user.Id,
                            RoleId = role.Id,
                            StartingFrom = DateTime.Now,
                            ValidUntil = DateTime.Parse("9999-12-31"),
                            CreatedBy = "SYSTEM"
                        };

                        context.Add(myRole);
                        context.SaveChanges();
                    }
                    
                }
            }
        }

        public static void SeedData(AppDbContext context)
        {
            var endOfDays = "9999-12-31";
            
            var kitchens = new[] {"Hell's Kitchen", "Bloody Kitchen"};

            foreach (var kitchenName in kitchens)
            {
                var kitchen = context.Kitchens.FirstOrDefault(k => k.KitchenName == kitchenName);

                if (kitchen == null)
                {
                    kitchen = new Kitchen()
                    {
                        KitchenName = kitchenName, 
                        CreatedBy = "SYSTEM"
                    };

                    context.Kitchens.Add(kitchen);

                    SaveMyStuff(context, "kitchen");
                }
            }


            var myActions = new[] {"Read", "Write"};

            foreach (var myActionName in myActions)
            {
                var myAction = context.MyActions.FirstOrDefault(a => a.MyActionName == myActionName);

                if (myAction == null)
                {
                    myAction = new MyAction()
                    {
                        MyActionName = myActionName,
                        MyActionDisplayName = myActionName,
                        CreatedBy = "SYSTEM"
                    };

                    context.MyActions.Add(myAction);

                    SaveMyStuff(context, "myAction");
                }
            }
            
            
            var mealTypeOne = new Dictionary<string, string>()
            {
                {"mealTypeName", "Hommikusöök"},
                {"mealTime", "07:45"},
                {"registrationCutoff", "24"},
                { "comment", "NomNomNom" },
                { "kitchenName", "Hell's Kitchen"},
                { "createdBy", "SYSTEM" }
            };
            var mealTypeTwo = new Dictionary<string, string>()
            {
                {"mealTypeName", "Lõuna"},
                {"mealTime", "12:25"},
                {"registrationCutoff", "24"},
                { "comment", "Lõunapaus" },
                { "kitchenName", "Hell's Kitchen"},
                { "createdBy", "SYSTEM" }
            };

            var mealTypes = new[] {mealTypeOne, mealTypeTwo};

            foreach (var mealTypeData in mealTypes)
            {
                var mealType = context.MealTypes
                    .FirstOrDefault(m => m.MealTypeName == mealTypeData["mealTypeName"]);
                var kitchen = context.Kitchens.FirstOrDefault(k => k.KitchenName == mealTypeData["kitchenName"]);

                if (mealType == null && kitchen != null)
                {
                    mealType = new MealType()
                    {
                        MealTypeName = mealTypeData["mealTypeName"],
                        MealTypeDisplayName = mealTypeData["mealTypeName"],
                        MealTime = DateTime.Parse(mealTypeData["mealTime"]),
                        RegistrationCutoff = Int32.Parse(mealTypeData["registrationCutoff"]),
                        Comment = mealTypeData["comment"],
                        CreatedBy = mealTypeData["createdBy"],
                        KitchenId = kitchen.Id
                    };

                    context.MealTypes.Add(mealType);

                    SaveMyStuff(context, "mealType");
                }
            }


            var loginHistory = context.LoginHistories
                .FirstOrDefault(l => l.UserName == "wannabe.hacker@localhost");
            if (loginHistory == null)
            {
                loginHistory = new LoginHistory()
                {
                    UserName = "wannabe.hacker@localhost",
                    Ipv4 = "127.0.0.1",
                    LoginTime = DateTime.Now
                };
                context.LoginHistories.Add(loginHistory);

                SaveMyStuff(context, "loginHistory");
            }

            
            var kitchenViewOne = new Dictionary<string, string>()
            {
                {"kvName", "MI Hommik"},
                {"kvMealType", "Hommikusöök"},
                {"createdBy", "SYSTEM"}
            };
            
            var kitchenViewTwo = new Dictionary<string, string>()
            {
                {"kvName", "MI Lõuna"},
                {"kvMealType", "Lõuna"},
                {"createdBy", "SYSTEM"}
            };

            var kitchenViews = new[] {kitchenViewOne, kitchenViewTwo};

            foreach (var kvData in kitchenViews)
            {
                var mealType = context.MealTypes.FirstOrDefault(mt => mt.MealTypeName == kvData["kvMealType"]);

                if (mealType != null)
                {
                    var kitchenView = context.KitchenViews.FirstOrDefault(kv => kv.KitchenViewName == kvData["kvName"]);

                    if (kitchenView == null)
                    {
                        kitchenView = new KitchenView()
                        {
                            KitchenViewName = kvData["kvName"],
                            KitchenViewDisplayName = kvData["kvName"],
                            StartingFrom = DateTime.Now,
                            ValidUntil = DateTime.Parse(endOfDays),
                            MealTypeId = mealType.Id,
                            CreatedBy = kvData["createdBy"]
                        };

                        context.KitchenViews.Add(kitchenView);

                        SaveMyStuff(context, "kitchenView");
                    }
                }
            }
            
            
            // Registration will not be seeded
            
            
            var roleMealTypeOne = new Dictionary<string, string>()
            {
                {"mealType", "Hommikusöök"},
                {"role", "User"},
                {"createdBy", "SYSTEM"}
            };
            
            var roleMealTypeTwo = new Dictionary<string, string>()
            {
                {"mealType", "Lõuna"},
                {"role", "User"},
                {"createdBy", "SYSTEM"}
            };

            var roleMealTypes = new[] {roleMealTypeOne, roleMealTypeTwo};

            foreach (var rmtData in roleMealTypes)
            {
                var mealType = context.MealTypes.FirstOrDefault(mt => mt.MealTypeName == rmtData["mealType"]);
                var role = context.AppRoles.FirstOrDefault(r => r.Name == rmtData["role"]);

                if (mealType != null && role != null)
                {
                    var roleMealType = context.RoleMealTypes.FirstOrDefault(rmt =>
                        rmt.MealTypeId == mealType.Id && rmt.AppRoleId == role.Id);

                    if (roleMealType == null)
                    {
                        roleMealType = new RoleMealType()
                        {
                            StartingFrom = DateTime.Now,
                            ValidUntil = DateTime.Parse(endOfDays),
                            MealTypeId = mealType.Id,
                            AppRoleId = role.Id,
                            CreatedBy = rmtData["createdBy"]
                        };

                        context.RoleMealTypes.Add(roleMealType);

                        SaveMyStuff(context, "roleMealType");
                    }
                }
            }
        }

        
        
        
        private static void SaveMyStuff(AppDbContext context, string tableName)
        {
            try
            {
                context.SaveChanges();
            }
            catch (ApplicationException)
            {
                Console.WriteLine("Failed to save " + tableName);
                throw;
            }
        }
    }
}