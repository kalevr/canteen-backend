using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class RoleActionRepository : 
        EFBaseRepository<AppDbContext,Domain.App.Identity.AppUser, Domain.App.RoleAction, DAL.App.DTO.RoleAction>, IRoleActionRepository
    {
        public RoleActionRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALAppMapper<Domain.App.RoleAction, DTO.RoleAction>())
        {
        }

        

        public virtual async Task<IEnumerable<DTO.RoleAction>> GetAllAsync(Guid? userId = null, bool noTracking = true)
        {
            var query = PrepareQuery(userId, noTracking);
            var domainEntities = await query
                .Include(a => a.Action)
                .Include(r => r.AppRole)
                .Include(tr => tr.TargetAppRole)
                .ToListAsync();
            var result = domainEntities.Select(e => Mapper.Map<Domain.App.RoleAction, DTO.RoleAction>(e));
            return result;
        }
        
        public virtual async Task<IEnumerable<DTO.RoleAction>> GetAllForRolesAsync(List<Guid> roleIds, bool noTracking = true)
        {
            var query = PrepareQuery(noTracking);
            var domainEntities = await query
                .Include(tr => tr.TargetAppRole)
                .Include(r => r.Action)
                .Where(r => roleIds.Contains(r.AppRoleId))
                .ToListAsync();
            var result = domainEntities.Select(e => Mapper.Map<Domain.App.RoleAction, DTO.RoleAction>(e));
            return result;
        }
        
        public virtual async Task<DTO.RoleAction> FirstOrDefaultAsync(Guid id, Guid? userId = null,
            bool noTracking = true)
        {
            var query = PrepareQuery(null, noTracking)
                .Where(ck => ck.Id == id)
                .Include(a => a.Action)
                .Include(r => r.AppRole)
                .Include(tr => tr.TargetAppRole)
                .AsQueryable();
            var domainEntity = await query.FirstOrDefaultAsync();
            var mapThatShit = Mapper.Map<Domain.App.RoleAction, DTO.RoleAction>(domainEntity);
            return mapThatShit;
        }

        public virtual async Task<bool> UserHasAction(
            List<Guid> userRoleIds, Guid targetRoleId, string actionName, bool noTracking = true)
        {
            var query = PrepareQuery(null, noTracking)
                .Where(ua =>
                    userRoleIds.Contains(ua.AppRoleId) && ua.TargetAppRoleId == targetRoleId &&
                    ua.Action!.MyActionName == actionName)
                .AsQueryable();
            var domainEntity = await query.FirstOrDefaultAsync();
            return domainEntity != null;
        }
    }
}