using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class RoleMealTypeRepository : 
        EFBaseRepository<AppDbContext,Domain.App.Identity.AppUser, Domain.App.RoleMealType, DAL.App.DTO.RoleMealType>, IRoleMealTypeRepository
    {
        public RoleMealTypeRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALAppMapper<Domain.App.RoleMealType, DTO.RoleMealType>())
        {
        }

        

        public virtual async Task<IEnumerable<DTO.RoleMealType>> GetAllAsync(Guid? userId = null, bool noTracking = true)
        {
            var query = PrepareQuery(userId, noTracking);
            var domainEntities = await query
                .Include(mt => mt.MealType)
                .Include(r => r.AppRole)
                .ToListAsync();
            var result = domainEntities.Select(e => Mapper.Map<Domain.App.RoleMealType, DTO.RoleMealType>(e));
            return result;
        }
        
        public virtual async Task<IEnumerable<DTO.RoleMealType>> GetAllForRoleAsync(Guid mealTypeId, List<Guid?>? appRoles = null, bool noTracking = true)
        {
            var query = PrepareQuery(null, noTracking);
            
            if (appRoles != null)
            {
                query = query.Where(r => r.MealTypeId == mealTypeId && appRoles.Contains(r.AppRoleId));
            }
            else
            {
                query = query.Where(r => r.MealTypeId == mealTypeId);
            }
            
            var domainEntities = await query
                .Include(mt => mt.MealType)
                .Include(r => r.AppRole)
                .OrderBy(e => e.AppRole!.DisplayName)
                .ToListAsync();
            var result = domainEntities.Select(e => Mapper.Map<Domain.App.RoleMealType, DTO.RoleMealType>(e));
            return result;
        }

        public virtual async Task<DTO.RoleMealType> FirstOrDefaultAsync(Guid id, Guid? userId = null,
            bool noTracking = true)
        {
            var query = PrepareQuery(null, noTracking)
                .Where(ck => ck.Id == id)
                .Include(mt => mt.MealType)
                .Include(r => r.AppRole)
                .AsQueryable();
            var domainEntity = await query.FirstOrDefaultAsync();
            var mapThatShit = Mapper.Map<Domain.App.RoleMealType, DTO.RoleMealType>(domainEntity);
            return mapThatShit;
        }
    }
}