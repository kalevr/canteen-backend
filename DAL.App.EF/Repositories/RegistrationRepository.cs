using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;
using Registration = BLL.App.DTO.Registration;

namespace DAL.App.EF.Repositories
{
    public class RegistrationRepository :
        EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.Registration, DAL.App.DTO.Registration>,
        IRegistrationRepository
    {
        public RegistrationRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALAppMapper<Domain.App.Registration, DTO.Registration>())
        {
        }


        public virtual async Task<IEnumerable<DTO.Registration>> GetAllAsync(Guid roleMealTypeId, Guid? userId = null,
            bool noTracking = true)
        {
            var query = PrepareQuery(userId, noTracking);
            var domainEntities = await query
                .Include(mt => mt.RoleMealType)
                .ThenInclude(u => u!.MealType)
                .ToListAsync();
            var result =
                domainEntities.Select(e => Mapper.Map<Domain.App.Registration, DTO.Registration>(e));
            return result;
        }

        public virtual async Task<IEnumerable<DTO.Registration>> GetForRoleMealTypeForMonthAsync(Guid roleMealTypeId,
            DateTime startDate, DateTime endDate, bool noTracking = true)
        {
            var query = PrepareQuery(null, noTracking);
            var domainEntities = await query
                .Include(u => u.AppUser)
                .Include(r => r.RoleMealType!.AppRole)
                .Where(d => d.RoleMealTypeId == roleMealTypeId
                            && d.MealDate >= startDate
                            && d.MealDate <= endDate)
                .ToListAsync();

            var result = domainEntities.Select(
                e => Mapper.Map<Domain.App.Registration, DTO.Registration>(e));
            return result;
        }


        public virtual async Task<Dictionary<string, int>> GetMealCountForGroupForMonthAsync(Guid mealTypeId,
            Guid roleId, DateTime startDate, DateTime endDate, bool noTracking = true)
        {
            var stats = new Dictionary<string, int>();
            var query = PrepareQuery(null, noTracking);
            var domainEntities = await query
                .Include(u => u.AppUser)
                .Include(r => r.RoleMealType)
                .ThenInclude(r => r!.AppRole)
                .Where(d => d.RoleMealType!.MealTypeId == mealTypeId
                            && d.RoleMealType.AppRoleId == roleId
                            && d.MealDate >= startDate
                            && d.MealDate <= endDate)
                .OrderBy(n => n.AppUser!.LastName)
                .GroupBy(u => u.AppUser!.FirstName + " " + u.AppUser!.LastName)
                .Select(r =>
                    new
                    {
                        Name = r.Key,
                        Count = r.Count(),
                    }).ToListAsync();

            foreach (var entity in domainEntities)
            {
                stats.Add(entity.Name, entity.Count);
            }

            return stats;
        }

        public virtual async Task<Dictionary<DateTime, int>> GetMealCountsPerDayForMonthAsync(
            Guid mealTypeId, List<Guid> roleIds, DateTime startDate, DateTime endDate, bool noTracking = true)
        {
            var ret = new Dictionary<DateTime, int>();
            var query = PrepareQuery(null, noTracking);
            var domainEntities = await query
                .Where(e =>
                    e.RoleMealType!.MealTypeId == mealTypeId
                    && roleIds.Contains(e.RoleMealType.AppRoleId)
                    && e.MealDate >= startDate
                    && e.MealDate <= endDate)
                .GroupBy(d => d.MealDate)
                .Select(r =>
                    new
                    {
                        MealDate = r.Key,
                        MealCount = r.Count()
                    }).ToDictionaryAsync(k => k.MealDate);

            foreach (var entity in domainEntities)
            {
                ret.Add(entity.Key, entity.Value.MealCount);
            }

            return ret;
        }
    }
}