using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class MyActionRepository : 
        EFBaseRepository<AppDbContext,Domain.App.Identity.AppUser, Domain.App.MyAction, DAL.App.DTO.MyAction>, IMyActionRepository
    {
        public MyActionRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALAppMapper<Domain.App.MyAction, DTO.MyAction>())
        {
        }

        

        public virtual async Task<IEnumerable<DTO.MyAction>> GetAllAsync(Guid myActionId, Guid? userId = null, bool noTracking = true)
        {
            var query = PrepareQuery(userId, noTracking);
            var domainEntities = await query.ToListAsync();
            var result = domainEntities.Select(e => Mapper.Map<Domain.App.MyAction, DTO.MyAction>(e));
            return result;
        }
    }
}