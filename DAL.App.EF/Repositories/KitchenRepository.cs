using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class KitchenRepository : 
        EFBaseRepository<AppDbContext,Domain.App.Identity.AppUser, Domain.App.Kitchen, DAL.App.DTO.Kitchen>, IKitchenRepository
    {
        public KitchenRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALAppMapper<Domain.App.Kitchen, DTO.Kitchen>())
        {
        }

        public virtual async Task<IEnumerable<DTO.Kitchen>> GetAllAsync(Guid? userId = null, bool noTracking = true)
        {
            var query = PrepareQuery(userId, noTracking);
            var domainEntities = await query.ToListAsync();
            var result = domainEntities.Select(e => Mapper.Map<Domain.App.Kitchen, DTO.Kitchen>(e));
            return result;
        }

        public virtual async Task<IEnumerable<DTO.Kitchen>> GetAllByIdsAsync(IReadOnlyList<Guid> kitchenIds, Guid? userId,
            bool noTracking = true)
        {
            var query = PrepareQuery(userId, noTracking);
            var domainEntities = await query.Where(c => kitchenIds.Contains(c.Id)).ToListAsync();
            var result = domainEntities.Select(e => Mapper.Map<Domain.App.Kitchen, DTO.Kitchen>(e));
            return result;
        }
    }
}