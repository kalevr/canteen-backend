using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;
using RoleInKitchenView = BLL.App.DTO.RoleInKitchenView;

namespace DAL.App.EF.Repositories
{
    public class RoleInKitchenViewRepository : 
        EFBaseRepository<AppDbContext,Domain.App.Identity.AppUser, Domain.App.RoleInKitchenView, 
            DAL.App.DTO.RoleInKitchenView>, IRoleInKitchenViewRepository
    {
        public RoleInKitchenViewRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALAppMapper<Domain.App.RoleInKitchenView, DTO.RoleInKitchenView>())
        {
        }

        

        public virtual async Task<IEnumerable<DTO.RoleInKitchenView>> GetAllAsync(Guid? userId = null, bool noTracking = true)
        {
            var query = PrepareQuery(userId, noTracking);
            var domainEntities = await query
                .Include(kv => kv.KitchenView)
                .Include(r => r.AppRole)
                .ToListAsync();
            var result = domainEntities.Select(e => Mapper.Map<Domain.App.RoleInKitchenView, DTO.RoleInKitchenView>(e));
            return result;
        }
        
        public virtual async Task<DTO.RoleInKitchenView> FirstOrDefaultAsync(Guid id, Guid? userId = null,
            bool noTracking = true)
        {
            var query = PrepareQuery(null, noTracking)
                .Where(rkv => rkv.Id == id)
                .Include(r => r.AppRole)
                .Include(k => k.KitchenView)
                .AsQueryable();
            var domainEntity = await query.FirstOrDefaultAsync();
            var mapThatShit = Mapper.Map<Domain.App.RoleInKitchenView, DTO.RoleInKitchenView>(domainEntity);
            return mapThatShit;
        }
        
        public virtual async Task<IEnumerable<DTO.RoleInKitchenView>> GetAllForKitchenViewAsync(Guid kitchenViewId, bool noTracking = true)
        {
            var query = PrepareQuery(null, noTracking);
            var domainEntities = await query
                .Include(u => u.AppRole)
                .Where(kv => kv.KitchenViewId == kitchenViewId)
                .ToListAsync();
            var result = domainEntities.Select(e => Mapper.Map<Domain.App.RoleInKitchenView, DTO.RoleInKitchenView>(e));
            return result;
        }
    }
}