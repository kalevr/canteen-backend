using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class LoginHistoryRepository : 
        EFBaseRepository<AppDbContext,Domain.App.Identity.AppUser, Domain.App.LoginHistory, DAL.App.DTO.LoginHistory>, ILoginHistoryRepository
    {
        public LoginHistoryRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALAppMapper<Domain.App.LoginHistory, DAL.App.DTO.LoginHistory>())
        {
        }
        
        
        public virtual async Task<IEnumerable<DTO.LoginHistory>> GetAllAsync(Guid? userId = null, bool noTracking = true)
        {
            var query = PrepareQuery(userId, noTracking);
            query = query.Include(u => u.AppUser).OrderByDescending(lh => lh.LoginTime);
            var domainEntities = await query.ToListAsync();
            var result = domainEntities.Select(e => Mapper.Map<Domain.App.LoginHistory, DTO.LoginHistory>(e));
            
            return result;
        }
        
        public virtual async Task<DTO.LoginHistory> FirstOrDefaultAsync(Guid id, Guid? userId = null,
            bool noTracking = true)
        {
            var query = PrepareQuery(null, noTracking)
                .Where(ck => ck.Id == id)
                .Include(c => c.AppUser)
                .AsQueryable();
            var domainEntity = await query.FirstOrDefaultAsync();
            var mapThatShit = Mapper.Map<Domain.App.LoginHistory, LoginHistory>(domainEntity);
            
            return mapThatShit;
        }
        
    }
}