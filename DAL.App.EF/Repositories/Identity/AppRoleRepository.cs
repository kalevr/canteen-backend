using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories.Identity;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories.Identity
{
    public class AppRoleRepository : 
        EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.Identity.AppRole, DAL.App.DTO.Identity.AppRole>, IAppRoleRepository
    {
        public AppRoleRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALAppMapper<Domain.App.Identity.AppRole, DTO.Identity.AppRole>())
        {
        }
        
        public virtual async Task<IEnumerable<DTO.Identity.AppRole>> GetAllAsync(Guid? userId = null, bool noTracking = true)
        {
            var query = PrepareQuery(userId, noTracking);
            var domainEntities = await query
                .ToListAsync();
            var result = domainEntities.Select(e => Mapper.Map<Domain.App.Identity.AppRole, DTO.Identity.AppRole>(e));
            return result;
        }
        
        public virtual async Task<IEnumerable<DTO.Identity.AppRole>> GetAllRolesAsync(bool mealOnly = false , bool noTracking = true)
        {
            var query = PrepareQuery(noTracking);
            if (mealOnly)
            {
                query = query.Where(c => c.GetsMeal == true);
            }
            
            var domainEntities = await query.ToListAsync();
            var result = domainEntities.Select(e => Mapper.Map<Domain.App.Identity.AppRole, DTO.Identity.AppRole>(e));
            return result;
        }
        
        public virtual async Task<IEnumerable<DTO.Identity.AppRole>> GetMultipleByIdsAsync(List<Guid> roleIds, bool noTracking = true)
        {
            var query = PrepareQuery(null, noTracking);
            var domainEntities = await query
                .Where(r => roleIds.Contains(r.Id))
                .OrderBy(r => r.DisplayName)
                .ToListAsync();
            var result = domainEntities.Select(e => Mapper.Map<Domain.App.Identity.AppRole, DTO.Identity.AppRole>(e));
            return result;
        }
        
        public virtual async Task<DTO.Identity.AppRole> FirstOrDefaultAsync(Guid id, Guid? userId = null,
            bool noTracking = true)
        {
            var query = PrepareQuery(null, noTracking)
                .Where(ck => ck.Id == id)
                .AsQueryable();
            var domainEntity = await query.FirstOrDefaultAsync();
            var mapThatShit = Mapper.Map<Domain.App.Identity.AppRole, DTO.Identity.AppRole>(domainEntity);
            return mapThatShit;
        }
    }
}