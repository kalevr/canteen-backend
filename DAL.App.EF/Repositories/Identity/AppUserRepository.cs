using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories.Identity;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories.Identity
{
    public class AppUserRepository :
        EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.Identity.AppUser,
            DAL.App.DTO.Identity.AppUser>, IAppUserRepository
    {
        public AppUserRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALAppMapper<Domain.App.Identity.AppUser, DTO.Identity.AppUser>())
        {
        }

        public virtual async Task<IEnumerable<DTO.Identity.AppUser>> GetAllAsync(Guid? userId = null,
            bool noTracking = true)
        {
            var query = PrepareQuery(userId, noTracking);
            var domainEntities = await query.ToListAsync();
            var result = domainEntities.Select(e => Mapper.Map<Domain.App.Identity.AppUser, DTO.Identity.AppUser>(e));
            return result;
        }
    }
}