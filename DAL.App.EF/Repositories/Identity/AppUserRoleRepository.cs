using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories.Identity;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories.Identity
{
    public class AppUserRoleRepository :
        EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.Identity.AppUserRole,
            DAL.App.DTO.Identity.AppUserRole>, IAppUserRoleRepository
    {
        public AppUserRoleRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALAppMapper<Domain.App.Identity.AppUserRole, DTO.Identity.AppUserRole>())
        {
        }

        public virtual async Task<IEnumerable<DTO.Identity.AppUserRole>> GetAllAsync(Guid? userId = null,
            bool noTracking = true)
        {
            var query = PrepareQuery(userId, noTracking);

            if (userId != null)
            {
                query = query.Where(u => u.UserId == userId);
            }

            var domainEntities = await query
                .Include(u => u.AppUser)
                .Include(r => r.AppRole)
                .ToListAsync();
            var result = domainEntities.Select(e =>
                Mapper.Map<Domain.App.Identity.AppUserRole, DTO.Identity.AppUserRole>(e));
            return result;
        }
        
        
        public virtual async Task<IEnumerable<DTO.Identity.AppUserRole>> GetAllWithRolesAsync(List<Guid> roleIds,
            bool noTracking = true)
        {
            var query = PrepareQuery(null, noTracking);

            var domainEntities = await query
                .Include(u => u.AppUser)
                .Include(r => r.AppRole)
                .Where(r => roleIds.Contains(r.RoleId))
                .ToListAsync();
            var result = domainEntities.Select(e =>
                Mapper.Map<Domain.App.Identity.AppUserRole, DTO.Identity.AppUserRole>(e));
            return result;
        }


        public virtual async Task<IEnumerable<DTO.Identity.AppUserRole>> GetAllWithRoleGroupedByUserIdAsync(
            Guid? roleId = null, bool noTracking = true)
        {
            var query = PrepareQuery(noTracking);


            if (roleId != Guid.Empty)
            {
                query = query.Where(u => u.RoleId == roleId);
            }

            var domainEntities = await query
                .Include(u => u.AppUser)
                .Include(r => r.AppRole)
                .ToListAsync();

            var temp = domainEntities.GroupBy(u => u.UserId).Select(e =>
                e.First());
            var result = temp
                .Select(e => Mapper.Map<Domain.App.Identity.AppUserRole, DTO.Identity.AppUserRole>(e));

            return result;
        }

        public virtual async Task<IEnumerable<DTO.Identity.AppUserRole>> GetAllForRoleAsync(Guid roleId,
            bool noTracking = true)
        {
            var query = PrepareQuery(noTracking);


            query = query.Where(u => u.RoleId == roleId);


            var domainEntities = await query
                .Include(u => u.AppUser)
                .ToListAsync();
            var result = domainEntities.Select(e =>
                Mapper.Map<Domain.App.Identity.AppUserRole, DTO.Identity.AppUserRole>(e));
            return result;
        }

        public virtual async Task<DTO.Identity.AppUserRole> FindByUserAndRoleAsync(Guid userId, Guid roleId)
        {
            var query = PrepareQuery();
            var domainEntities = await query
                .Where(ur => ur.UserId == userId && ur.RoleId == roleId)
                .Include(u => u.AppUser)
                .Include(r => r.AppRole)
                .FirstOrDefaultAsync();

            var result = Mapper.Map<Domain.App.Identity.AppUserRole, DTO.Identity.AppUserRole>(domainEntities);
            return result;
        }

        public bool HasSimilarRole(string targetRole, List<string> allRoles)
        {
            foreach (var role in allRoles)
            {
                if (role.ToLower().Contains(targetRole.ToLower()))
                {
                    return true;
                }
            }
            return false;
        }
    }
}