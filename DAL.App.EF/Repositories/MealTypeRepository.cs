using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class MealTypeRepository : 
        EFBaseRepository<AppDbContext,Domain.App.Identity.AppUser, Domain.App.MealType, DAL.App.DTO.MealType>, IMealTypeRepository
    {
        public MealTypeRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALAppMapper<Domain.App.MealType, DTO.MealType>())
        {
        }

        

        public virtual async Task<IEnumerable<DTO.MealType>> GetAllAsync(Guid? userId = null, bool noTracking = true)
        {
            var query = PrepareQuery(userId, noTracking);
            var domainEntities = await query.Include(k => k.Kitchen).ToListAsync();
            var result = domainEntities.Select(e => Mapper.Map<Domain.App.MealType, DTO.MealType>(e));
            return result;
        }
        
        public virtual async Task<IEnumerable<DTO.MealType>> GetAllForKitchenAsync(List<Guid> kitchenIds, bool noTracking = true)
        {
            var query = PrepareQuery(noTracking);
            var domainEntities = await query.Where(c => kitchenIds.Contains(c.KitchenId)).ToListAsync();
            var result = domainEntities.Select(e => Mapper.Map<Domain.App.MealType, DTO.MealType>(e));
            return result;
        }
        
        public virtual async Task<DTO.MealType> FirstOrDefaultAsync(Guid id, Guid? userId = null,
            bool noTracking = true)
        {
            var query = PrepareQuery(null, noTracking)
                .Where(ck => ck.Id == id)
                .Include(k => k.Kitchen)
                .AsQueryable();
            var domainEntity = await query.FirstOrDefaultAsync();
            var mapThatShit = Mapper.Map<Domain.App.MealType, DTO.MealType>(domainEntity);
            return mapThatShit;
        }
        
        public virtual async Task<IEnumerable<DTO.MealType>> GetAllByIdsAsync(IReadOnlyList<Guid> mealTypeIds, Guid? userId,
            bool noTracking = true)
        {
            var query = PrepareQuery(userId, noTracking);
            var domainEntities = await query.Where(c => mealTypeIds.Contains(c.Id)).ToListAsync();
            var result = domainEntities.Select(e => Mapper.Map<Domain.App.MealType, DTO.MealType>(e));
            return result;
        }
    }
}