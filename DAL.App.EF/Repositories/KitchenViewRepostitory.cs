using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class KitchenViewRepository :
        EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.KitchenView, DAL.App.DTO.KitchenView>,
        IKitchenViewRepository
    {
        public KitchenViewRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALAppMapper<Domain.App.KitchenView, DTO.KitchenView>())
        {
        }


        public virtual async Task<IEnumerable<DTO.KitchenView>> GetAllAsync(Guid? userId = null, bool noTracking = true)
        {
            var query = PrepareQuery(userId, noTracking);
            var domainEntities = await query.Include(m => m.MealType).ToListAsync();
            var result = domainEntities.Select(
                e => Mapper.Map<Domain.App.KitchenView, DTO.KitchenView>(e));
            return result;
        }

        public virtual async Task<DTO.KitchenView> FirstOrDefaultAsync(Guid id, Guid? userId = null,
            bool noTracking = true)
        {
            var query = PrepareQuery(null, noTracking)
                .Where(ck => ck.Id == id)
                .Include(m => m.MealType)
                .AsQueryable();
            var domainEntity = await query.FirstOrDefaultAsync();
            var mapThatShit = Mapper.Map<Domain.App.KitchenView, DTO.KitchenView>(domainEntity);
            return mapThatShit;
        }
        
        public virtual async Task<IEnumerable<DTO.KitchenView>> GetAllByIdsAsync(IReadOnlyList<Guid> kitchenViewIds, 
            Guid? userId, bool noTracking = true)
        {
            var query = PrepareQuery(userId, noTracking);
            var domainEntities = await query
                .Where(c => kitchenViewIds.Contains(c.Id))
                .Include(m => m.MealType)
                .ToListAsync();
            var result = domainEntities.Select(e => Mapper.Map<Domain.App.KitchenView, DTO.KitchenView>(e));
            return result;
        }
    }
}