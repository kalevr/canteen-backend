using AutoMapper;
using DAL.Base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class DALAppMapper<TLeftObject, TRightObject> : BaseDALMapper<TLeftObject, TRightObject>
        where TRightObject : class?, new()
        where TLeftObject : class?, new()
    {
        public DALAppMapper() : base()
        {
            // add more mappings
            MapperConfigurationExpression.CreateMap<Domain.App.Identity.AppUser, DAL.App.DTO.Identity.AppUser>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Identity.AppUser, Domain.App.Identity.AppUser>();

            MapperConfigurationExpression.CreateMap<Domain.App.Identity.AppRole, DAL.App.DTO.Identity.AppRole>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Identity.AppRole, Domain.App.Identity.AppRole>();

            MapperConfigurationExpression
                .CreateMap<Domain.App.Identity.AppUserRole, DAL.App.DTO.Identity.AppUserRole>();
            MapperConfigurationExpression
                .CreateMap<DAL.App.DTO.Identity.AppUserRole, Domain.App.Identity.AppUserRole>();

            MapperConfigurationExpression.CreateMap<Domain.App.Kitchen, DAL.App.DTO.Kitchen>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Kitchen, Domain.App.Kitchen>();

            MapperConfigurationExpression.CreateMap<Domain.App.KitchenView, DAL.App.DTO.KitchenView>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.KitchenView, Domain.App.KitchenView>();

            MapperConfigurationExpression.CreateMap<Domain.App.LoginHistory, DAL.App.DTO.LoginHistory>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.LoginHistory, Domain.App.LoginHistory>();

            MapperConfigurationExpression.CreateMap<Domain.App.MealType, DAL.App.DTO.MealType>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.MealType, Domain.App.MealType>();

            MapperConfigurationExpression.CreateMap<Domain.App.MyAction, DAL.App.DTO.MyAction>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.MyAction, Domain.App.MyAction>();

            MapperConfigurationExpression.CreateMap<Domain.App.Registration, DAL.App.DTO.Registration>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Registration, Domain.App.Registration>();

            MapperConfigurationExpression.CreateMap<Domain.App.RoleAction, DAL.App.DTO.RoleAction>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.RoleAction, Domain.App.RoleAction>();

            MapperConfigurationExpression.CreateMap<Domain.App.RoleInKitchenView, DAL.App.DTO.RoleInKitchenView>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.RoleInKitchenView, Domain.App.RoleInKitchenView>();

            MapperConfigurationExpression.CreateMap<Domain.App.RoleMealType, DAL.App.DTO.RoleMealType>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.RoleMealType, Domain.App.RoleMealType>();
            
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }
    }
}