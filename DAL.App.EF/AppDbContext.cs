﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Contracts.DAL.Base;
using Contracts.Domain;
using Domain.App;
using Domain.App.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF
{
    public class AppDbContext : IdentityDbContext<
        AppUser,
        AppRole,
        Guid,
        IdentityUserClaim<Guid>,
        AppUserRole,
        IdentityUserLogin<Guid>,
        IdentityRoleClaim<Guid>,
        IdentityUserToken<Guid>
    >, IBaseEntityTracker
    {
        private readonly IUserNameProvider _userNameProvider;

        public DbSet<AppUser> AppUsers { get; set; } = default!;
        public DbSet<AppRole> AppRoles { get; set; } = default!;
        public DbSet<AppUserRole> AppUserRoles { get; set; } = default!;
        
        
        public DbSet<MyAction> MyActions { get; set; } = default!;
        public DbSet<Kitchen> Kitchens { get; set; } = default!;
        public DbSet<KitchenView> KitchenViews { get; set; } = default!;
        public DbSet<LoginHistory> LoginHistories { get; set; } = default!;
        public DbSet<MealType> MealTypes { get; set; } = default!;
        public DbSet<Registration> Registrations { get; set; } = default!;
        public DbSet<RoleAction> RoleActions { get; set; } = default!;
        public DbSet<RoleInKitchenView> RoleInKitchenViews { get; set; } = default!;
        public DbSet<RoleMealType> RoleMealTypes { get; set; } = default!;
        
        private readonly Dictionary<IDomainEntityId<Guid>, IDomainEntityId<Guid>> _entityTracker =
            new Dictionary<IDomainEntityId<Guid>, IDomainEntityId<Guid>>();

        public AppDbContext(DbContextOptions<AppDbContext> options, IUserNameProvider userNameProvider) : base(options)
        {
            _userNameProvider = userNameProvider;
        }

        public void AddToEntityTracker(IDomainEntityId<Guid> internalEntity, IDomainEntityId<Guid> externalEntity)
        {
            _entityTracker.Add(internalEntity, externalEntity);
        }



        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            
            // disable cascade delete
            /*
            foreach (var relationship in builder.Model
                .GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            */

            builder.Entity<AppUserRole>(appUserRole =>
            {
                /* RoleManager stops working if i change PK!
                // get rid of the composite PK, replace with single ID
                appUserRole.HasKey(x => x.Id);
                // restore the functionality of composite PK with composite unique index
                appUserRole.HasIndex(x => new {x.UserId, x.RoleId}).IsUnique();
                // point out correct FKs for navigation properties to avoid shadow FK-s
                */
                appUserRole.Property(x => x.Id).HasDefaultValueSql("UUID()");
                appUserRole.Property(x => x.StartingFrom).HasDefaultValue(DateTime.Now);
                appUserRole.Property(x => x.ValidUntil).HasDefaultValue(DateTime.Parse("9999-12-31 23:59:59"));
                appUserRole.HasOne(x => x.AppUser).WithMany().HasForeignKey(x => x.UserId);
                appUserRole.HasOne(x => x.AppRole).WithMany().HasForeignKey(x => x.RoleId);
            });
        }
        
        /*
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // disable cascade delete
            foreach (var relationship in builder.Model
                .GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            // enable cascade delete on GpsSession->GpsLocation
            builder.Entity<GpsSession>()
                .HasMany(s => s.GpsLocations)
                .WithOne(l => l.GpsSession!)
                .OnDelete(DeleteBehavior.Cascade);

            // enable cascade delete on LangStr->LangStrTranslations
            builder.Entity<LangStr>()
                .HasMany(s => s.Translations)
                .WithOne(l => l.LangStr!)
                .OnDelete(DeleteBehavior.Cascade);

            // indexes
            builder.Entity<GpsSession>().HasIndex(i => i.CreatedAt);
            builder.Entity<GpsSession>().HasIndex(i => i.RecordedAt);
            builder.Entity<GpsLocation>().HasIndex(i => i.CreatedAt);
            builder.Entity<GpsLocation>().HasIndex(i => i.RecordedAt);

            builder.Entity<LangStrTranslation>().HasIndex(i => new {i.Culture, i.LangStrId}).IsUnique();
        }
        */

        private void SaveChangesMetadataUpdate()
        {
            // update the state of ef tracked objects
            ChangeTracker.DetectChanges();

            var markedAsAdded = ChangeTracker.Entries().Where(x => x.State == EntityState.Added);
            foreach (var entityEntry in markedAsAdded)
            {
                if (!(entityEntry.Entity is IDomainEntityMetadata entityWithMetaData)) continue;

                entityWithMetaData.CreatedAt = DateTime.Now;
                entityWithMetaData.CreatedBy = _userNameProvider.CurrentUserName;
            }

            var markedAsModified = ChangeTracker.Entries().Where(x => x.State == EntityState.Modified);
            foreach (var entityEntry in markedAsModified)
            {
                // check for IDomainEntityMetadata
                if (!(entityEntry.Entity is IDomainEntityMetadata entityWithMetaData)) continue;

                // something like this can be used for SoftDelete
                // entityWithMetaData.ChangedAt = DateTime.Now;
                // entityWithMetaData.ChangedBy = _userNameProvider.CurrentUserName;

                // do not let changes on these properties get into generated db sentences - db keeps old values
                entityEntry.Property(nameof(entityWithMetaData.CreatedAt)).IsModified = false;
                entityEntry.Property(nameof(entityWithMetaData.CreatedBy)).IsModified = false;
            }
        }

        private void UpdateTrackedEntities()
        {
            foreach (var (key, value) in _entityTracker)
            {
                value.Id = key.Id;
            }
        }

        public override int SaveChanges()
        {
            SaveChangesMetadataUpdate();
            var result = base.SaveChanges();
            UpdateTrackedEntities();
            return result;
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            SaveChangesMetadataUpdate();
            var result = base.SaveChangesAsync(cancellationToken);
            UpdateTrackedEntities();
            return result;
        }
    }
}