using System;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.App.Repositories.Identity;
using DAL.App.EF.Repositories;
using DAL.App.EF.Repositories.Identity;
using DAL.Base.EF;

namespace DAL.App.EF
{
    public class AppUnitOfWork : EFBaseUnitOfWork<Guid, AppDbContext>, IAppUnitOfWork
    {
        public AppUnitOfWork(AppDbContext uowDbContext) : base(uowDbContext)
        {
        }

        public IKitchenRepository Kitchens =>
            GetRepository<IKitchenRepository>(() => new KitchenRepository(UOWDbContext));
        
        public IKitchenViewRepository KitchenViews =>
            GetRepository<IKitchenViewRepository>(() => new KitchenViewRepository(UOWDbContext));
        
        public ILoginHistoryRepository LoginHistories =>
            GetRepository<ILoginHistoryRepository>(() => new LoginHistoryRepository(UOWDbContext));
        
        public IMealTypeRepository MealTypes =>
            GetRepository<IMealTypeRepository>(() => new MealTypeRepository(UOWDbContext));
        
        public IMyActionRepository MyActions =>
            GetRepository<IMyActionRepository>(() => new MyActionRepository(UOWDbContext));
        
        public IRegistrationRepository Registrations =>
            GetRepository<IRegistrationRepository>(() => new RegistrationRepository(UOWDbContext));
        
        public IParticipationRepository Participations =>
            GetRepository<IParticipationRepository>(() => new ParticipationRepository(UOWDbContext));

        public IRoleActionRepository RoleActions =>
            GetRepository<IRoleActionRepository>(() => new RoleActionRepository(UOWDbContext));
        
        public IRoleInKitchenViewRepository RoleInKitchenViews =>
            GetRepository<IRoleInKitchenViewRepository>(() => new RoleInKitchenViewRepository(UOWDbContext));
        
        public IRoleMealTypeRepository RoleMealTypes =>
            GetRepository<IRoleMealTypeRepository>(() => new RoleMealTypeRepository(UOWDbContext));
        
        public IAppUserRepository AppUsers =>
            GetRepository<IAppUserRepository>(() => new AppUserRepository(UOWDbContext));
        
        public IAppRoleRepository AppRoles =>
            GetRepository<IAppRoleRepository>(() => new AppRoleRepository(UOWDbContext));
        
        public IAppUserRoleRepository AppUserRoles =>
            GetRepository<IAppUserRoleRepository>(() => new AppUserRoleRepository(UOWDbContext));
            
    }
}