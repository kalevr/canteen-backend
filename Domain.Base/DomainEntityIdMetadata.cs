﻿using System;
using System.ComponentModel.DataAnnotations;
using Contracts.Domain;

namespace Domain.Base
{
    public abstract class DomainEntityIdMetadata : DomainEntityIdMetadata<Guid>, IDomainEntityId
    {
        
    }

    public abstract class DomainEntityIdMetadata<TKey> : DomainEntityId<TKey>, IDomainEntityMetadata
        where TKey : IEquatable<TKey>
    {
        [MaxLength(256)]
        [Display(Name = nameof(CreatedBy), ResourceType = typeof(Resources.BLL.Metadata))]
        public string? CreatedBy { get; set; }

        [Display(Name = nameof(CreatedAt), ResourceType = typeof(Resources.BLL.Metadata))]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [Display(Name = nameof(DeletedBy), ResourceType = typeof(Resources.BLL.Metadata))]
        [MaxLength(256)]
        public string? DeletedBy { get; set; }

        [Display(Name = nameof(DeletedAt), ResourceType = typeof(Resources.BLL.Metadata))]
        public DateTime? DeletedAt { get; set; }
    }
}