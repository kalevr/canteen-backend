using System;

namespace PublicApi.DTO.v1
{
    public class Registration
    {
        // this is for registering users for meals
        public Guid Id { get; set; }

        public DateTime MealDate { get; set; }

        public Guid AppUserId { get; set; }
        public string? AppUserName { get; set; } = default!;

        public Guid RoleMealTypeId { get; set; }
        public string? RoleMealTypeName { get; set; } = default!;
    }
}