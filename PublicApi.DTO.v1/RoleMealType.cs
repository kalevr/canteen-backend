using System;

namespace PublicApi.DTO.v1
{
    public class RoleMealType
    {
        public Guid Id { get; set; }
        
        public DateTime StartingFrom { get; set; }
        public DateTime? ValidUntil { get; set; }

        public Guid MealTypeId { get; set; }
        public string? MealTypeName { get; set; } = default!;

        public Guid AppRoleId { get; set; }
        public string? AppRoleName { get; set; } = default!;
    }
}