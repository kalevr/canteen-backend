using System;
using System.Collections.Generic;

namespace PublicApi.DTO.v1
{
    public class ParticipationsContainer
    {
        public Dictionary<string, Dictionary<string, int>> ParticipationList { get; set; } = default!;
        public IEnumerable<RoleMealType> RoleMealTypes { get; set; } = default!;
        public DateTime ChosenDate { get; set; }
        public bool HasEditRight { get; set; }
        public bool HasExtendedEditRight { get; set; }
        public int RegCutoff { get; set; }
        public DateTime MealTime { get; set; }
        public string MealName { get; set; } = default!;

    }
}