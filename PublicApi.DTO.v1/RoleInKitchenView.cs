using System;

namespace PublicApi.DTO.v1
{
    public class RoleInKitchenView
    {
        public Guid Id { get; set; }

        public DateTime StartingFrom { get; set; }
        public DateTime? ValidUntil { get; set; }

        public Guid AppRoleId { get; set; }
        public string AppRoleName { get; set; } = default!;

        public Guid KitchenViewId { get; set; }
        public string? KitchenViewName { get; set; } = default!;
    }
}