using System;
using System.ComponentModel.DataAnnotations;

namespace PublicApi.DTO.v1
{
    public class MyAction
    {
        public Guid Id { get; set; }

        [MinLength(1)]
        [MaxLength(256)]
        public string MyActionName { get; set; } = default!;
        
        [MinLength(1)]
        [MaxLength(256)]
        public string MyActionDisplayName { get; set; } = default!;
    }
}