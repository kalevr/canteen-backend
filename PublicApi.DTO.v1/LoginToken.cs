﻿namespace PublicApi.DTO.v1
{
    public class LoginToken
    {
        public string Email { get; set; } = default!;
        public string JwtToken { get; set; } = default!;
    }
}