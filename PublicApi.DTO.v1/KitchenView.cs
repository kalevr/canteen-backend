using System;
using System.ComponentModel.DataAnnotations;
using HotChocolate.Types.Relay;

namespace PublicApi.DTO.v1
{
    public class KitchenView
    {
        public Guid Id { get; set; }

        [MinLength(1)]
        [MaxLength(256)]
        public string KitchenViewName { get; set; } = default!;
        
        [MinLength(1)]
        [MaxLength(256)]
        public string KitchenViewDisplayName { get; set; } = default!;

        public DateTime StartingFrom { get; set; }
        public DateTime? ValidUntil { get; set; }

        [ID(nameof(MealType))]
        public Guid MealTypeId { get; set; }
        public MealType? MealType { get; set; }
    }
}