using AutoMapper;

namespace PublicApi.DTO.v1.Mappers
{
    public class MealTypeMapper : BaseMapper<BLL.App.DTO.MealType, MealType>
    {
        private readonly KitchenMapper _kitchenMapper = new KitchenMapper();
        public MealTypeMapper()
        {
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.MealType, MealType>();
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }
        public MealType BllToV1WithKitchen(BLL.App.DTO.MealType inObject)
        {
            Kitchen? kitchen = null;
            if (inObject.Kitchen != null)
            {
                kitchen = _kitchenMapper.Map<BLL.App.DTO.Kitchen, Kitchen>(inObject.Kitchen);
            }

            var result = new MealType()
            {
                Id = inObject.Id,
                MealTypeName = inObject.MealTypeName,
                MealTypeDisplayName = inObject.MealTypeDisplayName,
                MealTime = inObject.MealTime,
                RegistrationCutoff = inObject.RegistrationCutoff,
                Comment = inObject.Comment,
                KitchenId = inObject.KitchenId,
                Kitchen = kitchen
            };
            
            return result;
        }
    }
}