using AutoMapper;

namespace PublicApi.DTO.v1.Mappers
{
    public class LoginHistoryMapper : BaseMapper<BLL.App.DTO.LoginHistory, LoginHistory>
    {
        public LoginHistoryMapper()
        {
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.LoginHistory, LoginHistory>();
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }

        public LoginHistory LoginHistoryApiView(BLL.App.DTO.LoginHistory inObject)
        {
            var mapped = Mapper.Map<LoginHistory>(inObject);
            if (inObject.AppUser != null)
            {
                mapped.AppUserName = inObject.AppUser.FirstLastName;
            }

            return mapped;
        }
    }
}