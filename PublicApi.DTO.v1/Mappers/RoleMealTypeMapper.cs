using AutoMapper;

namespace PublicApi.DTO.v1.Mappers
{
    public class RoleMealTypeMapper : BaseMapper<BLL.App.DTO.RoleMealType, RoleMealType>
    {
        public RoleMealTypeMapper()
        {
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.RoleMealType, RoleMealType>();
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }
        public RoleMealType RoleMealTypeApiView(BLL.App.DTO.RoleMealType inObject)
        {
            var mapped = Mapper.Map<RoleMealType>(inObject);
            if (inObject.AppRole != null)
            {
                mapped.AppRoleName = inObject.AppRole.Name;
            }

            if (inObject.MealType != null)
            {
                mapped.MealTypeName = inObject.MealType.MealTypeName;

            }
            
            return mapped;
        }
    }
}