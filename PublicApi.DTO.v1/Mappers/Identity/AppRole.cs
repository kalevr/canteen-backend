using AutoMapper;
using PublicApi.DTO.v1.Identity;

namespace PublicApi.DTO.v1.Mappers.Identity
{
    public class AppRoleMapper : BaseMapper<BLL.App.DTO.Identity.AppRole, AppRole>
    {
        public AppRoleMapper()
        {
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Identity.AppRole, AppRole>();
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }
        public AppRole AppRoleApiView(BLL.App.DTO.Identity.AppRole inObject)
        {
            var mapped = Mapper.Map<AppRole>(inObject);
            return mapped;
        }
    }
}