using PublicApi.DTO.v1.Identity;

namespace PublicApi.DTO.v1.Mappers.Identity
{
    public class AppUserMapper : BaseMapper<BLL.App.DTO.Identity.AppUser, AppUser>
    {
        
    }
}