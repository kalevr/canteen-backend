using AutoMapper;

namespace PublicApi.DTO.v1.Mappers
{
    public class KitchenViewMapper : BaseMapper<BLL.App.DTO.KitchenView, KitchenView>
    {
        private readonly MealTypeMapper _mealTypeMapper = new MealTypeMapper();
        public KitchenViewMapper()
        {
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.KitchenView, KitchenView>();
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }
        public KitchenView BllToV1WithMealType(BLL.App.DTO.KitchenView inObject)
        {
            MealType? mealType = null;
            if (inObject.MealType != null)
            {
                mealType = _mealTypeMapper.Map<BLL.App.DTO.MealType, MealType>(inObject.MealType);
            }

            var result = new KitchenView()
            {
                Id = inObject.Id,
                KitchenViewName = inObject.KitchenViewName,
                KitchenViewDisplayName = inObject.KitchenViewDisplayName,
                StartingFrom = inObject.StartingFrom,
                ValidUntil = inObject.ValidUntil,
                MealTypeId = inObject.MealTypeId,
                MealType = mealType
            };

            return result;
        }
    }
}