using AutoMapper;

namespace PublicApi.DTO.v1.Mappers
{
    public class RegistrationMapper : BaseMapper<BLL.App.DTO.Registration, Registration>
    {
        public RegistrationMapper()
        {
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Registration, Registration>();
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }
        public Registration RegistrationApiView(BLL.App.DTO.Registration inObject)
        {
            var mapped = Mapper.Map<Registration>(inObject);
            
            if (inObject.AppUser!= null)
            {
                mapped.AppUserName = inObject.AppUser.FirstLastName;
            }

            if (inObject.RoleMealType?.MealType != null)
            {
                mapped.RoleMealTypeName = inObject.RoleMealType.MealType.MealTypeName;
            }
            
            return mapped;
        }
    }
}