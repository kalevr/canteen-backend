using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PublicApi.DTO.v1
{
    /// <summary>
    /// A kitchen object
    /// </summary>
    public class Kitchen
    {
        public Guid Id { get; set; }
        
        [MinLength(1)]
        [MaxLength(256)]
        public string KitchenName { get; set; } = default!;

    }
}