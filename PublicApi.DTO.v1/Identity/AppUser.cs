﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PublicApi.DTO.v1.Identity
{
    public class AppUser
    {
        public Guid Id { get; set; }
        
        [MaxLength(50)]
        [MinLength(1)]
        [Required]
        public string FirstName { get; set; } = default!;

        [MaxLength(50)]
        [MinLength(1)]
        [Required]
        public string LastName { get; set; } = default!;

        public bool ManageSelf { get; set; } = default!;

        [MaxLength(1024)] 
        public string? Comment { get; set; }

        public string FirstLastName => FirstName + " " + LastName;
        public string Password { get; set; } = default!;
        public string ConfirmPassword { get; set; } = default!;


    }
}