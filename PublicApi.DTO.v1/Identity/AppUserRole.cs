﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PublicApi.DTO.v1.Identity
{
    public class AppUserRole
    {
        public Guid Id { get; set; } = new Guid();

        [Display(Name = nameof(StartingFrom), ResourceType = typeof(Resources.BLL.Identity.AppUserRole))]
        public DateTime StartingFrom { get; set; }

        [Display(Name = nameof(ValidUntil), ResourceType = typeof(Resources.BLL.Identity.AppUserRole))]
        public DateTime ValidUntil { get; set; }

        public Guid UserId { get; set; }
        public string? Username { get; set; }

        public Guid RoleId { get; set; }
        public string? RoleName { get; set; }
    }
}