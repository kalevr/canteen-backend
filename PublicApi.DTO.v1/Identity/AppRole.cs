﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PublicApi.DTO.v1.Identity
{
    public class AppRole
    {
        public Guid Id { get; set; }

        public string Name { get; set; } = default!;
        
        [Display(Name = nameof(GetsMeal), ResourceType = typeof(Resources.BLL.Identity.AppRole))]
        public bool GetsMeal { get; set; } = default!;

        [Display(Name = nameof(Comment), ResourceType = typeof(Resources.BLL.Identity.AppRole))]
        [MaxLength(1024)] public string? Comment { get; set; }
        
        [MinLength(1)]
        [MaxLength(255)]
        public string DisplayName { get; set; } = default!;
    }
}