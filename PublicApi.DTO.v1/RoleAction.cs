using System;

namespace PublicApi.DTO.v1
{
    public class RoleAction
    {
        public Guid Id { get; set; }

        public DateTime StartingFrom { get; set; }
        public DateTime ValidUntil { get; set; }

        public Guid ActionId { get; set; }
        public string? ActionName { get; set; } = default!;

        public Guid AppRoleId { get; set; }
        public string? AppRoleName { get; set; } = default!;

        public Guid? TargetAppRoleId { get; set; }
        public string? TargetAppRoleName { get; set; } = default!;
    }
}