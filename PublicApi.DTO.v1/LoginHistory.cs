using System;
using System.ComponentModel.DataAnnotations;

namespace PublicApi.DTO.v1
{
    public class LoginHistory
    {
        // actually... nobody but the admin should see this
        public Guid Id { get; set; }
        
        [MaxLength(256)]
        public string UserName { get; set; } = default!;
        
        [MaxLength(20)]
        public string? Ipv4 { get; set; }

        [MaxLength(35)]
        public string? Ipv6 { get; set; }

        public DateTime LoginTime { get; set; }
        
        public bool Success { get; set; }


        public Guid? AppUserId { get; set; }
        public string? AppUserName { get; set; }
    }
}