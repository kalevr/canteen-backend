using System;

namespace PublicApi.DTO.v1
{
    public class MealTypeForMenu
    {
        public Guid Id { get; set; }
        public string MealTypeName { get; set; } = default!;
    }
}