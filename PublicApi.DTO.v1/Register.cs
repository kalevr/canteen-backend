using System;
using PublicApi.DTO.v1.Identity;

namespace PublicApi.DTO.v1
{
    public class Register
    {
        // this is for registration as registering users to system
        public string Email { get; set; } = default!;
        public string Password { get; set; } = default!;
        public string ConfirmPassword { get; set; } = default!;
        public string FirstName { get; set; } = default!;
        public string LastName { get; set; } = default!;


        public Guid AppUserId { get; set; }
        public AppUser? AppUser { get; set; }

        public Guid MealTypeId { get; set; }
        public MealType? MealType { get; set; }
    }
}