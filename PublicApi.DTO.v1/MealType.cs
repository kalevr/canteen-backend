using System;
using System.ComponentModel.DataAnnotations;

namespace PublicApi.DTO.v1
{
    public class MealType
    {
        public Guid Id { get; set; }

        [MinLength(1)]
        [MaxLength(256)]
        public string MealTypeName { get; set; } = default!;
        
        [MinLength(1)]
        [MaxLength(256)]
        public string MealTypeDisplayName { get; set; } = default!;
        
        public DateTime MealTime { get; set; }
        public int RegistrationCutoff { get; set; }
        public string? Comment { get; set; }

        public Guid KitchenId { get; set; }
        public Kitchen? Kitchen { get; set; }
        //public string? KitchenName { get; set; } = default!;
    }
}