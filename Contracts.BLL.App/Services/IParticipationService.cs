using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;
using PublicApi.DTO.v1;
using Participation = BLL.App.DTO.Participation;

namespace Contracts.BLL.App.Services
{
    public interface IParticipationService : IBaseEntityService<Participation>, IParticipationRepositoryCustom<Participation>
    {
        Task<ParticipationSidebarViewModel> GetParticipationsForGroupForMonthAsync(
            ParticipationSidebarViewModel prtSidebar, List<Guid> roleIdList, Guid mealTypeId, Guid roleId, 
            Guid roleMealTypeId, DateTime date);
        
        Task<ParticipationsContainer> GetParticipationsForGroupForMonthApiAsync(
            ParticipationsContainer prtSidebar, List<Guid> roleIdList, Guid mealTypeId, Guid roleId, 
            Guid roleMealTypeId, DateTime date);

        Task<Participation> UpdateParticipations(List<string> selection, string username, 
            ParticipationSidebarViewModel prtSidebar, Guid userId, 
            Guid mealTypeId, Guid roleId, Guid roleMealTypeId, DateTime date);

        Task<FinancialReportSidebarView> GetFinancialReport(
            FinancialReportSidebarView frSidebar, Guid mealTypeId, DateTime date);
    }
}