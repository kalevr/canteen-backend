using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;
using PublicApi.DTO.v1;
using Registration = BLL.App.DTO.Registration;

namespace Contracts.BLL.App.Services
{
    public interface IRegistrationService : IBaseEntityService<Registration>, IRegistrationRepositoryCustom<Registration>
    {
        Task<RegistrationSidebarViewModel> GetRegistrationsForGroupForMonthAsync(
            RegistrationSidebarViewModel regSidebar, List<Guid> roleIdList, Guid mealTypeId, Guid roleId, 
            Guid roleMealTypeId, DateTime date);
        
        Task<RegistrationsContainer> GetRegistrationsForGroupForMonthApiAsync(
            RegistrationsContainer regSidebar, List<Guid> roleIdList, Guid mealTypeId, Guid roleId, 
            Guid roleMealTypeId, DateTime date);

        Task<Registration> UpdateRegistrations(List<string> selection, string username, 
            RegistrationSidebarViewModel regSidebar, Guid userId, 
            Guid mealTypeId, Guid roleId, Guid roleMealTypeId, DateTime date);

        Task<FinancialReportSidebarView> GetFinancialReport(
            FinancialReportSidebarView frSidebar, Guid mealTypeId, DateTime date);
    }
}