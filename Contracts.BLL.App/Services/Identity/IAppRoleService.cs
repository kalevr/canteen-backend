using BLL.App.DTO.Identity;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories.Identity;

namespace Contracts.BLL.App.Services.Identity
{
    public interface IAppRoleService : IBaseEntityService<AppRole>, IAppRoleRepositoryCustom<AppRole>
    {
    }
}