using System;
using System.Threading.Tasks;
using BLL.App.DTO.Identity;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories.Identity;

namespace Contracts.BLL.App.Services.Identity
{
    public interface IAppUserRoleService : IBaseEntityService<AppUserRole>, IAppUserRoleRepositoryCustom<AppUserRole>
    {
        Task<bool> CheckIfAppUserRoleExists(Guid userId, Guid roleId);
    }
}