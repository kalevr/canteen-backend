using System;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;

namespace Contracts.BLL.App.Services
{
    public interface IRoleInKitchenViewService : IBaseEntityService<RoleInKitchenView>, IRoleInKitchenViewRepositoryCustom<RoleInKitchenView>
    {
        Task<RoleInKitchenViewSidebarViewModel> GetMealStatsForMonth(RoleInKitchenViewSidebarViewModel rkvSidebar,
            DateTime date,Guid mealTypeId);
    }
}