using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;

namespace Contracts.BLL.App.Services
{
    public interface IRoleMealTypeService : IBaseEntityService<RoleMealType>, IRoleMealTypeRepositoryCustom<RoleMealType>
    {
        Task<IEnumerable<RoleMealType>> GetAllWithNamesAsync(Guid? userId = null, bool noTracking = true);

    }
}