﻿using Contracts.BLL.App.Services;
 using Contracts.BLL.App.Services.Identity;
using Contracts.BLL.Base;

namespace Contracts.BLL.App
{
    public interface IAppBLL : IBaseBLL
    {
        IAppUserService AppUsers { get; }
        
        IAppRoleService AppRoles { get; }
        
        IAppUserRoleService AppUserRoles { get; }

        IKitchenService Kitchens { get; }
        
        IKitchenViewService KitchenViews { get; }

        ILoginHistoryService LoginHistories { get; }

        IMealTypeService MealTypes { get; }

        IMyActionService MyActions { get; }
        IRegistrationService Registrations { get; }

        IRoleActionService RoleActions { get; }

        IRoleInKitchenViewService RoleInKitchenViews { get; }

        IRoleMealTypeService RoleMealTypes { get; }
    }
}