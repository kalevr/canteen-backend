﻿namespace Contracts.BLL.Base.Mappers
{
    public interface IBaseMapper
    {
        /*
        TRightObject Map(TLeftObject inObject);
        TLeftObject Map(TRightObject inObject);
        */

        TOut Map<TIn, TOut>(TIn inObject)
            where TIn : class?, new()
            where TOut : class?, new();
    }
}
