﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace Domain.App
{
    public class MyAction : DomainEntityIdMetadata
    {
        // id'd ei ole eraldi vaja (DomainEntityMetadata kaudu tuleb Id)

        [MinLength(1)]
        [MaxLength(256)]
        public string MyActionName { get; set; } = default!;
        
        [MinLength(1)]
        [MaxLength(256)]
        public string MyActionDisplayName { get; set; } = default!;

        // ---- GOING OUT ----
        public ICollection<RoleAction>? RoleActions { get; set; }
    }
}