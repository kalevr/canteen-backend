﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Base;

namespace Domain.App
{
    public class KitchenView : DomainEntityIdMetadata
    {
        [MinLength(1)]
        [MaxLength(256)]
        public string KitchenViewName { get; set; } = default!;
        
        [MinLength(1)]
        [MaxLength(256)]
        public string KitchenViewDisplayName { get; set; } = default!;

        public DateTime StartingFrom { get; set; }

        public DateTime? ValidUntil { get; set; }

        // ---- COMING IN ----

        [ForeignKey(nameof(MealType))]
        public Guid MealTypeId { get; set; } = default!;

        public MealType? MealType { get; set; }


        // ---- GOING OUT ----
        [InverseProperty(nameof(RoleInKitchenView.KitchenView))]
        public ICollection<RoleInKitchenView>? RoleInKitchenViews { get; set; }
    }
}