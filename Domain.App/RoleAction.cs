﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.App.Identity;
using Domain.Base;

namespace Domain.App
{
    public class RoleAction: DomainEntityIdMetadata
    {
        public DateTime StartingFrom { get; set; }

        public DateTime ValidUntil { get; set; }

        // ---- COMING IN ----
        [ForeignKey(nameof(Action))]
        public Guid MyActionId { get; set; } = default!;
        public MyAction? Action { get; set; }

        [ForeignKey(nameof(AppRole))]
        public Guid AppRoleId { get; set; } = default!;
        public AppRole? AppRole { get; set; }

        [ForeignKey(nameof(TargetAppRole))]
        public Guid? TargetAppRoleId { get; set; } = default!;
        public AppRole? TargetAppRole { get; set; }
    }
}