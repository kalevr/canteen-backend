﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Base;

namespace Domain.App
{
    public class MealType : DomainEntityIdMetadata
    {
        [MinLength(1)]
        [MaxLength(256)]
        public string MealTypeName { get; set; } = default!;
        
        [MinLength(1)]
        [MaxLength(256)]
        public string MealTypeDisplayName { get; set; } = default!;

        public DateTime MealTime { get; set; } = default!;

        public int RegistrationCutoff { get; set; } = default!;

        [MaxLength(1024)]
        public string? Comment { get; set; }

        // ---- COMING IN ----
        [ForeignKey(nameof(Kitchen))] 
        public Guid KitchenId { get; set; } = default!;

        public Kitchen? Kitchen { get; set; }


        // ---- GOING OUT ----
        [InverseProperty(nameof(RoleMealType.MealType))]
        public ICollection<RoleMealType>? RoleMealTypes { get; set; }

        [InverseProperty(nameof(KitchenView.MealType))]
        public ICollection<KitchenView>? KitchenViews { get; set; }
    }
}