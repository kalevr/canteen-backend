﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.App.Identity;
using Domain.Base;

namespace Domain.App
{
    public class RoleInKitchenView: DomainEntityIdMetadata
    {
        public DateTime StartingFrom { get; set; }

        public DateTime? ValidUntil { get; set; }
        
        
        // ---- COMING IN ----
        [ForeignKey(nameof(AppRole))]
        public Guid AppRoleId { get; set; } = default!;
        public AppRole? AppRole { get; set; }

        [ForeignKey(nameof(KitchenView))]
        public Guid KitchenViewId { get; set; } = default!;
        public KitchenView? KitchenView { get; set; }
    }
}