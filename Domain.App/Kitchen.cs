﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Base;

namespace Domain.App
{
    public class Kitchen: DomainEntityIdMetadata
    {
        [MinLength(1)]
        [MaxLength(256)]
        public string KitchenName { get; set; } = default!;
        

        // ---- GOING OUT ----
        [InverseProperty(nameof(MealType.Kitchen))]
        public ICollection<MealType>? MealTypes { get; set; }
    }
}