﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.App.Identity;
using Domain.Base;

namespace Domain.App
{
    public class Registration : DomainEntityIdMetadataUser<AppUser>
    {
        [Display(Name = nameof(MealDate), ResourceType = typeof(Resources.BLL.Registration))]
        public DateTime MealDate { get; set; }


        // ---- COMING IN ----

        [ForeignKey(nameof(RoleMealType))] public Guid RoleMealTypeId { get; set; } = default!;
        public RoleMealType? RoleMealType { get; set; }
    }
}