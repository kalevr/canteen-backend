﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.App.Identity;
using Domain.Base;

namespace Domain.App
{
    public class LoginHistory : DomainEntityIdMetadata
    {
        [MaxLength(256)]
        public string UserName { get; set; } = default!;

        [MaxLength(20)]
        public string? Ipv4 { get; set; }

        [MaxLength(35)]
        public string? Ipv6 { get; set; }

        public DateTime LoginTime { get; set; }

        public bool Success { get; set; }

        [MaxLength(4096)]
        public string? UserAgent { get; set; }


        // ---- COMING IN ----
        [ForeignKey(nameof(AppUser))]
        public Guid? AppUserId { get; set; }

        public AppUser? AppUser { get; set; }
    }
}