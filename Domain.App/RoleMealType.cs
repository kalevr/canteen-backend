﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.App.Identity;
using Domain.Base;

namespace Domain.App
{
    public class RoleMealType: DomainEntityIdMetadata
    {
        public DateTime StartingFrom { get; set; }

        public DateTime? ValidUntil { get; set; }
        
        // ---- GOING OUT ----
        [InverseProperty(nameof(Registration.RoleMealType))]
        public ICollection<Registration>? Registrations { get; set; }
        
        
        // ---- COMING IN ----
        [ForeignKey(nameof(MealType))]
        public Guid MealTypeId { get; set; } = default!;
        public MealType? MealType { get; set; }

        [ForeignKey(nameof(AppRole))]
        public Guid AppRoleId { get; set; } = default!;
        public AppRole? AppRole { get; set; }
    }
}