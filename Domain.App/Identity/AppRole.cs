﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Contracts.Domain;
using Microsoft.AspNetCore.Identity;

namespace Domain.App.Identity
{
    public class AppRole : IdentityRole<Guid>, IDomainEntityId
    {
        [Display(Name = nameof(GetsMeal), ResourceType = typeof(Resources.BLL.Identity.AppRole))]
        public bool GetsMeal { get; set; } = default!;

        [Display(Name = nameof(Comment), ResourceType = typeof(Resources.BLL.Identity.AppRole))]
        [MaxLength(1024)] public string? Comment { get; set; }

        [MinLength(1)]
        [MaxLength(255)]
        public string DisplayName { get; set; } = default!;


        [Display(Name = nameof(CreatedBy), ResourceType = typeof(Resources.BLL.Metadata))]
        public string? CreatedBy { get; set; }

        [Display(Name = nameof(CreatedAt), ResourceType = typeof(Resources.BLL.Metadata))]
        public DateTime CreatedAt { get; set; }

        [Display(Name = nameof(DeletedBy), ResourceType = typeof(Resources.BLL.Metadata))]
        public string? DeletedBy { get; set; }

        [Display(Name = nameof(DeletedAt), ResourceType = typeof(Resources.BLL.Metadata))]
        public DateTime? DeletedAt { get; set; }


        // ---- COMING IN ----
        


        // ---- GOING OUT ----
        [InverseProperty(nameof(RoleAction.AppRole))]
        public ICollection<RoleAction>? RoleActions { get; set; }

        [InverseProperty(nameof(RoleAction.TargetAppRole))]
        public ICollection<RoleAction>? RoleTargetActions { get; set; }

        [InverseProperty(nameof(RoleMealType.AppRole))]
        public ICollection<RoleMealType>? RoleMealTypes { get; set; }

        [InverseProperty(nameof(RoleInKitchenView.AppRole))]
        public ICollection<RoleInKitchenView>? RoleInKitchenViews { get; set; }
    }
}