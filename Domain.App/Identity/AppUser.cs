﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Contracts.Domain;
using Microsoft.AspNetCore.Identity;

namespace Domain.App.Identity
{
    public class AppUser: IdentityUser<Guid>, IDomainEntityId
    {
        // add your own fields
        [MaxLength(50)] [MinLength(1)]
        public string FirstName { get; set; } = default!;

        [MaxLength(50)] [MinLength(1)] 
        public string LastName { get; set; } = default!;

        public bool ManageSelf { get; set; } = default!;
        
        [MaxLength(1024)]
        public string? Comment { get; set; }
        
        
        // ---- GOING OUT -----
        
        [InverseProperty(nameof(LoginHistory.AppUser))]
        public ICollection<LoginHistory>? LoginHistories { get; set; }


        [Display(Name = nameof(FirstLastName), ResourceType = typeof(Resources.BLL.Identity.AppUser))]
        public string FirstLastName => FirstName + " " + LastName;

        public AppUserRole? AppUserRole { get; set; }



        [Display(Name = nameof(CreatedBy), ResourceType = typeof(Resources.BLL.Metadata))]
        public string? CreatedBy { get; set; }

        [Display(Name = nameof(CreatedAt), ResourceType = typeof(Resources.BLL.Metadata))]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [Display(Name = nameof(DeletedBy), ResourceType = typeof(Resources.BLL.Metadata))]
        public string? DeletedBy { get; set; }

        [Display(Name = nameof(DeletedAt), ResourceType = typeof(Resources.BLL.Metadata))]
        public DateTime? DeletedAt { get; set; }
    }
}