﻿using System;
using System.ComponentModel.DataAnnotations;
using Contracts.Domain;
using Microsoft.AspNetCore.Identity;

namespace Domain.App.Identity
{
    public class AppUserRole : IdentityUserRole<Guid>, IDomainEntityId
    {
        public Guid Id { get; set; }

        public virtual AppUser? AppUser { get; set; }
        
        public virtual AppRole? AppRole { get; set; }


        [Display(Name = nameof(StartingFrom), ResourceType = typeof(Resources.BLL.Identity.AppUserRole))]
        public DateTime StartingFrom { get; set; }

        [Display(Name = nameof(ValidUntil), ResourceType = typeof(Resources.BLL.Identity.AppUserRole))]
        public DateTime ValidUntil { get; set; }


        [Display(Name = nameof(CreatedBy), ResourceType = typeof(Resources.BLL.Metadata))]
        public string? CreatedBy { get; set; }

        [Display(Name = nameof(CreatedAt), ResourceType = typeof(Resources.BLL.Metadata))]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [Display(Name = nameof(DeletedBy), ResourceType = typeof(Resources.BLL.Metadata))]
        public string? DeletedBy { get; set; }

        [Display(Name = nameof(DeletedAt), ResourceType = typeof(Resources.BLL.Metadata))]
        public DateTime? DeletedAt { get; set; }
    }
}